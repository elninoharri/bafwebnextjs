export const APP_NAME = 'BAF WEB PERDJOEANGAN'
export const API_TIMEOUT = 60000;
export const MESSAGE_TOKEN_EXP = 'Token is expired';
export const API_KEY = 'soon';
export const TOKEN_EXP =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJQaG9uZW5vIjoiMDg1OTM5ODA2MjIxIiwiVG9rZW5pZCI6IjU2NTkzNkJBRk1PQklMRSIsIlVzZXJJZGVudGl0eSI6IjA2OTgyMDIxNTY1OTM2MTUwMjQ1IiwiVXNlcmlkIjoiNTY1OTM2IiwiZXhwIjoxNjEyNjMwNjc4LCJvcmlnX2lhdCI6MTYxMjYyNzA3OH0.Je7gEIKxUsAVnYWiiSpXQIJu3fOTa7aNV2DD1gt5oEc';

export const API_URL_USR_MNGMNT =
  'https://apidemo.baf.id/bafmobile/mobile/user_mgmnt/v1/';

export const API_URL_COMMON =
  'https://apidemo.baf.id/bafmobile/mobile/common/v1/';


export const API_URL_TRANSACTION =
  'https://apidemo.baf.id/bafmobile/mobile/transaction/v1/';

export const API_URL_COMMON_WEB =
  'https://apidemo.baf.id/bafmobile/cms/common/v1/';

export const API_URL_WEB =
  'https://apidemo.baf.id/baf/web/';

export const API_URL_SANDIAWEB = 'https://apidemo.baf.id/SalesPortal/';

export const API_URL_PRIVATE_DEV = 'http://172.16.1.187:2052/api/v1/bafwebuser/private/';
export const API_URL_PUBLIC_DEV = 'http://172.16.1.187:2052/api/v1/bafwebuser/public/';


export const CODE_JAMINAN = 'MGB^Multiguna Barang';
export const ADMIN_TYPE = "Masuk Angsuran";
export const INSTALLMENT_TYPE = "ADDM";

export const API_HEADERS_COMMON = {
  'Content-type': 'application/json',
  'X-Api-Key': '9515328e-d485-4d3e-b0e3-7bf20be04926',
};
export const API_HEADERS_CORE = {
  'Content-type': 'application/json',
  'X-Api-Key': '62ac2676-8e25-414b-95ba-f9b90c5bb745',
};
export const API_HEADERS_ORDER = {
  'Content-type': 'application/json',
  'X-Api-Key': '457b12ad-1baf-4545-8f0e-cdabb639c6d6',
};
export const API_HEADERS_USRMGMT = {
  'Content-type': 'application/json',
  'X-Api-Key': 'd5ba15b0-959a-4092-a7a9-66d8e7f46a08',
};
export const API_HEADERS_MASTER = {
  'Content-type': 'application/json',
  'X-Api-Key': 'b5dff617-50fd-47df-8547-e912ca9d04d4',
};


export const REGEX_EMAIL = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
export const REGEX_HP = /^(^\+62|62|^08)(\d{3,4}-?){2}\d{3,4}$/i;