export async function filterFetch(url, options) {
  return await fetch(url, options)
    .then((respone) => {
      if (respone.status == 500) {
        throw new Error('Error Internal Server');
      } else {
        try {
          return respone.json();
        } catch {
          throw new Error('Error Internal Server'); //kalo balikannya dari backend HTML
        }
      }
    })
    .then((json) => {
      if (json.response == 401) {
        throw new Error(JSON.stringify(json));
      } else if (json.response != 200) {
        throw new Error(json.message || 'Error API fetch data');
      }
      return json.result;
    });
}

export async function filterFetchStatus(url, options) {
  return await fetch(url, options)
    .then((respone) => {
      if (respone.status == 500) {
        throw new Error('Error Internal Server');
      } else if (respone.status == 401) {
        throw new Error('Error Unauthorized or Token Expired');
      } else {
        return respone.json();
      }
    })
    .then((json) => {
      if (json.status != 200) {
        throw new Error(json.message || 'Error API fetch data');
      }
      return json.result;
    });
}

export async function filterFetchToken(url, options) {
  return await fetch(url, options)
    .then((respone) => {
      if (respone.status == 500) {
        throw new Error('Error Internal Server');
      } else if (respone.status == 401) {
        throw new Error('Error Unauthorized or Token Expired');
      } else {
        return respone.json();
      }
    })
    .then((json) => {
      if (json.response != 200) {
        throw new Error(json.message || 'Error API fetch data');
      }
      return json.message;
    });
}

export async function filterFetchOrder(url, options) {
  return await fetch(url, options)
    .then((respone) => {
      if (respone.status == 500) {
        throw new Error(respone.status || 'Error API server');
      }
      if (respone.status == 404) {
        throw new Error(respone.status || 'Error API response');
      }
      return respone.json();
    })
    .then((json) => {
      if (json.statusquery !== 200) {
        throw new Error(json.message || 'Error API fetch data');
      }
      return json;
    });
}

export async function filterFetchCore(url, options) {
  return await fetch(url, options)
    .then((respone) => {
      if (!respone.ok) {
        throw new Error(respone.status || 'Error API response');
      }
      return respone.json();
    })
    .then((json) => {
      if (json == null) {
        throw new Error(json.log || 'Data tidak ditemukan');
      }
      return json;
    });
}

export async function filterFetchCommon(url, options) {
  return await fetch(url, options)
    .then((respone) => {
      if (!respone.ok) {
        throw new Error(respone.statusText || 'Error API response');
      }
      return respone.json();
    })
    .then((json) => {
      if (json.statusquery !== 200 || json.statusdb !== 200) {
        throw new Error(json.errormsg || 'Error API fetch data');
      } else if (json.statusload !== 200) {
        throw new Error(json.result || 'Data tidak ditemukan');
      }
      return json.result;
    });
}

export async function filterFetchSubmitOrder(url, options) {
  return await fetch(url, options)
    .then((respone) => {
      if (!respone.ok) {
        throw new Error(respone.status || 'Error API response');
      }
      return respone.json();
    })
    .then((json) => {
      if (!json.status) {
        throw new Error(json.message || json.result || 'Error API fetch data');
      }
      return json;
    });
}

export async function filterFetchFinancialData(url, options) {
  return await fetch(url, options)
    .then((respone) => {
      if (!respone.ok) {
        throw new Error(respone.status || 'Error API response');
      }
      return respone.json();
    })
    .then((json) => {
      if (json.errormsg) {
        throw new Error(json.errormsg || 'Error API fetch data');
      }
      return json;
    });
}

export async function filterFetchRevKT(url, options) {
  return await fetch(url, options)
    .then((respone) => {
      if (respone.status == 500) {
        throw new Error(respone.status || 'Error API server');
      }
      if (respone.status == 404) {
        throw new Error(respone.status || 'Error API response');
      }
      return respone.json();
    })
    .then((json) => {
      if (json.status != 13) {
        throw new Error(json.message || 'Error API fetch data');
      }
      return json;
    });
}

export async function filterFetchCancel(url, option) {
  return await fetch(url, option)
    .then((response) => {
      if (response.status == 500) {
        throw new Error('Error Internal Server');
      } else {
        return response.json();
      }
    })
    .then((json) => {
      console.log(json);
      if (json.status !== 200) {
        throw new Error(json.message || 'Error API Fetch API');
      }
      return json;
    });
}

export async function filterFetchLogout(url, options) {
  return await fetch(url, options)
    .then((respone) => {
      if (respone.status == 500) {
        throw new Error('Error Internal Server');
      } else {
        return respone.json();
      }
    })
    .then((json) => {
      if (json.response == 401) {
        throw new Error(JSON.stringify(json));
      } else if (json.response != 200) {
        throw new Error(json.message || 'Error API fetch data');
      }
      return json;
    });
}

export async function filterFetchBanner(url, options) {
  return await fetch(url, options)
    .then((respone) => {
      if (respone.status == 500) {
        throw new Error('Error Internal Server');
      } else {
        try {
          return respone.json();
        } catch {
          throw new Error('Error Internal Server'); //kalo balikannya dari backend HTML
        }
      }
    })
    .then((json) => {
      // console.log(json)
      return json;
    });
}

export async function filterFetchStatusload(url, options) {
  return await fetch(url, options)
    .then((respone) => {
      if (!respone.ok) {
        throw new Error(respone.status || 'Error API response');
      }
      return respone.json();
    })
    .then((json) => {
      if (!json.statusload) {
        throw new Error(json.errormsg || 'Error API fetch data');
      } else if (json.statusload !== 200) {
        throw new Error(json.result || 'Data tidak ditemukan');
      }
      return json.result;
    });
}