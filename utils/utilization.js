// import moment from 'moment';

export const substringCircum = (newValue) => {
  var output = newValue.split('^');
  return output[0];
};

export const substringDot = (newValue) => {
  var output = newValue.split('.');
  return output[0];
};

export const substringUrl = (newValue) => {
  var output = newValue.split('/');
  return output;
};

export const splitDot = (newValue) => {
  var output = newValue.split('.').join('');
  return output;
};

export const handleRefreshToken = async (data) => {
  await AsyncStorage.removeItem('userToken');
  await AsyncStorage.setItem('userToken', data);
};

export const jsonParse = async (data) => {
  try {
    const dataParse = JSON.parse(data);
    return dataParse;
  } catch (e) {
    return false;
  }
};

export const substringSecondCircum = (newValue) => {
  var output = newValue.split('^');
  return output[1];
};

export const substringThirdCircum = (newValue) => {
  var output = newValue.split('^');
  return output[2];
};

export const substringFourthCircum = (newValue) => {
  var output = newValue.split('^');
  return output[3];
};

export const sliceFirstEightLastSix = (newValue) => {
  let firstAttempt = newValue.slice(8);
  // let lastIndex = firstAttempt.length - 1;
  let secondAttempt = firstAttempt.slice(0, -6);
  return secondAttempt;
};

export const trimLineBreak = (value) => {
  const newValue = value.replace(/\\n/g, '');
  return newValue;
};

export const substringFifthCircum = (newValue) => {
  var output = newValue.split('^');
  return output[4];
};

export const substringNoHpFirstCircum = (newValue) => {
  var output = newValue.slice(0, 3); //0812888565* --> 0812
  return output;
};

export const substringNoHpSecondCircum = (newValue) => {
  var output = newValue.slice(3); //0812888565* --> 888565*
  return output;
};

export const substringTilde = (newValue) => {
  var output = newValue.split('~');
  return output[0];
};

export const substringSecondTilde = (newValue) => {
  var output = newValue.split('~');
  return output[1];
};

export const mergeValuesData = (newValue) => {
  var output = newValue
    .replace(/","/g, '|')
    .replace(/\["/g, '')
    .replace(/\"]/g, '');
  return output;
};

export const regexJsonEscape = (newValue) => {
  var output = /["'|^~\\\\\n\\\t\\\f\\\r\\\n\\]/g.test(newValue);
  return output;
};

export const regexHTML = (newValue) => {
  var regex = /(&nbsp;|<([^>]+)>)/gi,
    body = newValue,
    result = body.replace(regex, '');

  return result;
};

export const regexBackslash = (newValue) => {
  const body = newValue;
  const result = body.replace(/\"/g, '');
  return result;
};

export const regexSymbol = (newValue) => {
  var output = /[*+=_?^${/}<>&%(),.:;'^~!@#`\\\\|"[\]\\\n\\\t\\\f\\\r\\\n\\\-\\]/g.test(
    newValue,
  );
  return output;
};

export const regexSymbolNum = (newValue) => {
  var output = /[*+=_?^${/}<>&%(),.:;'^~!@#`\\\\|"[\]\\\n\\\t\\\f\\\r\\\n\\\-\\0-9]/g.test(
    newValue,
  );
  return output;
};

export const regexSymbolNumSpace = (newValue) => {
  var output = /[ *+=_.,?^${/}<>&%():;'^~!@#`\\\\|"[\]\\\n\\\t\\\f\\\r\\\n\\\-\\0-9]/g.test(
    newValue,
  );
  return output;
};

export const regexSymbolExceptDot = (newValue) => {
  var output = /[*+=_?^${/}<>&%():;'^~!@#`\\\\|"[\]\\\n\\\t\\\f\\\r\\\n\\\-\\]/g.test(
    newValue,
  );
  return output;
};

export const regexEmail = (newValue) => {
  var output = !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i.test(
    newValue,
  );
  return output;
};

export const regexNumOnly = (newValue) => {
  var output = /[A-Za-z *+=_.,?^${/}<>&%():;'^~!@#`\\\\|"[\]\\\n\\\t\\\f\\\r\\\n\\\-\\]/g.test(
    newValue,
  );
  return output;
};

export const resJsonParser = (newValue) => {
  var output = JSON.parse(
    newValue.replace(/([\[\]])/gm, '').replace(/(['])/gm, '"'),
  );
  return output;
};

export function romanize(num) {
  if (isNaN(num)) return NaN;
  var digits = String(+num).split(''),
    key = [
      '',
      'C',
      'CC',
      'CCC',
      'CD',
      'D',
      'DC',
      'DCC',
      'DCCC',
      'CM',
      '',
      'X',
      'XX',
      'XXX',
      'XL',
      'L',
      'LX',
      'LXX',
      'LXXX',
      'XC',
      '',
      'I',
      'II',
      'III',
      'IV',
      'V',
      'VI',
      'VII',
      'VIII',
      'IX',
    ],
    roman = '',
    i = 3;
  while (i--) roman = (key[+digits.pop() + i * 10] || '') + roman;
  return Array(+digits.join('') + 1).join('M') + roman;
}

export const formatCurrency = (value) => {
  if (!value) {
    return value;
  }
  return value.replace(/,/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, '.');
};

export const fdeFormatCurrency = (value) => {
  if (!value) {
    return value;
  }
  return 'Rp. ' + value.replace(/,/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, '.');
};

export const normalizeCurrency = (value) => {
  if (!value) {
    return value;
  }
  const onlyNums = value.replace(/[^\d]/g, '');
  return onlyNums;
};

// export const validSearchDaysRange = (startDate, endDate) => {
//   if (moment(endDate).diff(moment(startDate), 'days') > 7) {
//     return false;
//   } else {
//     return true;
//   }
// };

export const dateYearValidate = (newValue) => {
  var output = newValue.split('-');
  if (Number(output[0]) < 999 || Number(output[0].length > 4)) {
    return false;
  } else {
    return true;
  }
};

// export const nextMonth = (newValue) => {
//   var nextMonth = moment(newValue).add(1, 'M').format('YYYY-MM-DD');
//   return nextMonth;
// };

export const capitalizeFirstLetter = (string) => {
  let newString = string.toLowerCase();
  return newString.charAt(0).toUpperCase() + newString.slice(1);
}

export const titleToUrl = (string) =>
{
    return string
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
}

export const getTagFromType = (type, category) => {
  const tag = (type == 'BANNER_TYPE_ARTICLE' ? 'Article' : type == 'BANNER_TYPE_PROMOTION' ? 'Promo' : type == 'BANNER_TYPE_EVENT' ? 'Event' : category) || 'Other';
  return capitalizeFirstLetter(tag);
}