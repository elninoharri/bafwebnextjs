export const GET_PROVINCE_PROCESS = "GET_PROVINCE_PROCESS";
export const GET_PROVINCE_SUCCESS = "GET_PROVINCE_SUCCESS";
export const GET_PROVINCE_ERROR = "GET_PROVINCE_ERROR";

export const GET_CITY_PROCESS = 'GET_CITY_PROCESS';
export const GET_CITY_SUCCESS = 'GET_CITY_SUCCESS';
export const GET_CITY_ERROR = 'GET_CITY_ERROR';

export const GET_KECAMATAN_PROCESS = "GET_KECAMATAN_PROCESS";
export const GET_KECAMATAN_SUCCESS = "GET_KECAMATAN_SUCCESS";
export const GET_KECAMATAN_ERROR = "GET_KECAMATAN_ERROR";

export const GET_KELURAHAN_PROCESS = "GET_KELURAHAN_PROCESS";
export const GET_KELURAHAN_SUCCESS = "GET_KELURAHAN_SUCCESS";
export const GET_KELURAHAN_ERROR = "GET_KELURAHAN_ERROR";

export const GET_PENGAJUAN_PROCESS = 'GET_PENGAJUAN_PROCESS'
export const GET_PENGAJUAN_SUCCESS = 'GET_PENGAJUAN_SUCCESS'
export const GET_PENGAJUAN_ERROR = 'GET_PENGAJUAN_ERROR'

export const SIMULATION_DATA = "SIMULATION_DATA";

export function getProvince(data) {
    return {
        type: GET_PROVINCE_PROCESS,
        data: data
    };
}

export function getCity(data) {
    return {
        type: GET_CITY_PROCESS,
        data: data
    };
}

export function getKecamatan(data) {
    return {
        type: GET_KECAMATAN_PROCESS,
        data: data
    };
}

export function getKelurahan(data) {
    return {
        type: GET_KELURAHAN_PROCESS,
        data: data
    };
}

export function simulationData(data) {
    return {
        type: SIMULATION_DATA,
        payload: data
    }
}


export function getPengajuan(data) {
    return {
        type: GET_PENGAJUAN_PROCESS,
        data: data
    }
}