export const GET_REGION_PROCESS = 'GET_REGION_PROCESS';
export const GET_REGION_SUCCESS = 'GET_REGION_SUCCESS';
export const GET_REGION_ERROR = 'DETAIL_USER_ERROR';

export const GET_MAPPING_RISK_PROCESS = 'GET_MAPPING_RISK_PROCESS'
export const GET_MAPPING_RISK_SUCCESS = 'GET_MAPPING_RISK_SUCCESS'
export const GET_MAPPING_RISK_ERROR = 'GET_MAPPING_RISK_ERROR'

export const GET_MP_INSTALLMENT_PROCESS = 'GET_MP_INSTALLMENT_PROCESS'
export const GET_MP_INSTALLMENT_SUCCESS = 'GET_MP_INSTALLMENT_SUCCESS'
export const GET_MP_INSTALLMENT_ERROR = 'GET_MP_INSTALLMENT_ERROR'

export const GET_OTR_PROCESS = 'GET_OTR_PROCESS'
export const GET_OTR_SUCCESS = 'GET_OTR_SUCCESS'
export const GET_OTR_ERROR = 'GET_OTR_ERROR'

export const GET_JENIS_PROCESS = "GET_JENIS_PROCESS";
export const GET_JENIS_SUCCESS = "GET_JENIS_SUCCESS";
export const GET_JENIS_ERROR = "GET_JENIS_ERROR";

export const GET_MERK_PROCESS = "GET_MERK_PROCESS";
export const GET_MERK_SUCCESS = "GET_MERK_SUCCESS";
export const GET_MERK_ERROR = "GET_MERK_ERROR";

export const SELECTED_LOB = "SELECTED_LOB";
export const SELECTED_PRODUCT = "SELECTED_PRODUCT";

export function getRegion() {
    return {
        type: GET_REGION_PROCESS,
    };
}

export function getMappingRisk(data) {
    return {
        type: GET_MAPPING_RISK_PROCESS,
        data: data
    }
}

export function getMpInstallmentCalc(data) {
    return {
        type: GET_MP_INSTALLMENT_PROCESS,
        data: data
    }    
}

export function getOTR(data) {
    return {
        type: GET_OTR_PROCESS,
        data: data,
    };
}

export function setSelectedLob(id) {
    return {
        type: SELECTED_LOB,
        payload: id
    };
}

export function setSelectedProduct(id) {
    return {
        type: SELECTED_PRODUCT,
        payload: id
    };
}

export function getJenis(data) {
    return {
        type: GET_JENIS_PROCESS,
        data: data
    };
}

export function getMerk(data, token) {
    return {
        type: GET_MERK_PROCESS,
        data: data,
        token: token
    };
}