export const DETAIL_USER_PROCESS = 'DETAIL_USER_PROCESS';
export const DETAIL_USER_SUCCESS = 'DETAIL_USER_SUCCESS';
export const DETAIL_USER_ERROR = 'DETAIL_USER_ERROR';

export function detailUser(data, token) {
    return {
        type: DETAIL_USER_PROCESS,
        data: data,
        token: token,
    };
}