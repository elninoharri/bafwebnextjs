export const TOKEN_SETTER = "TOKEN_SETTER"

export const CHECK_TOKEN_PROCESS = "CHECK_TOKEN_PROCESS"
export const CHECK_TOKEN_SUCCESS = "CHECK_TOKEN_SUCCESS"
export const CHECK_TOKEN_ERROR = "CHECK_TOKEN_ERROR"

export const REFRESH_TOKEN_PROCESS = "REFRESH_TOKEN_PROCESS"
export const REFRESH_TOKEN_SUCCESS = "REFRESH_TOKEN_SUCCESS"
export const REFRESH_TOKEN_ERROR = "REFRESH_TOKEN_ERROR"

export const LOGIN_PROCESS = "LOGIN_PROCESS"
export const LOGIN_SUCCESS = "LOGIN_SUCCESS"
export const LOGIN_ERROR = "LOGIN_ERROR"

export const REGISTER_PROCESS = 'REGISTER_PROCESS';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_ERROR = 'REGISTER_ERROR';

export function setToken(data) {
    return {
        type: TOKEN_SETTER,
        data: data
    }
}

export function checkToken(token) {
    return {
        type: CHECK_TOKEN_PROCESS,
        token: token
    }
}

export function refreshToken(token) {
    return {
        type: REFRESH_TOKEN_PROCESS,
        token: token
    }
}

export function login(data) {
    return {
        type: LOGIN_PROCESS,
        data: data
    }
}

export function regis(data) {
    return {
        type: REGISTER_PROCESS,
        data: data,
    };
}
