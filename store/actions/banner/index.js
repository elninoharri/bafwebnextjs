// import * as t from "../../types";
export const GET_BANNER_PROCCESS = 'GET_BANNER_PROCCESS'
export const GET_BANNER_SUCCESS = 'GET_BANNER_SUCCESS'
export const GET_BANNER_ERROR = 'GET_BANNER_ERROR'

export const NEWS_DATA = 'NEWS_DATA'

export const GET_VOUCHER_CODE_PROCESS = 'GET_VOUCHER_CODE_PROCESS';
export const GET_VOUCHER_CODE_SUCCESS = 'GET_VOUCHER_CODE_SUCCESS';
export const GET_VOUCHER_CODE_ERROR = 'GET_VOUCHER_CODE_ERROR';

export function getBanner(data) {
    return {
        type: GET_BANNER_PROCCESS,
        data: data
    };
}

export function newsData(data) {
    return {
        type: NEWS_DATA,
        payload: data
    };
}

export function getVoucherCode(data, token) {
    return {
        type: GET_VOUCHER_CODE_PROCESS,
        data: data,
        token: token
    };
}