import { createStore, applyMiddleware } from "redux";
import { createWrapper } from "next-redux-wrapper";
import { composeWithDevTools } from "redux-devtools-extension";
import createMiddleware from "redux-saga";
import { persistStore } from 'redux-persist';

import rootReducer from "./reducers";
import rootSaga from "./sagas";

let store;
const sagaMiddleware = createMiddleware();

const isClient = typeof window !== 'undefined';

if (isClient) {
	const { persistReducer } = require('redux-persist');
	const storage = require('redux-persist/lib/storage/session').default;

	const persistConfig = {
		key: 'root',
		storage,
		blacklist: ['login', 'register', 'checkToken', 'setToken', 'refreshToken']
	};

	store = createStore(
		persistReducer(persistConfig, rootReducer),
		applyMiddleware(sagaMiddleware)
	);

	store.__PERSISTOR = persistStore(store);
} else {
	store = createStore(
		rootReducer,
		composeWithDevTools(applyMiddleware(sagaMiddleware))
	);
}

sagaMiddleware.run(rootSaga);

const makeStore = () => store;

export const wrapper = createWrapper(makeStore);