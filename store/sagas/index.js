import {all, fork} from 'redux-saga/effects'
import {
    watchFetchBanner,
    watchGetVoucherCode
} from './banner'
import {
    watchFetchRegion,
    watchFetchMappingRisk,
    watchFetchInstallmentCalc,
    watchFetchGetOTR,
    watchFetchJenis,
    watchFetchMerk
} from './homepage'
import {
    watchFetchProvince,
    watchFetchCity,
    watchFetchKecamatan,
    watchFetchKelurahan,
    watchFetchGetPengajuan
} from './application'
import {
    watchCheckToken,
    watchRefreshToken,
    watchLogin,
    watchRegister
} from './auth'
import {
    watchDetailUser
} from './account'

export default function* sagas() {
    yield all([
        fork(watchFetchBanner),
        fork(watchFetchRegion),
        fork(watchFetchMappingRisk),
        fork(watchFetchInstallmentCalc),
        fork(watchFetchGetPengajuan),
        fork(watchFetchGetOTR),
        fork(watchFetchJenis),
        fork(watchFetchMerk),
        fork(watchFetchProvince),
        fork(watchFetchCity),
        fork(watchFetchKecamatan),
        fork(watchFetchKelurahan),
        fork(watchCheckToken),
        fork(watchRefreshToken),
        fork(watchLogin),
        fork(watchRegister),        
        fork(watchDetailUser),
        fork(watchGetVoucherCode)
    ])
}