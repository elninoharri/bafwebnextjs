import { all, put, takeLatest } from "redux-saga/effects";
import {
  /* GET REGION */
  GET_REGION_PROCESS,
  GET_REGION_SUCCESS,
  GET_REGION_ERROR,

  /* GET MAPPING RISK */
  GET_MAPPING_RISK_PROCESS,
  GET_MAPPING_RISK_SUCCESS,
  GET_MAPPING_RISK_ERROR,

  /* GET INSTALLMENT */
  GET_MP_INSTALLMENT_PROCESS,
  GET_MP_INSTALLMENT_SUCCESS,
  GET_MP_INSTALLMENT_ERROR,

  GET_OTR_PROCESS,
  GET_OTR_SUCCESS,
  GET_OTR_ERROR,

  /* GET TIPE BARANG */
  GET_JENIS_PROCESS,
  GET_JENIS_SUCCESS,
  GET_JENIS_ERROR,

  /* GET MEREK BARANG */
  GET_MERK_PROCESS,
  GET_MERK_SUCCESS,
  GET_MERK_ERROR,

} from '../../actions/homepage'
import { filterFetch, filterFetchStatusload, filterFetchCore, filterFetchStatus } from '../../../utils/apiFetch'
import {
  API_URL_USR_MNGMNT,
  API_HEADERS_USRMGMT,
  API_TIMEOUT,
  API_KEY,
  API_URL_TRANSACTION,
  API_HEADERS_CORE,
  API_HEADERS_COMMON,
  API_URL_COMMON,
} from './../../../utils/constant'


function* getRegion() {
  try {
    const result = yield filterFetch(API_URL_USR_MNGMNT + 'getRegisteredCity', {
      method: 'GET',
      headers: {
        'Content-type': 'application/json',
        'x-api-key': API_KEY,
      },
      timeout: API_TIMEOUT,
    });
    yield put({
      type: GET_REGION_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_REGION_ERROR,
      error: error,
    });
  }
}

function* getMappingRisk(action) {
  try {
    const result = yield filterFetchStatusload(
      API_URL_TRANSACTION + 'getMappingRisk',
      {
        method: 'POST',
        headers: API_HEADERS_COMMON,
        body: JSON.stringify(action.data),
        timeout: API_TIMEOUT,
      },
    );
    yield put({
      type: GET_MAPPING_RISK_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_MAPPING_RISK_ERROR,
      error: error,
    });
  }
}

function* getMpInstallmentCalc(action) {
  try {
    const result = yield filterFetchStatus(
      API_URL_TRANSACTION + 'getInstallmentCalc',
      {
        method: 'POST',
        headers: API_HEADERS_CORE,
        body: JSON.stringify(action.data),
        timeout: API_TIMEOUT,
      },
    );
    yield put({
      type: GET_MP_INSTALLMENT_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_MP_INSTALLMENT_ERROR,
      error: error,
    });
  }
}

function* getOTR(action) {
  try {
    const result = yield filterFetch(API_URL_COMMON + 'getOTRPrice', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_OTR_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_OTR_ERROR,
      error: error,
    });
  }
}

function* getJenis(action) {
  try {
    const result = yield filterFetchCore(API_URL_TRANSACTION + 'getJenis', {
      method: 'POST',
      headers: API_HEADERS_CORE,
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });

    yield put({
      type: GET_JENIS_SUCCESS,
      result: result,
    });

  } catch (error) {
    yield put({
      type: GET_JENIS_ERROR,
      error: error,
    });
  }
}

function* getMerk(action) {
  try {
    const result = yield filterFetchCore(API_URL_TRANSACTION + 'getMerk', {
      method: 'POST',
      headers: API_HEADERS_CORE,
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_MERK_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_MERK_ERROR,
      error: error,
    });
  }
}

export function* watchFetchRegion() {
  yield takeLatest(GET_REGION_PROCESS, getRegion);
}

export function* watchFetchMappingRisk() {
  yield takeLatest(GET_MAPPING_RISK_PROCESS, getMappingRisk);
}

export function* watchFetchInstallmentCalc() {
  yield takeLatest(GET_MP_INSTALLMENT_PROCESS, getMpInstallmentCalc);
}

export function* watchFetchGetOTR() {
  yield takeLatest(GET_OTR_PROCESS, getOTR);
}


export function* watchFetchJenis() {
  yield takeLatest(GET_JENIS_PROCESS, getJenis);
}

export function* watchFetchMerk() {
  yield takeLatest(GET_MERK_PROCESS, getMerk);
}