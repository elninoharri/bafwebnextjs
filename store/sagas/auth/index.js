import { put, takeLatest } from "redux-saga/effects";
import {
    CHECK_TOKEN_SUCCESS, CHECK_TOKEN_ERROR, CHECK_TOKEN_PROCESS,
    REFRESH_TOKEN_SUCCESS, REFRESH_TOKEN_ERROR, REFRESH_TOKEN_PROCESS,
    LOGIN_PROCESS, LOGIN_SUCCESS, LOGIN_ERROR, REGISTER_SUCCESS, REGISTER_ERROR, REGISTER_PROCESS
} from '../../actions/auth'
import { filterFetch, filterFetchToken } from '../../../utils/apiFetch'
import {
    API_URL_USR_MNGMNT,
    API_TIMEOUT,
    API_KEY,
    API_URL_WEB,
    API_URL_PRIVATE_DEV,
    API_URL_PUBLIC_DEV,
} from './../../../utils/constant'

function* checkToken(action) {
    try {
        const result = yield filterFetchToken(API_URL_PRIVATE_DEV + 'token/check', {
        // const result = yield filterFetchToken(API_URL_WEB + 'gate/user/token/check', {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${action.token}`,
                "Content-type": "application/json",
                "x-api-key": API_KEY
            },
            timeout: API_TIMEOUT
        });
        yield put({
            type: CHECK_TOKEN_SUCCESS,
            result: result
        });
    } catch (error) {
        yield put({
            type: CHECK_TOKEN_ERROR,
            error: error
        });
    }
}

function* refreshToken(action) {
    try {
        const result = yield filterFetch(API_URL_PRIVATE_DEV + 'token/refresh', {
        // const result = yield filterFetch(API_URL_WEB + 'gate/user/token/refresh', {
            method: 'GET',
            headers: {
                "Authorization": `Bearer ${action.token}`,
                "Content-type": "application/json",
            },
            timeout: API_TIMEOUT
        });
        yield put({
            type: REFRESH_TOKEN_SUCCESS,
            result: result
        });
    } catch (error) {
        yield put({
            type: REFRESH_TOKEN_ERROR,
            error: error
        });
    }
}

function* login(action) {
    try {
        const result = yield filterFetch(API_URL_PUBLIC_DEV + 'login', {
        // const result = yield filterFetch(API_URL_WEB + 'gate/user/login', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                'x-api-key': API_KEY,
            },
            timeout: API_TIMEOUT,
            body: JSON.stringify(action.data),
        });
        yield put({
            type: LOGIN_SUCCESS,
            result: result,
        });
    } catch (error) {
        yield put({
            type: LOGIN_ERROR,
            error: error,
        });
    }
}

function* register(action) {
    try {
        const result = yield filterFetch(API_URL_PUBLIC_DEV + 'register', {
        // const result = yield filterFetch(API_URL_WEB + 'gate/user/register', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
                'x-api-key': API_KEY,
            },
            timeout: API_TIMEOUT,
            body: JSON.stringify(action.data),
        });
        yield put({
            type: REGISTER_SUCCESS,
            result: result,
        });
        console.log('resultsaga', result);

    } catch (error) {
        console.log('errorsaga', error);
        yield put({
            type: REGISTER_ERROR,
            error: error,
        });
    }
}

export function* watchCheckToken() {
    yield takeLatest(CHECK_TOKEN_PROCESS, checkToken);
}

export function* watchRefreshToken() {
    yield takeLatest(REFRESH_TOKEN_PROCESS, refreshToken);
}

export function* watchLogin() {
    yield takeLatest(LOGIN_PROCESS, login);
}

export function* watchRegister() {
    yield takeLatest(REGISTER_PROCESS, register);

}