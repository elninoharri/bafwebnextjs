import { takeLatest, all, put } from "@redux-saga/core/effects";
import { filterFetch, filterFetchCore } from "../../../utils/apiFetch";
import { API_HEADERS_CORE, API_TIMEOUT, API_URL_SANDIAWEB, API_URL_WEB } from "../../../utils/constant";
import { GET_CITY_ERROR, GET_CITY_PROCESS, GET_CITY_SUCCESS, GET_KECAMATAN_ERROR, GET_KECAMATAN_PROCESS, GET_KECAMATAN_SUCCESS, GET_KELURAHAN_ERROR, GET_KELURAHAN_PROCESS, GET_KELURAHAN_SUCCESS, GET_PENGAJUAN_ERROR, GET_PENGAJUAN_PROCESS, GET_PENGAJUAN_SUCCESS, GET_PROVINCE_ERROR, GET_PROVINCE_PROCESS, GET_PROVINCE_SUCCESS } from "../../actions/application";


function* getProvince(action) {
  try {
    const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getProvince', {
      method: 'POST',
      headers: API_HEADERS_CORE,
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data)
    });
    yield put({
      type: GET_PROVINCE_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: GET_PROVINCE_ERROR,
      error: error
    });
  }
}

function* getCity(action) {
  try {
    const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getCity', {
      method: 'POST',
      headers: API_HEADERS_CORE,
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data)
    });
    yield put({
      type: GET_CITY_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: GET_CITY_ERROR,
      error: error
    });
  }
}

function* getKecamatan(action) {
  try {
    const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getKecamatan', {
      method: 'POST',
      headers: API_HEADERS_CORE,
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data)
    });
    yield put({
      type: GET_KECAMATAN_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: GET_KECAMATAN_ERROR,
      error: error
    });
  }
}

function* getKelurahan(action) {
  try {
    const result = yield filterFetchCore(API_URL_SANDIAWEB + 'core/getKelurahan', {
      method: 'POST',
      headers: API_HEADERS_CORE,
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data)
    });
    yield put({
      type: GET_KELURAHAN_SUCCESS,
      result: result
    });
  } catch (error) {
    yield put({
      type: GET_KELURAHAN_ERROR,
      error: error
    });
  }
}

function* getPengajuan(action) {
  try {
    const result = yield filterFetch(API_URL_WEB + 'sendemail/leads', {
      method: 'POST',
      // headers: {
      //   "Authorization": `Bearer ${action.token}`,
      //   'Content-type': 'application/json',
      //   'x-api-key': API_KEY,
      // },
      headers: {
        'Content-type': 'application/json'
      },
      timeout: API_TIMEOUT,
      body: JSON.stringify(action.data),
    });
    yield put({
      type: GET_PENGAJUAN_SUCCESS,
      result: result,
    });
  } catch (error) {
    yield put({
      type: GET_PENGAJUAN_ERROR,
      error: error,
    });
  }
}

export function* watchFetchProvince() {
  yield takeLatest(GET_PROVINCE_PROCESS, getProvince);
}

export function* watchFetchCity() {
  yield takeLatest(GET_CITY_PROCESS, getCity);
}

export function* watchFetchKecamatan() {
  yield takeLatest(GET_KECAMATAN_PROCESS, getKecamatan);
}

export function* watchFetchKelurahan() {
  yield takeLatest(GET_KELURAHAN_PROCESS, getKelurahan);
}

export function* watchFetchGetPengajuan() {
  yield takeLatest(GET_PENGAJUAN_PROCESS, getPengajuan);
}