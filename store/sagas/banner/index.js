import { all, put, takeLatest } from "redux-saga/effects";
import {
	GET_BANNER_PROCCESS,
	GET_BANNER_SUCCESS,
	GET_BANNER_ERROR,
	GET_VOUCHER_CODE_PROCESS,
	GET_VOUCHER_CODE_SUCCESS,
	GET_VOUCHER_CODE_ERROR
} from '../../actions/banner'
import { filterFetch, filterFetchBanner, filterFetchCommon } from '../../../utils/apiFetch'
import {
	API_URL_USR_MNGMNT,
	API_HEADERS_USRMGMT,
	API_TIMEOUT,
	API_KEY,
	API_URL_COMMON_WEB,
	API_URL_WEB,
	API_URL_PRIVATE_DEV,
} from './../../../utils/constant'

function* getBanner(action) {
	try {
		const result = yield filterFetchBanner(API_URL_WEB + 'getListBannerWeb', {
			method: 'POST',
			headers: {
				'Content-type': 'application/json'
				// 'x-api-key': API_KEY,
			},
			//   timeout: API_TIMEOUT,
			body: JSON.stringify(action.data)

		});
		yield put({
			type: GET_BANNER_SUCCESS,
			result: result,
		});
	} catch (error) {
		yield put({
			type: GET_BANNER_ERROR,
			error: error,
		});
	}
}

function* getVoucherCode(action) {
	try {
		const result = yield filterFetch(API_URL_PRIVATE_DEV + 'voucher/code', {
		// const result = yield filterFetch(API_URL_WEB + 'gate/voucher/code', {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${action.token}`,
				'Content-type': 'application/json',
				'x-api-key': API_KEY,
			},
			timeout: API_TIMEOUT,
			body: JSON.stringify(action.data),
		});
		yield put({
			type: GET_VOUCHER_CODE_SUCCESS,
			result: result,
		});
	} catch (error) {
		yield put({
			type: GET_VOUCHER_CODE_ERROR,
			error: error,
		});
	}
}

export function* watchFetchBanner() {
	yield takeLatest(GET_BANNER_PROCCESS, getBanner);
}

export function* watchGetVoucherCode() {
	yield takeLatest(GET_VOUCHER_CODE_PROCESS, getVoucherCode);
}