import { takeLatest, put } from 'redux-saga/effects';
import { DETAIL_USER_ERROR, DETAIL_USER_PROCESS, DETAIL_USER_SUCCESS } from '../../actions';
import { filterFetch } from './../../../utils/apiFetch';
import { API_KEY, API_TIMEOUT, API_URL_PRIVATE_DEV, API_URL_USR_MNGMNT } from './../../../utils/constant';

function* detailUser(action) {
    try {
        // const result = yield filterFetch(API_URL_USR_MNGMNT + 'getDetailUser', {
        const result = yield filterFetch(API_URL_PRIVATE_DEV + 'account/detail', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'x-api-key': API_KEY,
                Authorization: 'Bearer ' + action.token,
            },
            timeout: API_TIMEOUT,
        });
        yield put({
            type: DETAIL_USER_SUCCESS,
            result: result,
        });
    } catch (error) {
        yield put({
            type: DETAIL_USER_ERROR,
            error: error,
        });
    }
}


export function* watchDetailUser() {
    yield takeLatest(DETAIL_USER_PROCESS, detailUser);
}