import { combineReducers } from "redux";
import {
	getBanner,
	newsData,
	getVoucherCode
} from '../reducers/banner';
import {
	getRegion,
	getMappingRisk,
	getMpInstallmentCalc,
	getOTR,
	setSelectedLob,
	setSelectedProduct,
	getJenis,
	getMerk
} from '../reducers/homepage';

import {
	getProvince,
	getCity,
	getKecamatan,
	getKelurahan,
	simulationData,
	getPengajuan
} from '../reducers/application';

import {
	setToken,
	checkToken,
	refreshToken,
	login,
	register
} from '../reducers/auth';

import {
	detailUser
} from '../reducers/account';

const rootReducer = combineReducers({
	getBanner,
	getRegion,
	getMappingRisk,
	getMpInstallmentCalc,
	getOTR,
	setSelectedLob,
	setSelectedProduct,
	getJenis,
	getMerk,
	getProvince,
	getCity,
	getKecamatan,
	getKelurahan,
	simulationData,
	getPengajuan,
	newsData,
	setToken,
	checkToken,
	refreshToken,
	login,
	register,
	detailUser,
	getVoucherCode
	// form: formReducer
});

export default rootReducer;