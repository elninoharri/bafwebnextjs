// import { HYDRATE } from "next-redux-wrapper";
// import * as t from "../../types";

import {
    GET_BANNER_PROCCESS,
    GET_BANNER_SUCCESS,
    GET_BANNER_ERROR,
    NEWS_DATA,
    GET_VOUCHER_CODE_PROCESS,
    GET_VOUCHER_CODE_SUCCESS,
    GET_VOUCHER_CODE_ERROR,
} from '../../actions/banner'

const initState = {
    result: [],
    loading: false
}

const initStates = {
    result: [],
    loading: false
};

const initStateNews = {
    data: []
};

export function getBanner(state = initState, action) {
    switch (action.type) {
        case GET_BANNER_PROCCESS:
            return {
                ...initState,
                loading: true,
                result: [],
                error: null,
            };
        case GET_BANNER_SUCCESS:
            return {
                ...state,
                result: action.result,
                loading: false,
                error: null,
            };
        case GET_BANNER_ERROR:
            return {
                ...state,
                error: action.error,
                loading: false,
                result: [],
            };
        default:
            return state;
    }
}

export function newsData(state = initStateNews, action) {
    switch (action.type) {
        case NEWS_DATA:
            const data = action.payload;
            return {
                ...state,
                data
            };
        default:
            return state;
    }
}


export function getVoucherCode(state = initStates, action) {
    switch (action.type) {
        case GET_VOUCHER_CODE_PROCESS:
            return {
                ...initStates,
                loading: true,
                result: null,
                error: null,
            };
        case GET_VOUCHER_CODE_SUCCESS:
            return {
                ...state,
                result: action.result,
                loading: false,
                error: null,
            };
        case GET_VOUCHER_CODE_ERROR:
            return {
                ...state,
                error: action.error,
                loading: false,
                result: null,
            };
        default:
            return state;
    }
}