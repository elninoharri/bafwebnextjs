import {
  /* GET REGION */
  GET_REGION_PROCESS,
  GET_REGION_SUCCESS,
  GET_REGION_ERROR,

  /* GET MAPPING RISK */
  GET_MAPPING_RISK_PROCESS,
  GET_MAPPING_RISK_SUCCESS,
  GET_MAPPING_RISK_ERROR,

  /* GET MP INSTALLMENT */
  GET_MP_INSTALLMENT_PROCESS,
  GET_MP_INSTALLMENT_SUCCESS,
  GET_MP_INSTALLMENT_ERROR,

  /* GET PENGAJUAN DATA */
  GET_PENGAJUAN_PROCESS,
  GET_PENGAJUAN_SUCCESS,
  GET_PENGAJUAN_ERROR,

  /* GET OTR */
  GET_OTR_PROCESS,
  GET_OTR_SUCCESS,
  GET_OTR_ERROR,

  /* GET TIPE BARANG */
  GET_JENIS_PROCESS,
  GET_JENIS_SUCCESS,
  GET_JENIS_ERROR,

  /* GET MERK BARANG */
  GET_MERK_PROCESS,
  GET_MERK_SUCCESS,
  GET_MERK_ERROR,

  SELECTED_LOB,
  SELECTED_PRODUCT,
} from '../../actions/homepage'

const initState = {
  result: [],
  loading: false
}

const initStateOTR = {
  result: null,
  loading: false
};

const initStateLob = {
  selectedLob: 'NMC'
}

const initStateProduct = {
  selectedProduct: 'NMC'
}


export function getRegion(state = initState, action) {
  switch (action.type) {
    case GET_REGION_PROCESS:
      return {
        ...initState,
        loading: true,
        result: [],
        error: null,
      };
    case GET_REGION_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };

    case GET_REGION_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: [],
      };
    default:
      return state;
  }
}

export function getMappingRisk(state = initState, action) {
  switch (action.type) {
    case GET_MAPPING_RISK_PROCESS:
      return {
        ...initState,
        loading: true,
        result: [],
        error: null,
      };
    case GET_MAPPING_RISK_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_MAPPING_RISK_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: [],
      };
    default:
      return state;
  }
}

export function getMpInstallmentCalc(state = initState, action) {
  switch (action.type) {
    case GET_MP_INSTALLMENT_PROCESS:
      return {
        ...initState,
        loading: true,
        result: [],
        error: null,
      };
    case GET_MP_INSTALLMENT_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_MP_INSTALLMENT_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: [],
      };
    default:
      return state;
  }
}

export function getOTR(state = initStateOTR, action) {
  switch (action.type) {
    case GET_OTR_PROCESS:
      return {
        ...initState,
        loading: true,
        result: null,
        error: null,
      };
    case GET_OTR_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_OTR_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: null,
      };
    default:
      return state;
  }
}

export function getJenis(state = initState, action) {
  switch (action.type) {
    case GET_JENIS_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case GET_JENIS_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case GET_JENIS_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}

export function getMerk(state = initState, action) {
  switch (action.type) {
    case GET_MERK_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case GET_MERK_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case GET_MERK_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}


export function setSelectedLob(state = initStateLob, action) {
  switch (action.type) {
    case SELECTED_LOB:
      const selectedLob = action.payload;
      return {
        ...state,
        selectedLob

      };
    default:
      return state;
  }
}

export function setSelectedProduct(state = initStateProduct, action) {
  switch (action.type) {
    case SELECTED_PRODUCT:
      const selectedProduct = action.payload;
      return {
        ...state,
        selectedProduct

      };
    default:
      return state;
  }
}
