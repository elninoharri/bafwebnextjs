import {
    TOKEN_SETTER,
    CHECK_TOKEN_PROCESS, CHECK_TOKEN_SUCCESS, CHECK_TOKEN_ERROR,
    REFRESH_TOKEN_PROCESS, REFRESH_TOKEN_SUCCESS, REFRESH_TOKEN_ERROR, 
    LOGIN_PROCESS, LOGIN_SUCCESS, LOGIN_ERROR, 
    REGISTER_PROCESS, REGISTER_SUCCESS, REGISTER_ERROR
} from "../../actions/auth";

const initStateST = {
    data: []
};

const initState = {
    result: null,
    loading: false
}

export function setToken(state = initStateST, action) {
    switch (action.type) {
        case TOKEN_SETTER:
            const data = action.data;
            window.sessionStorage.setItem('userToken', data.userToken);
            window.sessionStorage.setItem('userGroupID', data.userGroupID);

            return {
                ...state,
                data
            };
        default:
            return state;
    }
}

export function checkToken(state = initState, action) {
    switch (action.type) {
        case CHECK_TOKEN_PROCESS:
            return {
                ...initState,
                loading: true,
                result: null,
                error: null,
            };
        case CHECK_TOKEN_SUCCESS:
            return {
                ...state,
                result: action.result,
                loading: false,
                error: null
            };
        case CHECK_TOKEN_ERROR:
            return {
                ...state,
                error: action.error,
                loading: false,
                result: null
            };
        default:
            return state;
    }
}

export function refreshToken(state = initState, action) {
    switch (action.type) {
        case REFRESH_TOKEN_PROCESS:
            return {
                ...initState,
                loading: true,
                result: null,
                error: null,
            };
        case REFRESH_TOKEN_SUCCESS:
            return {
                ...state,
                result: action.result,
                loading: false,
                error: null
            };
        case REFRESH_TOKEN_ERROR:
            return {
                ...state,
                error: action.error,
                loading: false,
                result: null
            };
        default:
            return state;
    }
}

export function login(state = initState, action) {
    switch (action.type) {
        case LOGIN_PROCESS:
            return {
                ...initState,
                loading: true,
                result: null,
                error: null,
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                result: action.result,
                loading: false,
                error: null,
            };
        case LOGIN_ERROR:
            return {
                ...state,
                error: action.error,
                loading: false,
                result: null,
            };
        default:
            return state;
    }
}

export function register(state = initState, action) {
    switch (action.type) {
      case REGISTER_PROCESS:
        return {
          ...initState,
          loading: true,
          result: null,
          error: null,
        };
      case REGISTER_SUCCESS:
        return {
          ...state,
          result: action.result,
          loading: false,
          error: null,
        };
      case REGISTER_ERROR:
        return {
          ...state,
          error: action.error,
          loading: false,
          result: null,
        };
      default:
        return state;
    }
  }