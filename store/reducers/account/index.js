import { DETAIL_USER_ERROR, DETAIL_USER_PROCESS, DETAIL_USER_SUCCESS } from "../../actions";

const initState = {
    result: null,
    loading: false,
};

export function detailUser(state = initState, action) {
    switch (action.type) {
        case DETAIL_USER_PROCESS:
            return {
                ...initState,
                loading: true,
                result: null,
                error: null,
            };
        case DETAIL_USER_SUCCESS:
            return {
                ...state,
                result: action.result,
                loading: false,
                error: null,
            };
        case DETAIL_USER_ERROR:
            return {
                ...state,
                error: action.error,
                loading: false,
                result: null,
            };
        default:
            return state;
    }
}