import { GET_CITY_ERROR, GET_CITY_PROCESS, GET_CITY_SUCCESS, GET_KECAMATAN_ERROR, GET_KECAMATAN_PROCESS, GET_KECAMATAN_SUCCESS, GET_KELURAHAN_ERROR, GET_KELURAHAN_PROCESS, GET_KELURAHAN_SUCCESS, GET_PENGAJUAN_ERROR, GET_PENGAJUAN_PROCESS, GET_PENGAJUAN_SUCCESS, GET_PROVINCE_ERROR, GET_PROVINCE_PROCESS, GET_PROVINCE_SUCCESS, SIMULATION_DATA } from "../../actions/application";

const initState = {
  result: [],
  loading: false
};

const initStateApp = {
  data: []
};

export function getProvince(state = initState, action) {
  switch (action.type) {
    case GET_PROVINCE_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case GET_PROVINCE_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case GET_PROVINCE_ERROR:
      return {
        ...state,
        error: action.error,
        result: [],
        loading: false
      };
    default:
      return state;
  }
}


export function getCity(state = initState, action) {
  switch (action.type) {
    case GET_CITY_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case GET_CITY_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case GET_CITY_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}


export function getKecamatan(state = initState, action) {
  switch (action.type) {
    case GET_KECAMATAN_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case GET_KECAMATAN_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case GET_KECAMATAN_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}


export function getKelurahan(state = initState, action) {
  switch (action.type) {
    case GET_KELURAHAN_PROCESS:
      return {
        ...state,
        loading: true,
        result: [],
        error: null
      };
    case GET_KELURAHAN_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null
      };
    case GET_KELURAHAN_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: []
      };
    default:
      return state;
  }
}

export function simulationData(state = initStateApp, action) {
  switch (action.type) {
    case SIMULATION_DATA:
      const data = action.payload;
      return {
        ...state,
        data
      };
    default:
      return state;
  }
}

export function getPengajuan(state = initState, action) {
  switch (action.type) {
    case GET_PENGAJUAN_PROCESS:
      return {
        ...initState,
        loading: true,
        result: [],
        error: null,
      };
    case GET_PENGAJUAN_SUCCESS:
      return {
        ...state,
        result: action.result,
        loading: false,
        error: null,
      };
    case GET_PENGAJUAN_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        result: [],
      }
    default:
      return state;
  }
}