import { PageTitle, Layout, ApplicationForm } from "../../components";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

function AppForm() {
    const { t } = useTranslation("common");

    return (
        <Layout>
            <PageTitle title={t('applicationForm')} />
            <ApplicationForm />
        </Layout>
    )
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'footer', 'alerts', 'common']),
    },
})

export default AppForm;