import { Layout, CompanyProfile, AboutBAF, Slider } from "../../components";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { withTranslation } from 'next-i18next';

function AboutUs() {
    return (
        <Layout>
            <AboutBAF />
            <section id="company-profile">
                <CompanyProfile />
            </section>
        </Layout>
    )
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'aboutus', 'footer', 'common', 'directors', 'commissioners', 'secretary']),
    },
})

export default AboutUs;