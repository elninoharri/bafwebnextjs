import { PageTitle, Layout, ProductTab } from "../../components";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

function Product() {
    return (
        <Layout>
            <PageTitle title={'PRODUK'} />
            <ProductTab />
        </Layout>
    )
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'footer', 'simulation', 'product', 'alerts']),
    },
})

export default Product;