import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { Login } from '../../components';
import { HalfLayout } from "../../components/HalfLayout";

function LoginPage() {
    return (
        <HalfLayout>
            <Login/>
        </HalfLayout>
    )
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'footer', 'login', 'alerts','common']),
    },
})

export default LoginPage;