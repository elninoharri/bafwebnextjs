import '../styles/fonts.css';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { wrapper } from "./../store";
import Router from 'next/router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import { appWithTranslation } from 'next-i18next';
import { PersistGate } from 'redux-persist/integration/react';
import { useStore } from 'react-redux';

Router.events.on('routeChangeStart', () => NProgress.start()); 
Router.events.on('routeChangeComplete', () => NProgress.done()); 
Router.events.on('routeChangeError', () => NProgress.done());

function MyApp({ Component, pageProps }) {
  const store = useStore();
  return <PersistGate persistor={store.__PERSISTOR} loading={null}><Component {...pageProps} /></PersistGate>
}

export default wrapper.withRedux(appWithTranslation(MyApp));