import { useRouter } from "next/router";
import { Layout, NewsDetail } from "../../../components";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

function EventDetail() {
    const router = useRouter();
    const { id } = router.query;
    return (
        <Layout>
            <NewsDetail bannerType={'EVENT'}/>
        </Layout>
    )
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'aboutus', 'footer', 'simulation']),
    },
})

export async function getStaticPaths() {
    return {
        paths: [
            { params: { id: "5f0f502b9cb9363990f3de6c", newstype: "5f0f502b9cb9363990f3de6c", title: "title" } }
        ],
        fallback: true
    }
}

export default EventDetail;
