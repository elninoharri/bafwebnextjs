import { PageTitle, Layout, PromoTab } from "../../components";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

function Event() {
    return (
        <Layout>
            <PageTitle title={'EVENT'} />
            <PromoTab
                bannerType={'EVENT'}
                detailPage={'event'} />
        </Layout>
    )
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'footer', 'simulation', 'alerts', 'common']),
    },
})

export default Event;