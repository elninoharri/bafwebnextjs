import HomePage from './home'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';


export default function index() {
  return (
    <HomePage/>
  )
}

export const getStaticProps = async ({ locale }) => ({
  props: {
      ...await serverSideTranslations(locale, ['navbar', 'simulation', 'footer', 'aboutus', 'home', 'alerts', 'common']),
  },
})