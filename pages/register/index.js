import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { HalfLayout } from "../../components/HalfLayout";
import { RegistrationForm } from "../../components";

function RegisterPage() {
    return (
        <HalfLayout>
            <RegistrationForm/>
        </HalfLayout>
    )
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'footer', 'common', 'alerts']),
    },
})

export default RegisterPage;