import { PageTitle, Layout } from "../../components";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

function ContactUs() {
    return (
        <Layout>
            <PageTitle title={'HUBUNGI KAMI'} />
            <div>COMING SOON !!!</div>
        </Layout>
    )
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'footer']),
    },
})

export default ContactUs;