import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { CompanyProfile, Layout, LinkCard, SimulationFormCar, SimulationFormNMC, SimulationFormMP, SimulationFormSyana, Slider, SliderCard, Tagline } from '../../components';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

function HomePage() {
    const selectedLob = useSelector((state) => state.setSelectedLob.selectedLob);

    return (
        <Layout>
            <Slider />
            <Tagline />
            <SliderCard />
            <LinkCard />
            {
                selectedLob === 'NMC' ?
                    <SimulationFormNMC /> :
                    selectedLob === 'CAR' ?
                        <SimulationFormCar /> :
                        selectedLob === 'MP' ?
                            <SimulationFormMP /> :
                            selectedLob === 'SYANA' ?
                                <SimulationFormSyana />
                                : null
            }
            <section id='company-profile'>
                <CompanyProfile />
            </section>
        </Layout>
    )
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'simulation', 'footer', 'aboutus', 'home', 'alerts', 'common']),
    },
})

export default HomePage;