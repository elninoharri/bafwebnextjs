import { Layout, PageTitle } from "../../components";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';


function Lounge() {
    return (
        <Layout>
            <PageTitle title={'BAF LOUNGE'} />
            <div>COMING SOON !!!</div>
        </Layout>
    )
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'footer']),
    },
})

export default Lounge;