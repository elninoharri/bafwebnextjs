import { Layout, PageTitle } from "../../components";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { CompanyProfile } from "../../components/CompanyProfile";
import { Articles } from "../../components/Articles";


function Article() {
    return (
        <Layout>
            <Articles/>
            <section id='company-profile'>
                <CompanyProfile/>
            </section>
        </Layout>
    )
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'footer', 'common', 'aboutus']),
    },
})

export default Article;