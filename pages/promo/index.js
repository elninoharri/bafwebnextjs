import { PageTitle, Layout, PromoTab } from "../../components";
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

function Promo() {
    return (
        <Layout>
            <PageTitle title={'PROMO'} />
            <PromoTab
                bannerType={'PROMOTION'} 
                detailPage={'promo'}/>
        </Layout>
    )
}

export const getStaticProps = async ({ locale }) => ({
    props: {
        ...await serverSideTranslations(locale, ['navbar', 'footer', 'simulation', 'alerts', 'common']),
    },
})

export default Promo;