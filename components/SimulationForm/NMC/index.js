import React, { useEffect, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { RHFInput } from 'react-hook-form-input';
import Select from 'react-select'
import { Card, Container, Form, Button, Col, Row } from "react-bootstrap";
import { useDispatch, useSelector } from 'react-redux';
import { getOTR } from '../../../store/actions/homepage'
import { fdeFormatCurrency, substringCircum, substringSecondCircum, formatCurrency } from '../../../utils/utilization'
import NMC from '../nmc.json'
import swal from 'sweetalert'
import { useRouter } from 'next/router';
import { simulationData } from '../../../store/actions/application';
import { useTranslation } from 'next-i18next'

export function SimulationFormNMC() {
    const { t } = useTranslation(['simulation','alerts']);

    const dispatch = useDispatch();
    const router = useRouter();

    const getOTRResult = useSelector((state) => state.getOTR.result ? state.getOTR.result.OTRPrice : 0);
    const getOTRLoading = useSelector((state) => state.getOTR.loading);
    const getOTRError = useSelector((state) => state.getOTR.error);

    const [NMCTipe, setNMCTipe] = useState(NMC.Tipe)
    const [NMCTenor, setNMCTenor] = useState(NMC.Tenor)
    const [NMCDp, setNMCDp] = useState(NMC.Dp)
    const [LOB, setLOB] = useState("NMC")
    const [tipeMotor, setTipeMotor] = useState('')
    const [hargaBarang, setHargaBarang] = useState(0)
    const [appData, setAppData] = useState({ merek: '', nominal: '', dp: '', tenor: '', installmentAmount: false });

    useEffect(() => {
        if (tipeMotor != '') {
            dispatch(
                getOTR(
                    {
                        LobCode: LOB,
                        MerkCode: "",
                        TipeCode: tipeMotor,
                        ManufactureYear: ""
                    }
                )
            )
        }
    },
        [tipeMotor]
    )

    useEffect(() => {
        if (getOTRError) {
            swal({ title: t('alerts:warning'), text: t('alerts:warningText1'), icon: 'error', timer: 3000 });
        }
        else if (getOTRResult && tipeMotor != '') {
            swal({ title: t('alerts:success'), text: t('alerts:successText2'), icon: 'success', timer: 3000 });
            setHargaBarang(getOTRResult)
        }
    }, [getOTRResult, getOTRError])

    const onTipeChange = async (value) => {
        if (value) {
            setTipeMotor(value.value)
        }
    }

    const onSubmit = (data) => {
        let merekBarang = data.formMotor.value;
        let otrPrice = parseFloat(hargaBarang)
        let dpAmount = parseFloat(data.formUangMuka.value) * otrPrice
        let tenor = substringCircum(data.formJangkaWaktu.value)
        let rate = substringSecondCircum(data.formJangkaWaktu.value)
        let calcResult =
            ((otrPrice - dpAmount) * (1 + parseFloat(rate)))
            /
            parseFloat(tenor)
        swal({ title: t('alerts:success'), text: t('alerts:successText'), icon: 'success', timer: 3000 });
        setAppData({ merek: merekBarang, nominal: otrPrice, dp: data.formUangMuka.value, tenor: tenor, installmentAmount: Math.round(calcResult) });
    }

    const goToApplicationForm = () => {
        if (appData.installmentAmount) {
            let objSimulasi = {
                jenisBarang: LOB,
                merekBarang: appData.merek,
                nominal: appData.nominal,
                dp: appData.dp,
                tenor: appData.tenor
            }
            dispatch(simulationData(objSimulasi));
            router.push({ pathname: '/application-form' });
        } else {
            swal({ title: t('alerts:info'), text: t('alerts:infoText'), icon: 'warning', timer: 3000 });
        }
    }

    const { register, handleSubmit, setValue, errors, control } = useForm();

    return (
        <Container className="section-container">
            <Row>
                <Col>
                    <Card className="simul-card mx-auto column-margin">
                        <Card.Body className="txt-black">
                            <Form onSubmit={handleSubmit(onSubmit)}>
                                <Form.Group >
                                    <Form.Label>{t('chooseMotor')}</Form.Label>
                                    <RHFInput
                                        as={<Select options={NMCTipe} placeholder={t('select')} instanceId={'tipeNMC'} />}
                                        name="formMotor"
                                        setValue={setValue}
                                        register={register}
                                        rules={{ required: true }}
                                        onChange={(newValue) => onTipeChange(newValue)}
                                    />
                                    {errors.formMotor && <p className="txt-error">{t('nmcError')}</p>}
                                </Form.Group>
                                <Form.Group >
                                    <Form.Label>{t('motorPrice')}</Form.Label>
                                    <Form.Control ref={register} id="formHarga" name="formHarga" type="text" placeholder="" value={fdeFormatCurrency(hargaBarang)} readOnly={true} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>{t('timePeriod')}</Form.Label>
                                    <RHFInput
                                        as={<Select options={NMCTenor} placeholder={t('select')} instanceId={'tenorNMC'} />}
                                        register={register}
                                        name="formJangkaWaktu"
                                        setValue={setValue}
                                        rules={{ required: true }}
                                    />
                                    {errors.formJangkaWaktu && <p className="txt-error">{t('tenorError')}</p>}
                                </Form.Group>
                                <Form.Group >
                                    <Form.Label>{t('downPayment')}</Form.Label>
                                    <RHFInput
                                        as={<Select options={NMCDp} placeholder={t('select')} instanceId={'DPNMC'} />}
                                        register={register}
                                        name="formUangMuka"
                                        setValue={setValue}
                                        rules={{ required: true }}
                                    />
                                    {errors.formUangMuka && <p className="txt-error">{t('dpError')}</p>}

                                    {/* <Select value={NMCDp.find(c => c.value)} ref={register} options={NMCDp} placeholder={t('select')} id="formUangMuka" name="formUangMuka" /> */}
                                </Form.Group>
                                <div className="text-center mt-5">
                                    <Button variant="primary" type="Submit" className="bg-blue-baf w-75 font-weight-normal radius-2 p-2" disabled={getOTRLoading ? true : false}>
                                        {t('calculate')}
                                        {getOTRLoading && <span className="spinner-border spinner-border-sm ml-2" role="status" aria-hidden="true"></span>}
                                    </Button>
                                </div>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
                <Col>
                    <Card className="simul-card mx-auto bg-seablue column-margin">
                        <Card.Body>
                            <div className="text-center mt-5">
                                <h5 className="txt-white mb-4">{t('estimation')}</h5>
                                <div className="bg-white txt-blue-baf p-4 mb-4 w-75 radius-1 mx-auto">
                                    <h3 className="font-weight-bold">Rp {appData.installmentAmount ? formatCurrency(appData.installmentAmount.toString()) : ' - '} </h3>
                                    <h4 className="font-weight-bold">/{t('month')}</h4>
                                </div>
                                <div className="w-75 mx-auto txt-white mb-5">
                                    <h6>{t('simulationText')}</h6>
                                </div>
                                <Button variant="primary" onClick={goToApplicationForm} className="bg-white txt-blue-baf w-75 font-weight-bold radius-2 p-2">{t('submitNow')}</Button>
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}