import React, { useEffect, useState } from 'react'
import Select from 'react-select'
import { Card, Container, Form, Button, Col, Row } from "react-bootstrap";
import { useDispatch, useSelector } from 'react-redux';
import CarData from '../car.json';
import { substringCircum, substringSecondCircum, formatCurrency, fdeFormatCurrency } from '../../../utils/utilization';
import { useForm } from 'react-hook-form';
import { RHFInput } from 'react-hook-form-input';
import swal from 'sweetalert';
import { getOTR } from '../../../store/actions/homepage';
import { useRouter } from 'next/router';
import { simulationData } from '../../../store/actions/application';
import { useTranslation } from 'next-i18next';

export function SimulationFormCar() {
    const { t } = useTranslation(['simulation', 'alerts'])
    const dispatch = useDispatch();
    const router = useRouter();

    const getOTRResult = useSelector((state) => state.getOTR.result ? state.getOTR.result.OTRPrice : 0);
    const getOTRLoading = useSelector((state) => state.getOTR.loading);
    const getOTRError = useSelector((state) => state.getOTR.error);

    const [selectedJenis, setSelectedJenis] = useState('');
    const [price, setPrice] = useState(0);
    const [LOB, setLOB] = useState("CAR");
    const [appData, setAppData] = useState({ merek: '', nominal: '', dp: '', tenor: '', installmentAmount: false });

    useEffect(() => {
        if (selectedJenis != '') {
            dispatch(
                getOTR(
                    {
                        LobCode: LOB,
                        MerkCode: selectedJenis,
                        TipeCode: "",
                        ManufactureYear: ""
                    }
                )
            )
        }
    },
        [selectedJenis]
    )

    useEffect(() => {
        if (getOTRError) {
            swal({ title: t('alerts:warning'), text: t('alerts:warningText1'), icon: 'error', timer: 3000 });
        }
        else if (getOTRResult && selectedJenis != '') {
            swal({ title: t('alerts:success'), text: t('alerts:successText2'), icon: 'success', timer: 3000 });
            setPrice(getOTRResult)
        }
    }, [getOTRResult, getOTRError])


    const onChangeJenis = selectedVal => {
        setSelectedJenis(selectedVal.value);
    }

    const onSubmit = (data) => {
        let merekBarang = data.formCar.value;
        let otrPrice = parseFloat(price)
        let dpAmount = parseFloat(data.formUangMuka.value) * otrPrice
        let tenor = substringCircum(data.formJangkaWaktu.value)
        let rate = substringSecondCircum(data.formJangkaWaktu.value)
        let calcResult =
            ((otrPrice - dpAmount) * (1 + parseFloat(rate)))
            /
            parseFloat(tenor)
        swal({ title: t('alerts:success'), text: t('alerts:successText'), icon: 'success', timer: 3000 });
        setAppData({ merek: merekBarang, nominal: otrPrice, dp: data.formUangMuka.value, tenor: tenor, installmentAmount: Math.round(calcResult) });
    }

    const goToApplicationForm = () => {
        if (appData.installmentAmount) {
            let objSimulasi = {
                jenisBarang: LOB,
                merekBarang: appData.merek,
                nominal: appData.nominal,
                dp: appData.dp,
                tenor: appData.tenor
            }
            dispatch(simulationData(objSimulasi));
            router.push({ pathname: '/application-form' });
        } else {
            swal({ title: t('alerts:info'), text: t('alerts:infoText'), icon: 'warning', timer: 3000 });
        }
    }

    const { register, handleSubmit, setValue, errors, clearErrors } = useForm();

    return (
        <Container className="section-container">
            <Row>
                <Col>
                    <Card className="simul-card mx-auto column-margin">
                        <Card.Body className="txt-black">
                            <Form onSubmit={handleSubmit(onSubmit)}>
                                <Form.Group>
                                    <Form.Label>{t('chooseCar')}</Form.Label>
                                    <RHFInput
                                        as={<Select options={CarData.Tipe} placeholder={t('select')} instanceId={'tipeCar'} />}
                                        name="formCar"
                                        setValue={setValue}
                                        register={register}
                                        rules={{ required: true }}
                                        onChange={(newValue) => onChangeJenis(newValue)}
                                    />
                                    {errors.formCar && <p className="txt-error">{t("carError")}</p>}

                                </Form.Group>
                                <Form.Group controlId="formHarga">
                                    <Form.Label>{t('carPrice')}</Form.Label>
                                    <Form.Control ref={register} name="formHarga" type="text" placeholder="" value={fdeFormatCurrency(price)} min={0} readOnly={true} />
                                </Form.Group> 
                                <Row>
                                    <Col lg={12} md={12} xs={12}>
                                        <Form.Group>
                                            <Form.Label>{t('timePeriod')}</Form.Label>
                                            <RHFInput
                                                as={<Select options={CarData.Tenor} placeholder={t('select')} instanceId={'tenorCar'} />}
                                                register={register}
                                                name="formJangkaWaktu"
                                                setValue={setValue}
                                                rules={{ required: true }}
                                            />
                                            {errors.formJangkaWaktu && <p className="txt-error">{t('tenorError')}</p>}
                                        </Form.Group>
                                    </Col>
                                    <Col lg={12} md={12} xs={12}>
                                        <Form.Group>
                                            <Form.Label>{t('downPayment')}</Form.Label>
                                            <RHFInput
                                                as={<Select options={CarData.DP} placeholder={t('select')} instanceId={'DPCar'} />}
                                                register={register}
                                                name="formUangMuka"
                                                setValue={setValue}
                                                rules={{ required: true }}
                                            />
                                            {errors.formUangMuka && <p className="txt-error">{t('dpError')}</p>}
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <div className="text-center mt-5">
                                    <Button variant="primary" className="bg-blue-baf w-75 font-weight-normal radius-2 p-2" type="Submit" disabled={getOTRLoading ? true : false}>
                                        {t('calculate')}
                                        {getOTRLoading && <span className="spinner-border spinner-border-sm ml-2" role="status" aria-hidden="true"></span>}
                                    </Button>
                                </div>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
                <Col>
                    <Card className="simul-card mx-auto bg-seablue column-margin">
                        <Card.Body>
                            <Form>
                                <div className="text-center mt-5">
                                    <h5 className="txt-white mb-4">{t('estimation')}</h5>
                                    <div className="bg-white txt-blue-baf p-4 mb-4 w-75 radius-1 mx-auto">
                                        <h3 className="font-weight-bold">Rp {appData.installmentAmount ? formatCurrency(appData.installmentAmount.toString()) : ' - '}</h3>
                                        <h4 className="font-weight-bold">/{t('month')}</h4>
                                    </div>
                                    <div className="w-75 mx-auto txt-white mb-5">
                                        <h6>{t('simulationText')}</h6>
                                    </div>
                                    <Button variant="primary" className="bg-white txt-blue-baf w-75 font-weight-bold radius-2 p-2" onClick={goToApplicationForm}>{t('submitNow')}</Button>
                                </div>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}