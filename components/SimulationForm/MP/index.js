import React, { useEffect, useState } from 'react'
import Select from 'react-select'
import { Card, Container, Form, Button, Col, Row } from "react-bootstrap";
import { useDispatch, useSelector } from 'react-redux';
import { getJenis, getMerk, getMappingRisk, getMpInstallmentCalc } from '../../../store/actions/homepage';
import { CODE_JAMINAN, ADMIN_TYPE, INSTALLMENT_TYPE } from '../../../utils/constant';
import { substringCircum, substringThirdCircum, formatCurrency } from '../../../utils/utilization';
import { useForm } from 'react-hook-form';
import { RHFInput } from 'react-hook-form-input';
import swal from 'sweetalert';
import { useRouter } from 'next/router';
import { simulationData } from '../../../store/actions/application';
import { useTranslation } from 'react-i18next';

export function SimulationFormMP() {
    const { t } = useTranslation(["simulation", "alerts"]);
    const dispatch = useDispatch();
    const router = useRouter();

    const dataJenis = useSelector((state => state.getJenis.result));
    const dataJenisError = useSelector((state => state.getJenis.error));
    const dataJenisLoading = useSelector((state => state.getJenis.loading));

    const dataMerk = useSelector((state => state.getMerk.result));
    const dataMerkError = useSelector((state => state.getMerk.error));
    const dataMerkLoading = useSelector((state => state.getMerk.loading));

    const dataRisk = useSelector((state => state.getMappingRisk.result));
    const dataRiskError = useSelector((state => state.getMappingRisk.error));
    const dataRiskLoading = useSelector((state => state.getMappingRisk.loading));

    const dataSimulationResult = useSelector((state => state.getMpInstallmentCalc.result));
    const dataSimulationLoading = useSelector((state => state.getMpInstallmentCalc.loading));
    const dataSimulationError = useSelector((state => state.getMpInstallmentCalc.error));

    const [dropdownJenis, setDropdownJenis] = useState([]);
    const [dropdownMerk, setDropdownMerk] = useState([]);
    const [dropdownRisk, setDropdownRisk] = useState({ objTenor: [], objDP: [] });

    const [selectedJenis, setSelectedJenis] = useState('');
    const [price, setPrice] = useState('');
    const [LOB, setLOB] = useState("MP")
    const [appData, setAppData] = useState({ merek: '', nominal: '', dp: '', tenor: '', installmentAmount: false });

    let objJenisData = [];
    let objMerkData = [];
    let objTenorData = [];
    let objDPData = [];

    useEffect(() => { initDropdownJenis() }, [dataJenis]);
    useEffect(() => { initDropdownMerk() }, [dataMerk]);
    useEffect(() => {
        if (dataRiskError) {
            swal({ title: t('alerts:warning'), text: 'Unable to Retrieved Tenor and DP Option', icon: 'error', timer: 3000 });

        } else if (dataRisk.Tenor && dataRisk.DP && selectedJenis != '' && price != '') {
            swal({ title: t('alerts:success'), text: 'Successfully Retrieved Tenor and DP Option', icon: 'success', timer: 3000 });
            initDropDownTenorDP();
        }

    }, [dataRisk]);

    useEffect(() => {
        dispatch(getJenis({ LOB: LOB, Jaminan: substringCircum(CODE_JAMINAN) }));
    }, []);

    useEffect(() => {
        if (selectedJenis != '') {
            dispatch(getMerk({ JaminanJenis: substringCircum(selectedJenis) }));
        }
    }, [selectedJenis])

    useEffect(() => {
        if (dataSimulationError) {
            swal({ title: t('alerts:warning'), text: 'Unable to Retrieved Installment Data, Please try again', icon: 'error', timer: 3000 });
        } else if (dataSimulationResult.AngsuranRp && selectedJenis != '' && price != '') {
            swal({ title: t('alerts:success'), text: t('alerts:successText'), icon: 'success', timer: 3000 });

            setAppData(prevState => {
                return { ...prevState, installmentAmount: dataSimulationResult.AngsuranRp };
            })
        }
    }, [dataSimulationResult])

    const initDropdownJenis = () => {
        if (dataJenis) {
            dataJenis.map(function (val) {
                const obj = new Object()
                obj['value'] = val.Code + '^' + val.Descr + '^' + val.RiskTypeId
                obj['label'] = val.Descr
                objJenisData.push(obj)
            })
            setDropdownJenis(objJenisData);
        }
    }

    const initDropdownMerk = () => {
        if (dataMerk) {
            dataMerk.map(function (val) {
                const obj = new Object()
                obj['value'] = val.Code
                obj['label'] = val.Descr
                objMerkData.push(obj)
            })
            setDropdownMerk(objMerkData);
        }
    }

    const initDropDownTenorDP = () => {
        if (dataRisk.Tenor) {
            dataRisk.Tenor.map(function (val) {
                const obj = new Object()
                obj['value'] = val
                obj['label'] = val + ' Bulan'
                objTenorData.push(obj)
            })
        }

        if (dataRisk.DP) {
            dataRisk.DP.map(function (val) {
                const obj = new Object()
                obj['value'] = val
                obj['label'] = (parseFloat(val) * 100).toString() + '%'
                objDPData.push(obj)
            })
        }

        if (dataRisk.Tenor && dataRisk.DP) {
            setDropdownRisk({ objTenor: objTenorData, objDP: objDPData })
        }
    }

    const onChangeJenis = selectedVal => {
        reset({ formMerk: '', formHarga: '' })
        setSelectedJenis(selectedVal.value);
    }


    const onChangePrice = val => {
        setPrice(val.target.value);
    }

    const getTenorDP = () => {
        if (selectedJenis != '' && price >= 1000000) {
            dispatch(getMappingRisk({
                RiskType: substringThirdCircum(selectedJenis),
                Hargabrg: price,
            }));

        } else {
            swal({ title: t('alerts:info'), text: 'Pastikan Harga Barang Valid', icon: 'warning', timer: 3000 });
        }
    }

    const calculateSimulation = (data) => {
        const objSimulation = {
            Hargabrg: price,
            DP: data.formUangMuka.value,
            Tenor: data.formJangkaWaktu.value,
            Insurance: dataRisk.Asuransi,
            Adminfee: dataRisk.Admin,
            Tipebiayaadmin: ADMIN_TYPE,
            Bunga: dataRisk.IntRate,
            Tipeangsuran: INSTALLMENT_TYPE,
        }

        dispatch(getMpInstallmentCalc(objSimulation));
        setAppData(prevState => {
            return { ...prevState, merek: data.formMerk.label, nominal: price, dp: data.formUangMuka.value, tenor: data.formJangkaWaktu.value };
        })
    }

    const goToApplicationForm = () => {
        if (appData.installmentAmount) {
            let objSimulasi = {
                jenisBarang: LOB,
                merekBarang: appData.merek,
                nominal: appData.nominal,
                dp: appData.dp,
                tenor: appData.tenor
            }
            dispatch(simulationData(objSimulasi));
            router.push({ pathname: '/application-form' });
        } else {
            swal({ title: t('alerts:info'), text: t('alerts:infoText'), icon: 'warning', timer: 3000 });
        }
    }

    const { register, handleSubmit, setValue, errors, reset } = useForm();
    return (
        <Container className="section-container">
            <Row>
                <Col>
                    <Card className="simul-card-mp mx-auto column-margin">
                        <Card.Body className="txt-black">
                            <Form onSubmit={handleSubmit(calculateSimulation)}>
                                <Form.Group>
                                    <Form.Label>{t('itemType')}</Form.Label>
                                    {
                                        dataJenisError ? <Select options={[]} placeholder={t('select')} /> :
                                            <RHFInput
                                                as={<Select options={dropdownJenis} placeholder={t('select')} instanceId={'tipeMP'} />}
                                                name="formTipe"
                                                setValue={setValue}
                                                register={register}
                                                rules={{ required: true }}
                                                onChange={(newValue) => onChangeJenis(newValue)}
                                            />
                                    }
                                    {errors.formTipe && <p className="txt-error">{t('mpError1')}</p>}

                                </Form.Group>
                                <Form.Group controlId="formMerek">
                                    <Form.Label>{t('itemBrand')}</Form.Label>
                                    {
                                        dataMerkError ? <Select options={[]} placeholder={t('select')} /> :
                                            <RHFInput
                                                as={<Select options={dropdownMerk} placeholder={t('select')} instanceId={'merkMP'} />}
                                                name="formMerk"
                                                setValue={setValue}
                                                register={register}
                                                rules={{ required: true }}
                                            />
                                    }
                                    {errors.formMerk && <p className="txt-error">{t('mpError2')}</p>}

                                </Form.Group>
                                <Form.Group controlId="formHarga">
                                    <Form.Label>{t('itemPrice')}</Form.Label>
                                    <Form.Control ref={register({ required: true })} name="formHarga" type="number" placeholder={t('itemPrice')} onChange={onChangePrice} min={1} step="any" />
                                    {errors.formHarga && <p className="txt-error">{t('mpError3')}</p>}
                                </Form.Group>

                                <div className="text-center mb-3">
                                    <Button variant="primary" className="bg-blue-baf w-75 font-weight-normal radius-2 p-2" onClick={getTenorDP} disabled={dataRiskLoading ? true : false}>
                                        {t('checkDP')}
                                        {dataRiskLoading && <span className="spinner-border spinner-border-sm ml-2" role="status" aria-hidden="true"></span>}
                                    </Button>
                                </div>
                                <Row>
                                    <Col lg={6} md={6} xs={12}>
                                        <Form.Group>
                                            <Form.Label>{t('timePeriod')}</Form.Label>
                                            {
                                                dataRiskError ? <Select options={[]} placeholder={t('select')} /> :
                                                    <RHFInput
                                                        as={<Select options={dropdownRisk.objTenor} placeholder={t('select')} instanceId={'tenorMP'} />}
                                                        name="formJangkaWaktu"
                                                        setValue={setValue}
                                                        register={register}
                                                        rules={{ required: true }}
                                                    />
                                            }
                                            {errors.formJangkaWaktu && <p className="txt-error">{t('tenorError')}</p>}

                                        </Form.Group>
                                    </Col>
                                    <Col lg={6} md={6} xs={12}>
                                        <Form.Group controlId="formUangMuka">
                                            <Form.Label>{t('downPayment')}</Form.Label>
                                            {
                                                dataRiskError ? <Select options={[]} placeholder={t('select')} /> :
                                                    <RHFInput
                                                        as={<Select options={dropdownRisk.objDP} placeholder={t('select')} instanceId={'dpMP'} />}
                                                        name="formUangMuka"
                                                        setValue={setValue}
                                                        register={register}
                                                        rules={{ required: true }}
                                                    />
                                            }
                                            {errors.formUangMuka && <p className="txt-error">{t('dpError')}</p>}
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <div className="text-center">
                                    <Button variant="primary" className="bg-blue-baf w-75 font-weight-normal radius-2 p-2" type="Submit" disabled={dataSimulationLoading ? true : false}>
                                        {t('calculate')}
                                        {dataSimulationLoading && <span className="spinner-border spinner-border-sm ml-2" role="status" aria-hidden="true"></span>}
                                    </Button>
                                </div>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
                <Col>
                    <Card className="simul-card-mp mx-auto bg-seablue column-margin">
                        <Card.Body>
                            <Form>
                                <div className="text-center mt-5">
                                    <h5 className="txt-white mb-4">{t('estimation')}</h5>
                                    <div className="bg-white txt-blue-baf p-4 mb-4 w-75 radius-1 mx-auto">
                                        <h3 className="font-weight-bold">Rp {appData.installmentAmount ? formatCurrency(appData.installmentAmount.toString()) : ' - '}</h3>
                                        <h4 className="font-weight-bold">/{t('month')}</h4>
                                    </div>
                                    <div className="w-75 mx-auto txt-white mb-5">
                                        <h6>{t('simulationText')}</h6>
                                    </div>
                                    <Button variant="primary" className="bg-white txt-blue-baf w-75 font-weight-bold radius-2 p-2" onClick={goToApplicationForm}>{t('submitNow')}</Button>
                                </div>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container >
    )
}