import React, { useEffect, useState, createContext } from 'react'
import Select from 'react-select'
import { Card, Container, Form, Button, Col, Row } from "react-bootstrap";
import { useDispatch, useSelector } from 'react-redux';
import SyanaData from '../syana.json';
import { useForm } from 'react-hook-form';
import { RHFInput } from 'react-hook-form-input';
import swal from 'sweetalert';
import { substringCircum, substringSecondCircum, formatCurrency, fdeFormatCurrency } from '../../../utils/utilization';
import { useRouter } from 'next/router';
import { simulationData } from '../../../store/actions/application';
import { useTranslation } from 'react-i18next';


export function SimulationFormSyana() {
    const { t } = useTranslation(["simulation","alerts"]);
    const dispatch = useDispatch();
    const router = useRouter();

    const [plafonPinjaman, setPlafonPinjaman] = useState(1000000);
    const [appData, setAppData] = useState({ merek: '', nominal: '', dp: '', tenor: '', installmentAmount: false });
    const [LOB, setLOB] = useState("SYANA")

    const onChangePlafonPinjaman = val => {
        setPlafonPinjaman(val.target.value);
    }

    const onSubmit = (data) => {
        let merekBarang = data.formMerk.value;
        let nominal = parseFloat(data.formPlafon);
        let tenor = substringCircum(data.formJangkaWaktu.value);
        let rate = substringSecondCircum(data.formJangkaWaktu.value);
        let calcResult =
            (nominal * (1 + parseFloat(rate))) / parseFloat(tenor);

        swal({ title: t('alerts:success'), text: t('alerts:successText'), icon: 'success', timer: 3000 });
        setAppData({ merek: merekBarang, nominal: nominal, dp: '', tenor: tenor, installmentAmount: Math.round(calcResult) });
    }

    const goToApplicationForm = () => {
        if (appData.installmentAmount) {
            let objSimulasi = {
                jenisBarang: LOB,
                merekBarang: appData.merek,
                nominal: appData.nominal,
                dp: appData.dp,
                tenor: appData.tenor
            }
            dispatch(simulationData(objSimulasi));
            router.push({ pathname: '/application-form' });
        } else {
            swal({ title: t('alerts:info'), text: t('alerts:infoText'), icon: 'warning', timer: 3000 });
        }
    }

    const { register, handleSubmit, setValue, errors } = useForm();

    return (
        <Container className="section-container">
            <Row>
                <Col>
                    <Card className="simul-card-mp mx-auto column-margin">
                        <Card.Body className="txt-black">
                            <Form onSubmit={handleSubmit(onSubmit)}>
                                <Form.Group>
                                    <Form.Label>{t('chooseMotor')}</Form.Label>
                                    <RHFInput
                                        as={<Select options={SyanaData.Merk} placeholder={t('select')} instanceId={'merkSyana'} />}
                                        name="formMerk"
                                        setValue={setValue}
                                        register={register}
                                        rules={{ required: true }}
                                    />
                                    {errors.formMerk && <p className="txt-error">{t('syanaError1')}</p>}
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>{t('motorYear')}</Form.Label>
                                    <RHFInput
                                        as={<Select options={SyanaData.Year} placeholder={t('select')} instanceId={'yearSyana'} />}
                                        name="formTahun"
                                        setValue={setValue}
                                        register={register}
                                        rules={{ required: true }}
                                    />
                                    {errors.formTahun && <p className="txt-error">{t('syanaError2')}</p>}
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>{t('loanAmount')}</Form.Label>
                                    <Form.Control name={"formPlafon"} ref={register({ required: true })} type="number" placeholder="" value={plafonPinjaman} onChange={onChangePlafonPinjaman} />
                                    {errors.formPlafon && <p className="txt-error">Jumlah pinjaman wajib diisi</p>}

                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>{t('timePeriod')}</Form.Label>
                                    <RHFInput
                                        as={<Select options={SyanaData.Tenor} placeholder={t('select')} instanceId={'tenorSyana'} />}
                                        name="formJangkaWaktu"
                                        setValue={setValue}
                                        register={register}
                                        rules={{ required: true }}
                                    />
                                    {errors.formJangkaWaktu && <p className="txt-error">{t('tenorError')}</p>}
                                </Form.Group>
                                <div className="text-center mt-5">
                                    <Button variant="primary" type="submit" className="bg-blue-baf w-75 font-weight-normal radius-2 p-2">
                                        {t('calculate')}
                                    </Button>
                                </div>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
                <Col>
                    <Card className="simul-card-mp mx-auto bg-seablue column-margin">
                        <Card.Body>
                            <Form>
                                <div className="text-center mt-5">
                                    <h5 className="txt-white mb-4">{t('estimation')}</h5>
                                    <div className="bg-white txt-blue-baf p-4 mb-4 w-75 radius-1 mx-auto">
                                        <h3 className="font-weight-bold">Rp {appData.installmentAmount ? formatCurrency(appData.installmentAmount.toString()) : ' - '}</h3>
                                        <h4 className="font-weight-bold">/{t('month')}</h4>
                                    </div>
                                    <div className="w-75 mx-auto txt-white mb-5">
                                        <h6>{t('simulationText')}</h6>
                                    </div>
                                    <Button variant="primary" className="bg-white txt-blue-baf w-75 font-weight-bold radius-2 p-2" onClick={goToApplicationForm}>{t('submitNow')}</Button>
                                </div>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container >
    )
}
