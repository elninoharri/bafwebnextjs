import { Card, Button } from "react-bootstrap";

export function PromoInfoCard(props) {
    return (
        <Card className="promo-info block-card">
            <Card.Header className="text-center bg-seablue txt-white promo-info-header">
                Info Promo
            </Card.Header>
            <Card.Body>
                <Card.Text className="txt-blue-baf">Waktu Promo</Card.Text>
                <Card.Text>{`${props.datefrom} - ${props.dateto}`}</Card.Text>
                <Button className="btn btn-block bg-blue-baf font-weight-normal" onClick={props.handlePricelist}>Pricelist</Button>
                <Button className="btn btn-block bg-blue-baf font-weight-normal" onClick={props.handlePromo} disabled={props.promoLoading ? true : false}>
                    {'Ambil Promonya Sekarang'}
                    {props.promoLoading && <span className="spinner-border spinner-border-sm ml-2" role="status" aria-hidden="true"></span>}
                </Button>
            </Card.Body>
        </Card>
    )
}