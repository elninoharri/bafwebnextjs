import { Tab, Row, Col, Nav, Pagination, Container } from 'react-bootstrap';
import { useEffect, useState } from "react";
import { useTranslation } from "next-i18next";
import { useDispatch, useSelector } from "react-redux";
import { getBanner, newsData } from "../../store";
import { capitalizeFirstLetter, titleToUrl } from "../../utils/utilization";
import { CustomCard } from "../Card";
import { useRouter } from 'next/router';

export function PromoTab(props) {
    const { t } = useTranslation('common');
    const dispatch = useDispatch();
    const router = useRouter();
    const allData = useSelector((state) => state.getBanner.result.result ? state.getBanner.result.result.AllData : []);

    const [pagingData, setPagingData] = useState({
        activePageALL: 1,
        dataPagingALL: [],

        activePageNMC: 1,
        dataPagingNMC: [],

        activePageUMC: 1,
        dataPagingUMC: [],

        activePageCAR: 1,
        dataPagingCAR: [],

        activePageMP: 1,
        dataPagingMP: [],

        activePageSYANA: 1,
        dataPagingSYANA: [],

        activePageOTHER: 1,
        dataPagingOTHER: []
    })

    useEffect(() => {
        if (allData.length < 1) {
            dispatch(getBanner({
                Code: "",
                Type: "",
                Category: ""
            }))
        }
    }, [])

    useEffect(() => {
        if (allData.length > 0) {
            initData();
        }
    }, [allData])

    const initData = () => {
        console.log(props)
        let promoDataAll = allData.filter(function (item) {
            return item.BannerType === `BANNER_TYPE_${props.bannerType}`;
        })

        let promoDataNMC = allData.filter(function (item) {
            return item.BannerType === `BANNER_TYPE_${props.bannerType}` && item.BannerCat === 'BNMC';
        })

        let promoDataUMC = allData.filter(function (item) {
            return item.BannerType === `BANNER_TYPE_${props.bannerType}` && item.BannerCat === 'BUMC';
        })

        let promoDataCar = allData.filter(function (item) {
            return item.BannerType === `BANNER_TYPE_${props.bannerType}` && item.BannerCat === 'BCAR';
        })

        let promoDataMP = allData.filter(function (item) {
            return item.BannerType === `BANNER_TYPE_${props.bannerType}` && item.BannerCat === 'BMP';
        })

        let promoDataSyana = allData.filter(function (item) {
            return item.BannerType === `BANNER_TYPE_${props.bannerType}` && item.BannerCat === 'BSYANA';
        })

        let promoDataOther = allData.filter(function (item) {
            return item.BannerType === `BANNER_TYPE_${props.bannerType}` && item.BannerCat === 'OTHERS';
        })

        let dataPaginateAll = promoDataAll.length > 0 ? paginate(promoDataAll, 4, 1) : [];
        let dataPaginateNMC = promoDataNMC.length > 0 ? paginate(promoDataNMC, 4, 1) : [];
        let dataPaginateUMC = promoDataUMC.length > 0 ? paginate(promoDataUMC, 4, 1) : [];
        let dataPaginateCar = promoDataCar.length > 0 ? paginate(promoDataCar, 4, 1) : [];
        let dataPaginateMP = promoDataMP.length > 0 ? paginate(promoDataMP, 4, 1) : [];
        let dataPaginateSyana = promoDataSyana.length > 0 ? paginate(promoDataSyana, 4, 1) : [];
        let dataPaginateOther = promoDataOther.length > 0 ? paginate(promoDataOther, 4, 1) : [];

        setPagingData(prevState => {
            return {
                ...prevState,
                dataPagingALL: dataPaginateAll,
                dataPagingNMC: dataPaginateNMC,
                dataPagingUMC: dataPaginateUMC,
                dataPagingCAR: dataPaginateCar,
                dataPagingMP: dataPaginateMP,
                dataPagingSYANA: dataPaginateSyana,
                dataPagingOTHER: dataPaginateOther
            };
        })
    }

    const paginate = (array, page_size, page_number) => {
        return array.slice((page_number - 1) * page_size, page_number * page_size);
    }

    const handlePageChange = (page, flag) => {
        let dataPaginate = paginate(pagingData[`dataPaging${flag}`], 9, page);
        setPagingData(prevState => {
            return {
                ...prevState,
                [`activePage${flag}`]: page,
                [`dataPaging${flag}`]: dataPaginate
            };
        })
    }

    const handleDetail = (items, id) => {
        const category = (items.BannerCat).toLowerCase();
        const date = items.date_num || '';
        const dateto = items.date_to_num || '';
        const img = items.ContentPageList ? items.ContentPageList[0].ContentUrl : '';
        const title = items.ContentPageList ? items.ContentPageList[0].ContentName : '';
        const content = items.ContentPageList ? items.ContentPageList[0].ContentDescr : '';
        const bannerId = items.BannerID;
        const bannerCode = items.BannerCode;
        const slug = titleToUrl(title);
        dispatch(newsData(
            {
                bannerCode: bannerCode,
                category: category,
                img: img,
                title: title,
                date: date,
                dateto: dateto,
                content: content,
                hasPIC: true
            }
        ))

        router.push(`/${props.detailPage}/[id]/[title]`, `/${props.detailPage}/${bannerId}/${slug}`);
    }

    const renderData = (flag) => {
        let items = [];
        let active = pagingData[`activePage${flag}`];
        let data = pagingData[`dataPaging${flag}`];

        let length = data.length > 0 ? Math.ceil(data.length / 9) : 0;

        for (let number = 1; number <= length; number++) {
            items.push(
                <Pagination.Item key={number} active={number === active} onClick={() => { handlePageChange(number, flag) }}>
                    {number}
                </Pagination.Item>,
            );
        }

        return (
            <>
                {
                    data.length > 0 ?
                        <>
                            <Row>
                                {data.map((item, index) => {
                                    return (
                                        <Col key={index} lg={4} sm={12} md={4} className="justify-content-center d-flex align-items-stretch mb-5">
                                            <CustomCard
                                                imgSource={item.BannerImg}
                                                tag={item.BannerType}
                                                tagDetail={capitalizeFirstLetter(item.BannerCat) || 'Other'}
                                                cardTitle={item.ContentPageList ? item.ContentPageList[0].ContentName : ''}
                                                cardText={item.ContentPageList ? item.ContentPageList[0].ContentDescr : ''}
                                                cardDate={item.date}
                                                cardDateType={'news'}
                                                handleClick={() => { handleDetail(item, index) }}
                                                isFull={true}
                                            />
                                        </Col>
                                    )
                                })}
                            </Row>
                            <Pagination className="float-right pagination-news">
                                {items}
                            </Pagination>
                        </>
                        :
                        <Row>
                            <Col>
                                <div className="d-flex justify-content-center mt-5 mb-5">{t('noResults')}</div>
                            </Col>
                        </Row>
                }
            </>
        )
    }


    const tabData = [{
        id: 'ALL',
        tabHeader: 'Semua',
        tabContent: renderData('ALL')
    },
    {
        id: 'NMC',
        tabHeader: 'Motor Baru',
        tabContent: renderData('NMC')
    },
    {
        id: 'UMC',
        tabHeader: 'Motor Bekas',
        tabContent: renderData('UMC')
    },
    {
        id: 'CAR',
        tabHeader: 'Mobil',
        tabContent: renderData('CAR')
    }, {
        id: 'MP',
        tabHeader: 'Elektronik & Furnitur',
        tabContent: renderData('MP')
    }, {
        id: 'SYANA',
        tabHeader: 'Dana Syariah',
        tabContent: renderData('SYANA')
    },
    {
        id: 'OTHER',
        tabHeader: 'Lainnya',
        tabContent: renderData('OTHER')
    }]

    const defaultActive = tabData[0].id ? tabData[0].id : '';

    return (
        <Tab.Container id="product-tab" defaultActiveKey={defaultActive}>
            <Row>
                <Col>
                    <Nav variant="pills" className="flex-row nav-justified product-tab">
                        {tabData.map((item, index) => {
                            return (
                                <Nav.Item key={index}>
                                    <Nav.Link
                                        eventKey={item.id}
                                        style={{ color: "#002F5F", paddingTop: "10px", paddingBottom: "10px" }}
                                        className="nav-productTabs">{item.tabHeader}
                                    </Nav.Link>
                                </Nav.Item>
                            )
                        })}
                    </Nav>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Tab.Content>
                        {tabData.map((item, index) => {
                            return (
                                <Tab.Pane eventKey={item.id} key={index}>
                                    <div className="articles-container">{item.tabContent}</div>
                                </Tab.Pane>
                            )
                        })}
                    </Tab.Content>
                </Col>
            </Row>
        </Tab.Container>
    )
}