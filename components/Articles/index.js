import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import { Card, Button, Row, Col, Tab, Nav, Pagination } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { getBanner, newsData } from "../../store/actions";
import { CustomCard } from '../Card';
import { SearchBar } from '../SearchBar';
import { useTranslation } from 'next-i18next';
import { titleToUrl } from "../../utils/utilization";

export function Articles() {
    const { t } = useTranslation('common');

    const router = useRouter();
    const dispatch = useDispatch();

    const isInitialMount = useRef(true);
    const [searchKey, setSearchKey] = useState('');
    const [filteredData, setFilteredData] = useState([]);
    const allData = useSelector((state) => state.getBanner.result.result ? state.getBanner.result.result.AllData : []);


    const [pagingData, setPagingData] = useState({
        activePage: 1,
        dataPaging: [],
        dataInitial: []
    });

    useEffect(() => {
        if (allData.length < 1) {
            dispatch(getBanner({
                Code: "",
                Type: "",
                Category: ""
            }))
        }
    }, [])

    useEffect(() => {
        if (allData.length > 0) {
            initData();
        }
    }, [allData])

    const initData = () => {
        let dataArticle = allData.filter(function (item) {
            return item.BannerType === 'BANNER_TYPE_ARTICLE';
        })

        let dataPaginateArticle = dataArticle.length > 0 ? paginate(dataArticle, 9, 1) : [];
        console.log('init', dataPaginateArticle)

        setPagingData(prevState => {
            return {
                ...prevState,
                dataPaging: dataPaginateArticle,
                dataInitial: dataPaginateArticle
            };
        })
    }



    const updateSearch = () => {
        const { dataInitial } = pagingData;

        if (searchKey !== ' ') {
            const filtered = dataInitial.filter(item => { return (item.ContentPageList[0].ContentName.toLowerCase().includes(searchKey.toLowerCase())) })
            setFilteredData(filtered);
        }
        else {
            setFilteredData(dataInitial);
        }
    }

    useEffect(() => {
        if (isInitialMount.current) {
            isInitialMount.current = false;
        } else {
            setPagingData(prevState => {
                return {
                    ...prevState,
                    dataPaging: filteredData,
                };
            })
        }
    }, [filteredData])



    const handleDetail = (items, id) => {
        const category = (items.BannerCat).toLowerCase();
        const date = items.date_num || '';
        const dateto = items.date_to_num || '';
        const img = items.ContentPageList ? items.ContentPageList[0].ContentUrl : '';
        const title = items.ContentPageList ? items.ContentPageList[0].ContentName : '';
        const content = items.ContentPageList ? items.ContentPageList[0].ContentDescr : '';
        const bannerId = items.BannerID;
        const bannerCode = items.BannerCode;
        const slug = titleToUrl(title);
        dispatch(newsData(
            {
                bannerCode: bannerCode,
                category: category,
                img: img,
                title: title,
                date: date,
                dateto: dateto,
                content: content,
                hasPIC: true
            }
        ))

        router.push('/article/[id]/[title]', `/article/${bannerId}/${slug}`);
    }

    const handleSearchChange = (e) => {
        setSearchKey(e.target.value)
    }

    const handlePageChange = (page) => {
        const { dataPaging } = pagingData;
        let dataPaginate = paginate(dataPaging, 9, page);

        setPagingData(prevState => {
            return {
                ...prevState,
                activePage: page,
                dataPaging: dataPaginate,
            };
        })
    }

    const paginate = (array, page_size, page_number) => {
        return array.slice((page_number - 1) * page_size, page_number * page_size);
    }

    const renderData = () => {
        const { dataPaging, activePage } = pagingData;

        let items = [];
        let length = dataPaging.length > 1 ? Math.ceil(dataPaging.length / 9) : 1;

        for (let number = 1; number <= length; number++) {
            items.push(
                <Pagination.Item key={number} active={number === activePage} onClick={() => { handlePageChange(number) }}>
                    {number}
                </Pagination.Item>,
            );
        }

        return (
            <div className="articles-container">
                <Row>
                    {dataPaging.length > 0 ? dataPaging.map((item, index) => {
                        return (
                            <Col key={index} lg={4} sm={12} md={6} className="justify-content-center d-flex align-items-stretch mb-5">
                                <CustomCard
                                    imgSource={item.BannerImg}
                                    cardTitle={item.ContentPageList ? item.ContentPageList[0].ContentName : ''}
                                    cardText={item.ContentPageList ? item.ContentPageList[0].ContentDescr : ''}
                                    cardDate={item.date}
                                    cardDateType={'news'}
                                    handleClick={() => { handleDetail(item, index) }}
                                    isFull={true}
                                />
                            </Col>
                        )
                    })
                        : <h1 className="txt-blue-baf mx-auto">{t('noResults')}</h1>}
                </Row>
                <Pagination className="float-right pagination-news">
                    {items}
                </Pagination>
            </div>
        )
    }

    const tabData = {
        tabContent: renderData()
    }

    return (
        <>
            <SearchBar handleChange={handleSearchChange} keyword={searchKey} setKeyword={(val) => { updateSearch(val) }} />
            <Row>
                <Col>
                    <div>{tabData.tabContent}</div>
                </Col>
            </Row>
        </>
    )
}