import React, { createRef, useEffect, useState } from 'react';
import Select from 'react-select';
import { Container, Form, Button, Col, Row } from "react-bootstrap";
import { Controller, useForm } from 'react-hook-form';
import { RHFInput } from 'react-hook-form-input';
import { useDispatch, useSelector } from 'react-redux';
import { REGEX_EMAIL, REGEX_HP } from '../../utils/constant';
import { useRouter } from 'next/router';
import { getPengajuan } from '../../store/actions/application';
import moment from 'moment';
import ReCAPTCHA from "react-google-recaptcha";
import { ModalTC } from '../Modal';
import { useTranslation } from 'next-i18next';

export function ApplicationForm() {
    const { t } = useTranslation(["alerts", "common"]);
    const dispatch = useDispatch();
    const router = useRouter();

    const lobData = [{
        value: 'NMC',
        label: 'Motor Yamaha'
    }, {
        value: 'CAR',
        label: 'Mobil'
    }, {
        value: 'MP',
        label: 'Elektronik & Furniture'
    }, {
        value: 'SYANA',
        label: 'Dana Syariah'
    },
    {
        value: 'AGRI',
        label: 'Mesin Pertanian'
    },
    {
        value: 'FLEET',
        label: 'Pembiyaan Fleet'
    }];

    const simulationData = useSelector(state => state.simulationData.data ? state.simulationData.data : '');
    const pengajuanLoading = useSelector(state => state.getPengajuan.loading ? state.getPengajuan.loading : false);
    const pengajuanResult = useSelector(state => state.getPengajuan.result ? state.getPengajuan.result : {});
    const pengajuanError = useSelector(state => state.getPengajuan.error ? state.getPengajuan.error : '');
    const detailUser = useSelector(state => state.detailUser.result ? state.detailUser.result : {});

    const recaptchaRef = createRef();
    const [recaptchaSts, setRecaptchaSts] = useState(false);

    const [show, setShow] = useState(false);

    const openModal = () => {
        setShow(true);
    }
    const closeModal = () => {
        setShow(false);
    }

    const initUserData = () => {
        setValue("formNama", detailUser.Username || '')
        setValue("formEmail", detailUser.Email || '')
        setValue("formNo1", detailUser.Phoneno || '')
        setValue("formAddress", detailUser.UsrAdr || '')
    }

    useEffect(() => {
        if (simulationData == '') {
            swal({ title: t('alerts:info'), text: t('alerts:infoText'), icon: 'warning', timer: 3000 });
            router.replace('/product')
        } else {
            let idx = lobData.findIndex(obj => obj.value === simulationData.jenisBarang);
            if (idx > -1) {
                setValue("formApplicationType", lobData[idx]);
            }

            if (simulationData.hasOwnProperty('voucherCode')) {
                let voucherCode = simulationData.voucherCode;
                setValue("formPromoCode", voucherCode);
            }

            if (detailUser) { initUserData() }
        }
    }, [])

    useEffect(() => {
        if (pengajuanError) {
            swal({ title: t('alerts:warning'), text: 'Error While Submiting Data, Please Try Again', icon: 'error', timer: 3000 });
        } else if (pengajuanResult && pengajuanResult.hasOwnProperty('status')) {
            if (simulationData !== '') {
                if (pengajuanResult.status === 'success') {
                    swal({ title: t('alerts:success'), text: 'Successfully Submit Data', icon: 'success', timer: 3000 });
                    router.replace('/');
                } else {
                    swal({ title: t('alerts:warning'), text: 'Error While Submiting Data, Please Try Again', icon: 'error', timer: 3000 });
                }
            }
        }
    }, [pengajuanResult, pengajuanError])

    const validate = () => {
        if (!recaptchaSts) {
            swal({ title: t('alerts:info'), text: t('common:captcha'), icon: 'warning', timer: 3000 });
            return false;
        }
        return true;
    }

    const onSubmit = (data) => {
        if (validate()) {
            let nama = data.formNama;
            let email = data.formEmail;
            let hp = data.formNo1;
            let alamat = data.formAddress;
            let kodepos = data.formZipCode;
            let kodepromo = data.formPromoCode;
            let jenisbarang = simulationData.jenisBarang || '';
            let merekbarang = simulationData.merekBarang || '';
            let nominal = "Rp." + simulationData.nominal || '';
            let dp = simulationData.dp != '' ? ((parseFloat(simulationData.dp) * 100).toString() + '%') : '';
            let tenor = simulationData.tenor || '';

            const objPengajuan = {
                "UserID": "",
                "Fullname": nama,
                "MobilePhn": hp,
                "Email": email,
                "Alamat": alamat,
                "Source": "BAF Web",
                "TipePengajuan": jenisbarang,
                "JenisBrg": merekbarang,
                "Pinjaman": nominal,
                "DP": dp,
                "Tenor": tenor,
                "PromoCode": kodepromo,
                "DateReq": moment().format('YYYY-MM-DD hh:mm:ss'),
                "DOB": "",
                "KodePos": kodepos
            }

            dispatch(getPengajuan(objPengajuan));
        }
    }

    const handleCaptchaChange = async (captchaCode) => {
        if (!captchaCode) {
            return;
        }
        try {
            const response = await fetch("/api/recaptcha", {
                method: "POST",
                body: JSON.stringify({ captcha: captchaCode }),
                headers: {
                    "Content-Type": "application/json",
                },
            });
            if (response.ok) {
                setRecaptchaSts(true);
            } else {
                const error = await response.json();
                swal({ title: t('alerts:warning'), text: error?.message || 'Something went wrong', icon: 'error', timer: 3000 });
                setRecaptchaSts(false);
            }
        } catch (error) {
            swal({ title: t('alerts:warning'), text: error?.message || 'Something went wrong', icon: 'error', timer: 3000 });
            setRecaptchaSts(false);
        } finally {
            // recaptchaRef.current.reset();
        }
    }

    const handleCaptchaError = () => {
        setRecaptchaSts(false);
    }

    const { register, handleSubmit, setValue, errors, control } = useForm();
    return (
        <Container className="section-container">
            <Form onSubmit={handleSubmit(onSubmit)}>
                <Row>
                    <Col>
                        <Form.Group controlId="formNama">
                            <Form.Label>{t('common:fullName')}</Form.Label>
                            <Form.Control ref={register({ required: true })} name="formNama" type="text" placeholder={t('common:fullName')} style={{ borderColor: errors.formNama ? '#fa1e0e' : '#8692A6' }} />
                            {errors.formNama && <p className="txt-error">{errors.formNama.message || t('common:missingName')}</p>}
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Group controlId="formEmail">
                            <Form.Label>{t('common:email')}</Form.Label>
                            <Form.Control ref={register({
                                required: true,
                                pattern: {
                                    value: REGEX_EMAIL,
                                    message: t('common:invalidEmail')
                                }
                            })} name="formEmail" type="text" placeholder={t('common:email')} style={{ borderColor: errors.formEmail ? '#fa1e0e' : '#8692A6' }} />
                            {errors.formEmail && <p className="txt-error">{errors.formEmail.message || t('common:pleaseEnterEmail')}</p>}
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Group controlId="formNo1">
                            <Form.Label>{t('common:phoneNumber')}</Form.Label>
                            <Form.Control ref={register({
                                required: true,
                                pattern: {
                                    value: REGEX_HP,
                                    message: t('common:pleaseEnterHP')
                                }
                            })} name="formNo1" type="text" placeholder={t('common:phoneNumber')} style={{ borderColor: errors.formNo1 ? '#fa1e0e' : '#8692A6' }} />
                            {errors.formNo1 && <p className="txt-error">{errors.formNo1.message || t('common:pleaseEnterHP')}</p>}
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Group controlId="formAddress">
                            <Form.Label>{t('common:address')}</Form.Label>
                            <Form.Control as={'textarea'} rows={5} ref={register({ required: true })} name="formAddress" type="text" placeholder="Alamat" style={{ borderColor: errors.formAddress ? '#fa1e0e' : '#8692A6' }} />
                            {errors.formAddress && <p className="txt-error">{t('common:pleasEnterAddress')}</p>}
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Group controlId="formZipCode">
                            <Form.Label>{t('common:zipcode')}</Form.Label>
                            <Form.Control ref={register({ required: true })} name="formZipCode" type="text" placeholder={t('common:zipcode')} style={{ borderColor: errors.formZipCode ? '#fa1e0e' : '#8692A6' }} />
                            {errors.formZipCode && <p className="txt-error">{t('common:pleasEnterZipcode')}</p>}
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Group controlId="formApplicationType">
                            <Form.Label>{t('common:submission')}</Form.Label>
                            <RHFInput
                                as={<Select options={lobData} placeholder={t('common:select')} instanceId={'appType'} styles={{ control: styles => ({ ...styles, borderColor: errors.formApplicationType ? '#fa1e0e' : '#8692A6' }) }} isDisabled={true} />}
                                name="formApplicationType"
                                setValue={setValue}
                                register={register}
                                rules={{ required: true }}
                            />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Group controlId="formPromoCode">
                            <Form.Label>{t('common:promoCode')}</Form.Label>
                            <Form.Control ref={register} name="formPromoCode" type="text" placeholder={t('common:promoCode')} />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col className="d-flex justify-content-center">
                        <ReCAPTCHA
                            ref={recaptchaRef}
                            sitekey={process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY}
                            onChange={handleCaptchaChange}
                            onErrored={handleCaptchaError}
                            onExpired={handleCaptchaError}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Group controlId="formCheckboxInfo">
                            <Form.Check ref={register({ required: true })} style={{ borderColor: errors.formCheckboxInfo ? '#fa1e0e' : '#8692A6' }} type="checkbox" name="formCheckboxInfo" label={t('common:agreement1')} required />
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Form.Group controlId="formCheckboxTerms">
                            <Form.Check ref={register({ required: true })} style={{ borderColor: errors.formCheckboxTerms ? '#fa1e0e' : '#8692A6' }} type="checkbox" name="formCheckboxTerms" label={t('common:agreement2')} required>
                                <Form.Check.Input type="checkbox" isValid />
                                <label>{t('common:agreement2')}<a href="#responsive" onClick={openModal}>{t('common:tC')}</a></label>
                                <ModalTC showMod={show} closeMod={closeModal} />
                            </Form.Check>
                        </Form.Group>
                    </Col>
                </Row>
                <Row>
                    <Col lg={12} className="d-flex justify-content-center">
                        <Button type="submit" className="bg-blue-baf pl-4 pr-4" disabled={pengajuanLoading ? true : false}>
                            {t('common:submitNow')}
                            {pengajuanLoading && <span className="spinner-border spinner-border-sm ml-2" role="status" aria-hidden="true"></span>}
                        </Button>
                    </Col>
                </Row>
            </Form>
        </Container>
    )
}