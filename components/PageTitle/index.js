import { Container, Jumbotron } from "react-bootstrap";

export function PageTitle({ title }) {
    return (
        <Jumbotron fluid className="bg-blue-baf mb-0">
            <Container className="text-center">
                <h1 className="txt-jumbotron">{title}</h1>
            </Container>
        </Jumbotron>
    )
}