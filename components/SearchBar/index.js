import { urlObjectKeys } from 'next/dist/next-server/lib/utils';
import React, { useState } from 'react';
import { Button } from "react-bootstrap";
import { useTranslation } from 'next-i18next';

export function SearchBar({keyword, setKeyword, handleChange}) {
    const { t } = useTranslation('common');

    return (
        <div className="search-bar">
            <img src={'/static/images/icons/search_icon.svg'} style = {{position: 'absolute', paddingLeft: "0.75rem"}}/>
            <input 
                key="random1"
                placeholder= {t('searchArticle')}
                className="mr-3 search-input"
                onChange={handleChange}
            />
            <Button className="bg-blue-baf" onClick={setKeyword}>{t('search')}</Button>
        </div>
        
      );
}