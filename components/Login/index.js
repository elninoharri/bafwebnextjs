import Image from "next/image";
import Link from "next/link";
import { Button, Container, Form } from "react-bootstrap";
import { useForm } from 'react-hook-form';
import { REGEX_HP } from "../../utils/constant";
import { useDispatch, useSelector } from 'react-redux';
import { detailUser, login, setToken } from "../../store/actions";
import { useEffect, useRef } from "react";
import swal from "sweetalert";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";

export function Login() {
    const { t } = useTranslation(['login', 'alerts', 'common']);
    
    const dispatch = useDispatch();
    const router = useRouter();

    const state = useSelector(state => state);
    const loginResult = useSelector(state => state.login.result);
    const loginError = useSelector(state => state.login.error);
    const loginLoading = useSelector(state => state.login.loading);

    const isInitialMount = useRef(true);

    useEffect(() => {
        if (isInitialMount.current) {
            isInitialMount.current = false;
        } else {
            if (loginError) {
                swal({ title: t('alerts:warning'), text: `${loginError}` || t('alerts:warningText1'), icon: 'error', timer: 3000 });
            } else if (loginResult) {
                if (loginResult.Usertoken) {
                    dispatch(setToken({ userToken: loginResult.Usertoken, userGroupID: loginResult.UserGroupID }))
                    dispatch(detailUser({}, loginResult.Usertoken));
                    swal({ title: t('alerts:success'), text: t('alerts:success'), icon: 'success', timer: 3000 });
                    router.replace('/');
                }
            }
        }
    }, [loginResult, loginError]);

    const onSubmit = (data) => {
        const hp = data.formHp;
        const password = data.formPassword;

        dispatch(login({
            Phoneno: hp,
            Password: password
        }));
    }

    const { register, handleSubmit, errors } = useForm();

    return (
        <Container className="login login-alignment">
            <div className="text-center">
                <Image
                    src={'/static/images/logos/baf.svg'}
                    width={153}
                    height={71}
                    layout={'fixed'}
                    alt={'baf'}
                />
                <p>{t('common:pleaseEnter')}</p>
            </div>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <Form.Group>
                    <Form.Label className="font-weight-bold">{t('common:phoneNumber')}</Form.Label>
                    <Form.Control
                        ref={register({
                            required: true,
                            maxLength: 13,
                            pattern: {
                                value: REGEX_HP,
                                message: t('common:invalidHP')
                            }
                        })}
                        name="formHp"
                        type="tel"
                        placeholder={t('common:phoneNumber')}
                        style={{ borderColor: errors.formHp ? '#fa1e0e' : '#8692A6' }}
                        className="txt-blue-baf font-weight-bold" />
                    {errors.formHp && <p className="txt-error">{errors.formHp.message || t('common:pleaseEnterHP')}</p>}
                </Form.Group>
                <Form.Group>
                    <Form.Label className="font-weight-bold">{t('common:password')}</Form.Label>
                    <Form.Control
                        ref={register({ required: true })}
                        name="formPassword"
                        type="password"
                        placeholder={t('common:password')}
                        style={{ borderColor: errors.formPassword ? '#fa1e0e' : '#8692A6' }}
                        className="txt-blue-baf font-weight-bold" />
                    {errors.formPassword && <p className="txt-error">{t('common:pleaseEnterPassword')}</p>}
                </Form.Group>
                <Link href="#">
                    <a className="float-right mb-4 txt-blue-baf"><small>{t('common:forgotPW')} ?</small></a>
                </Link>
                <Button type="submit" className="btn btn-block bg-blue-baf font-weight-normal mb-4 p-2" disabled={loginLoading ? true : false}>
                    {t('common:enter')}
                    {loginLoading && <span className="spinner-border spinner-border-sm ml-2" role="status" aria-hidden="true"></span>}
                </Button>
                <p className="text-center">{t('common:noAccount')} ? <Link href="/register"><a className="txt-blue-baf">{t('common:registerNow')}</a></Link></p>
            </Form>
        </Container>
    )
}