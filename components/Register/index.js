import { Button, Container, Form } from "react-bootstrap";
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from "react-redux";
import { REGEX_EMAIL, REGEX_HP } from "../../utils/constant";
import { regis, setToken, detailUser } from "../../store";
import { useRef, useEffect } from "react";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";

export function RegistrationForm() {
    const { t } = useTranslation(['common', 'alerts'])

    const dispatch = useDispatch();
    const router = useRouter();

    const regisResult = useSelector(state => state.register.result);
    const regisLoading = useSelector(state => state.register.loading);
    const regisError = useSelector(state => state.register.error);
    const isInitialMount = useRef(true);

    useEffect(() => {
        if (isInitialMount.current) {
            isInitialMount.current = false;
        } else {
            if (regisError) {
                swal({ title: t('alerts:warning'), text: `${regisError}` || t('alerts:warningText1'), icon: 'error', timer: 3000 });
            } else if (regisResult) {
                if (regisResult.Usertoken) {
                    dispatch(setToken({ userToken: regisResult.Usertoken, userGroupID: regisResult.UserGroupID }))
                    dispatch(detailUser({}, regisResult.Usertoken));
                    swal({ title: regisResult.message, text: t('alerts:success'), icon: 'success', timer: 3000 });
                    router.replace('/');
                } else {
                    swal({ title: t('alerts:warning'), text: t('alerts:warningText1'), icon: 'error', timer: 3000 });
                }
            }
        }
    }, [regisResult, regisError]);

    const onSubmit = (data) => {
        const nama = data.formNama;
        const hp = data.formHp;
        const email = data.formEmail;
        const password = data.formPassword;
        const kota = data.formKota;
        const tanggal = data.formDOB;

        dispatch(regis({
            Username: nama,
            Password: password,
            Email: email,
            Phoneno: hp,
            BirthPlace: kota,
            BirthDate: tanggal,
            AreaUsr: kota,
            UsrRef: '', //added UsrRef, default: '', with CredolabService function: phoneNo + timestamp
            UsrCrt: nama,
            UsrAdr: '',
            UsrImg: '',
            Gender: '',
        }));
    }

    const { register, handleSubmit, setValue, errors } = useForm();

    return (
        <Container className="login">
            <h3 className="font-weight-bold mb-3 mt-3 fsAlbert">{t('letsRegister')}</h3>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <Form.Group>
                    <Form.Label className="font-weight-bold">{t('fullName')}</Form.Label>
                    <Form.Control
                        ref={register({ required: true })}
                        name="formNama"
                        type="text"
                        placeholder={t('fullName')}
                        style={{ borderColor: errors.formNama ? '#fa1e0e' : '#8692A6' }} />
                    {errors.formNama && <p className="txt-error">{errors.formNama.message || t('missingName')}</p>}
                </Form.Group>
                <Form.Group>
                    <Form.Label className="font-weight-bold">{t('activePhone')}</Form.Label>
                    <Form.Control
                        ref={register({
                            required: true,
                            maxLength: 13,
                            pattern: {
                                value: REGEX_HP,
                                message: t('invalidHP')
                            }
                        })}
                        name="formHp"
                        type="tel"
                        placeholder={t('phoneNumber')}
                        style={{ borderColor: errors.formHp ? '#fa1e0e' : '#8692A6' }} />
                    {errors.formHp && <p className="txt-error">{errors.formHp.message || t('pleaseEnterHP')}</p>}
                </Form.Group>
                <Form.Group>
                    <Form.Label className="font-weight-bold">{t('email')}</Form.Label>
                    <Form.Control
                        ref={register({
                            required: true,
                            pattern: {
                                value: REGEX_EMAIL,
                                message: t('invalidEmail'),
                            }
                        })}
                        name="formEmail"
                        type="text"
                        placeholder={t('email')}
                        style={{ borderColor: errors.formEmail ? '#fa1e0e' : '#8692A6' }} />
                    {errors.formEmail && <p className="txt-error">{errors.formEmail.message || t('pleaseEnterEmail')}</p>}
                </Form.Group>
                <Form.Group>
                    <Form.Label className="font-weight-bold">{t('password')}</Form.Label>
                    <Form.Control
                        ref={register({ required: true })}
                        name="formPassword"
                        type="password"
                        placeholder={t('password')}
                        style={{ borderColor: errors.formPassword ? '#fa1e0e' : '#8692A6' }} />
                    {errors.formPassword && <p className="txt-error">{t('pleaseEnterPassword')}</p>}
                </Form.Group>
                <Form.Group>
                    <Form.Label className="font-weight-bold">{t('city')}</Form.Label>
                    <Form.Control
                        ref={register({ required: true })}
                        name="formKota"
                        type="text"
                        placeholder={t('city')}
                        style={{ borderColor: errors.formKota ? '#fa1e0e' : '#8692A6' }} />
                    {errors.formKota && <p className="txt-error">{errors.formKota.message || t('pleaseEnterCity')}</p>}
                </Form.Group>
                <Form.Group>
                    <Form.Label className="font-weight-bold">{t('dob')}</Form.Label>
                    <Form.Control
                        ref={register({ required: true })}
                        name="formDOB"
                        type="date"
                        placeholder={t('dob')}
                        style={{ borderColor: errors.formDOB ? '#fa1e0e' : '#8692A6' }} />
                    {errors.formDOB && <p className="txt-error">{errors.formDOB.message || t('pleaseEnterDOB')}</p>}
                </Form.Group>
                <Button type="submit" className="btn btn-block bg-blue-baf font-weight-normal mb-4 p-2 register-top" disabled={regisLoading ? true : false}>
                    {t('register')}
                    {regisLoading && <span className="spinner-border spinner-border-sm ml-2" role="status" aria-hidden="true"></span>}

                </Button>
            </Form>
        </Container>
    )
}