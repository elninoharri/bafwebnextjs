import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';

const convertBreadcrumb = string => {
    return string
        .replace(/-/g, ' ')
        .replace(/oe/g, 'ö')
        .replace(/ae/g, 'ä')
        .replace(/ue/g, 'ü');
};

const Breadcrumbs = () => {
    const router = useRouter();
    const [breadcrumbs, setBreadcrumbs] = useState(null);

    useEffect(() => {
        if (router) {
            const linkPath = router.asPath.split('/');
            linkPath.shift();

            const pathArray = linkPath.map((path, i) => {
                return { breadcrumb: path, href: '/' + linkPath.slice(0, i + 1).join('/') };
            });

            setBreadcrumbs(pathArray);
        }
    }, [router]);

    if (!breadcrumbs) {
        return null;
    }

    return (
        <nav aria-label="breadcrumbs">
            <ol className="breadcrumb bg-transparent p-0 mt-2">
                {breadcrumbs.map((breadcrumb, i) => {
                    return (
                        <li key={breadcrumb.href}>
                            <Link href={breadcrumb.href}>
                                <a className="text-breadcumb">
                                    {convertBreadcrumb(breadcrumb.breadcrumb)}
                                </a>
                            </Link>
                            <span className="text-breadcumb">{breadcrumbs.length != i + 1 && ' >'}</span>
                            &nbsp;
                        </li>
                    );
                })}
            </ol>
        </nav>
    );
};

export default Breadcrumbs;