import { useEffect, useState } from 'react';
import { Carousel } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { getBanner } from '../../store/actions/banner';
import ContentLoader from 'react-content-loader';

export function Slider(props) {
    const allData = useSelector((state) => state.getBanner.result.result ? state.getBanner.result.result.AllData : []);
    const allDataLoading = useSelector((state) => state.getBanner.loading ? state.getBanner.loading : false);
    const allDataError = useSelector((state) => state.getBanner.result.error);
    const [dataBanner, setDataBanner] = useState([]);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getBanner({
            Code: "",
            Type: "",
            Category: ""
        }))
    },
        [dispatch]
    )

    useEffect(() => {
        if (allData.length > 0) {
            initData();
        }
    }, [allData])

    const initData = () => {
        let dataBanner = allData.filter(function (item) {
            return item.BannerType === 'BANNER_TYPE_ANNOUNCEMENT';
        })

        if (dataBanner) {
            setDataBanner(dataBanner);
        }
    }

    const renderLoader = () => {
        return (
            <ContentLoader width="100%" height="480">
                <rect x="0" y="0" rx="0" ry="0" width="100%" height="100%" />
            </ContentLoader>
        )
    }

    return (
        <div>
            {
                allDataError ?
                    <span />
                    : allDataLoading ?
                        renderLoader()
                        :
                        (<Carousel>
                            {dataBanner.map(({ BannerID, BannerName, BannerImg }) => {
                                return (
                                    <Carousel.Item key={BannerID}>
                                        <img
                                            className="d-block w-100"
                                            src={BannerImg}
                                            alt={BannerName}
                                            style={{ maxHeight: 480 }}
                                        />
                                    </Carousel.Item>
                                )
                            })}
                        </Carousel>)
            }
        </div>
    )
}