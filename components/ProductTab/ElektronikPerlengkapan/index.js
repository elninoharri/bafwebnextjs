import { Button, Col, Container, Row } from "react-bootstrap";
import Image from 'next/image';
import { useTranslation } from "next-i18next";

export function ElektronikPerlengkapan() {
    const { t } = useTranslation("product")
    return (
        <Container className="txt-black-baf">
            <br></br>
            <Col className="my-auto mx-auto" lg={12} md={12} xs={12}>
                
            </Col>
            <br></br>
            <Col className="" lg={12} md={12} xs={12}>
            <br></br>
            <p>
                {t('mpText')}
            </p>
            <ul>
                <li>
                    <p>{t('electronics')}</p>
                </li>
                <li>
                    <p>{t('furniture')}</p>
                </li>
                <li>
                    <p>{t('gadgets')}</p>
                </li>
                <li>
                    <p>{t('miscellaneous')}</p>
                </li>
            </ul>
            </Col>
            <br></br>
        </Container>
    )    
}
