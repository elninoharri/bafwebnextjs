import { Button, Col, Container, Row } from "react-bootstrap";
import Image from 'next/image';
import { Trans, useTranslation } from "next-i18next";

export function Mobil() {
    const { t } = useTranslation('product');
    return (
        <Container className="txt-black-baf">
            <br></br>
            <Col className="my-auto mx-auto" lg={12} md={12} xs={12}>

            </Col>
            <br></br>
            <Col className="" lg={12} md={12} xs={12}>
                <br></br>
                <p className="display-linebreak">
                    <Trans><b>{t('carText')}</b></Trans>
                </p>
            </Col>
            <br></br>
        </Container>
    )
}
