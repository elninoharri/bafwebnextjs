import { Button, Col, Container, Row } from "react-bootstrap";
import Image from 'next/image';
import { useTranslation, Trans } from "next-i18next";
import ReactMarkdown from "react-markdown";
export function PembiayaanFleet() {
    const { t } = useTranslation('product');
    return (
        <Container className="txt-black-baf">
            <br></br>
            <Col className="my-auto mx-auto" lg={12} md={12} xs={12}>

            </Col>
            <br></br>
            <Col className="" lg={12} md={12} xs={12}>
                <br></br>
                <p>
                    {t('fleetText1')}
                </p>
                <ol>
                    <li>
                        <Trans><b>{t('fleetFinancing1')}</b></Trans>
                    </li>
                    <li>
                        <Trans><b>{t('fleetFinancing2')}</b></Trans>
                    </li>
                </ol>
                <p>{t('fleetText2')}</p>
                <ol>
                    <li>
                        <p>{t('paymentMethod1')}</p>
                    </li>
                    <li>
                        <p>{t('paymentMethod2')}</p>
                    </li>
                    <li>
                        <p>{t('paymentMethod3')}</p>
                    </li>
                </ol>
            </Col>
            <br></br>
        </Container>
    )
}
