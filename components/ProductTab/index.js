import { useState } from "react";
import { Tabs, Tab, Row, Col, Nav, Card, ListGroup, Jumbotron, Container, Button, Form } from "react-bootstrap";
import { PembiayaanFleet } from "./PembiyaanFleet"
import { ElektronikPerlengkapan } from './ElektronikPerlengkapan';
import { DanaSyariah } from './DanaSyariah';
import { Mobil } from './Mobil';
import { NMC } from './NMC';
import { MesinPertanian } from "./MesinPertanian"
import { useDispatch } from "react-redux";
import { setSelectedProduct } from "../../store";
import { cps } from "@redux-saga/core/effects";
import Link from 'next/link';
import { useRouter } from "next/router";
import { SimulationFormCar, SimulationFormNMC, SimulationFormSyana, SimulationFormMP } from "../../components"
import { faUnderline } from "@fortawesome/free-solid-svg-icons";
import { useTranslation } from 'next-i18next';

export function ProductTab() {
    const { t } = useTranslation('product');
    const router = useRouter();

    const tabData = [{
        id: 'NMC',
        tabHeader: 'Motor Yamaha',
        tabContent: <NMC />
    }, {
        id: 'CAR',
        tabHeader: 'Mobil',
        tabContent: <Mobil />
    }, {
        id: 'MP',
        tabHeader: 'Elektronik & Furnitur',
        tabContent: <ElektronikPerlengkapan />
    }, {
        id: 'SYANA',
        tabHeader: 'Dana Syariah',
        tabContent: <DanaSyariah />
    },
    {
        id: 'AGRI',
        tabHeader: 'Mesin Pertanian',
        tabContent: <MesinPertanian />
    },
    {
        id: 'FLEET',
        tabHeader: 'Pembiyaan Fleet',
        tabContent: <PembiayaanFleet />
    }]

    const dispatch = useDispatch();
    const [currentProductTab, setCurrentProductTab] = useState('NMC')

    const setProductSelected = (idProduct) => {
        dispatch(setSelectedProduct(idProduct))
        setCurrentProductTab(idProduct)
    }

    const goToApplicationForm = () => {
        router.push('/application-form');
    }

    const defaultActive = tabData[0].id ? tabData[0].id : '';

    return (
        <Tab.Container id="product-tab" defaultActiveKey={defaultActive}>
            <Row>
                <Col>
                    <Nav variant="pills" className="flex-row nav-justified product-tab">
                        {tabData.map((item, index) => {
                            return (
                                <Nav.Item key={index}>
                                    <Nav.Link
                                        onClick={() => {
                                            setProductSelected(item.id);
                                        }}
                                        eventKey={item.id}
                                        style = {{color: "#002F5F", paddingTop: "10px", paddingBottom: "10px"}}
                                        className="nav-productTabs">{item.tabHeader}
                                    </Nav.Link>
                                </Nav.Item>
                            )
                        })}
                    </Nav>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Tab.Content>
                        {tabData.map((item, index) => {
                            return (
                                <Tab.Pane eventKey={item.id} key={index}>
                                    <div>{item.tabContent}</div>
                                </Tab.Pane>
                            )
                        })}
                    </Tab.Content>
                </Col>
            </Row>
            <br></br>
            <Row>
                <br></br>
                <Col>
                    <Card className="border-0" style={{'backgroundColor':'#F9B419'}}>
                        <br></br>
                        <h4 style={{ textAlign: 'center', color: '#fff', fontFamily: "Brandon Grotesque", fontSize: 35 }}>{t('requirements')}</h4>
                        <Card.Body style={{ 'backgroundColor': '#F9B419' }}>
                            <ListGroup horizontal={'lg'} className="justify-content-center" style={{ 'backgroundColor': '#F9B419' }}>
                                <ListGroup.Item style={{ 'backgroundColor': '#F9B419' }} className="border-0 lob-item">
                                    <img
                                        style={{ 'backgroundColor': '#F9B419', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)" }}
                                        src="/static/images/image/section/product/group-1.png"
                                        className="block-card mt-0 mb-0"
                                    />
                                </ListGroup.Item>
                                <ListGroup.Item style={{ 'backgroundColor': '#F9B419' }} className="border-0 lob-item">
                                    <img
                                        style={{ 'backgroundColor': '#F9B419', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)" }}
                                        src="/static/images/image/section/product/group-2.png"
                                        className="block-card mt-0 mb-0"
                                    />
                                </ListGroup.Item>
                                <ListGroup.Item style={{ 'backgroundColor': '#F9B419' }} className="border-0 lob-item">
                                    <img
                                        style={{ 'backgroundColor': '#F9B419', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)" }}
                                        src="/static/images/image/section/product/group-3.png"
                                        className="block-card mt-0 mb-0"
                                    />
                                </ListGroup.Item>
                                <ListGroup.Item style={{ 'backgroundColor': '#F9B419' }} className="border-0 lob-item">
                                    <img
                                        style={{ 'backgroundColor': '#F9B419', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)" }}
                                        src="/static/images/image/section/product/group-4.png"
                                        className="block-card mt-0 mb-0"
                                    />
                                </ListGroup.Item>
                            </ListGroup>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <br></br>
            {
                currentProductTab === 'FLEET' 
                ? 
                <Card className="border-0" style={{'backgroundColor':'#E0E7EC'}}>
                    <br/>
                    <h4 style={{textAlign:'center',color:'#002F5F', fontFamily: "Brandon Grotesque", fontSize: 35 }}>{t('financingFlow')}</h4>
                    <Card.Body style={{'backgroundColor':'#E0E7EC'}}>
                        <ListGroup horizontal={'lg'} className="justify-content-center" style={{'backgroundColor':'#E0E7EC'}}>
                            <ListGroup.Item style={{'backgroundColor':'#E0E7EC'}} className="border-0 lob-item">
                                <img
                                    style={{'backgroundColor':'#E0E7EC', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                    src="/static/images/image/section/product/ap-1.png"
                                    className="block-card2"
                                />
                            </ListGroup.Item>
                            <img className="ppArrow" src="/static/images/image/section/product/pp-arrow.png" style={{alignSelf:'center', position: 'relative', bottom: '35px'}}/>
                            <img className="ppArrowBottom" src="/static/images/image/section/product/pp-arrowBottom.png" style={{alignSelf:'center'}}/>
                            <ListGroup.Item style={{'backgroundColor':'#E0E7EC'}} className="border-0 lob-item">
                                <img
                                    style={{'backgroundColor':'#E0E7EC', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                    src="/static/images/image/section/product/ap-2.png"
                                    className="block-card2"
                                />
                            </ListGroup.Item>
                            <img className="ppArrow" src="/static/images/image/section/product/pp-arrow.png" style={{alignSelf:'center', position: 'relative', bottom: '35px'}}/>
                            <img className="ppArrowBottom" src="/static/images/image/section/product/pp-arrowBottom.png" style={{alignSelf:'center'}}/>
                            <ListGroup.Item style={{'backgroundColor':'#E0E7EC'}} className="border-0 lob-item">
                                <img
                                    style={{'backgroundColor':'#E0E7EC', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                    src="/static/images/image/section/product/ap-3.png"
                                    className="block-card2"
                                />
                            </ListGroup.Item>
                            <img className="ppArrow" src="/static/images/image/section/product/pp-arrow.png" style={{alignSelf:'center', position: 'relative', bottom: '35px'}}/>
                            <img className="ppArrowBottom" src="/static/images/image/section/product/pp-arrowBottom.png" style={{alignSelf:'center'}}/>
                            <ListGroup.Item style={{'backgroundColor':'#E0E7EC'}} className="border-0 lob-item">
                                <img
                                    style={{'backgroundColor':'#E0E7EC', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                    src="/static/images/image/section/product/ap-4.png"
                                    className="block-card2"
                                />
                            </ListGroup.Item>
                        </ListGroup>
                    </Card.Body>
                </Card>
                : currentProductTab === 'SYANA' ? 
                <Card className="border-0" style={{'backgroundColor':'#E0E7EC'}}>
                    <br/>
                    <h4 style={{textAlign:'center',color:'#002F5F', fontFamily: "Brandon Grotesque", fontSize: 35 }}>{t('submissionProcess')}</h4>
                    <Card.Body style={{'backgroundColor':'#E0E7EC'}}>
                        <ListGroup horizontal={'lg'} className="justify-content-center" style={{'backgroundColor':'#E0E7EC'}}>
                            <ListGroup.Item style={{'backgroundColor':'#E0E7EC'}} className="border-0 lob-item">
                                <img
                                    style={{'backgroundColor':'#E0E7EC', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                    src="/static/images/image/section/product/pp-6.png"
                                    className="block-card2"
                                />
                            </ListGroup.Item>
                            <img className="ppArrow" src="/static/images/image/section/product/pp-arrow.png" style={{alignSelf:'center', position: 'relative', bottom: '35px'}}/>
                            <img className="ppArrowBottom" src="/static/images/image/section/product/pp-arrowBottom.png" style={{alignSelf:'center'}}/>
                            <ListGroup.Item style={{'backgroundColor':'#E0E7EC'}} className="border-0 lob-item">
                                <img
                                    style={{'backgroundColor':'#E0E7EC', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                    src="/static/images/image/section/product/pp-7.png"
                                    className="block-card2"
                                />
                            </ListGroup.Item>
                            <img className="ppArrow" src="/static/images/image/section/product/pp-arrow.png" style={{alignSelf:'center', position: 'relative', bottom: '35px'}}/>
                            <img className="ppArrowBottom" src="/static/images/image/section/product/pp-arrowBottom.png" style={{alignSelf:'center'}}/>
                            <ListGroup.Item style={{'backgroundColor':'#E0E7EC'}} className="border-0 lob-item">
                                <img
                                    style={{'backgroundColor':'#E0E7EC', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                    src="/static/images/image/section/product/pp-8.png"
                                    className="block-card2"
                                />
                            </ListGroup.Item>
                            <img className="ppArrow" src="/static/images/image/section/product/pp-arrow.png" style={{alignSelf:'center', position: 'relative', bottom: '35px'}}/>
                            <img className="ppArrowBottom" src="/static/images/image/section/product/pp-arrowBottom.png" style={{alignSelf:'center'}}/>
                            <ListGroup.Item style={{'backgroundColor':'#E0E7EC'}} className="border-0 lob-item">
                                <img
                                    style={{'backgroundColor':'#E0E7EC', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                    src="/static/images/image/section/product/pp-9.png"
                                    className="block-card2"
                                />
                            </ListGroup.Item>
                            <img className="ppArrow" src="/static/images/image/section/product/pp-arrow.png" style={{alignSelf:'center', position: 'relative', bottom: '35px'}}/>
                            <img className="ppArrowBottom" src="/static/images/image/section/product/pp-arrowBottom.png" style={{alignSelf:'center'}}/>
                            <ListGroup.Item style={{'backgroundColor':'#E0E7EC'}} className="border-0 lob-item">
                                <img
                                    style={{'backgroundColor':'#E0E7EC', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                    src="/static/images/image/section/product/pp-10.png"
                                    className="block-card2"
                                />
                            </ListGroup.Item>
                        </ListGroup>
                    </Card.Body>
                </Card>
                : currentProductTab === 'NMC' || currentProductTab === 'AGRI' || currentProductTab === "MP" || currentProductTab === "CAR" ?
                <Card className="border-0" style={{'backgroundColor':'#E0E7EC'}}>
                    <br/>
                    <h4 style={{textAlign:'center',color:'#002F5F', fontFamily: "Brandon Grotesque", fontSize: 35 }}>{t('submissionProcess')}</h4>
                    <Card.Body style={{'backgroundColor':'#E0E7EC'}}>
                        <ListGroup horizontal={'lg'} className="justify-content-center" style={{'backgroundColor':'#E0E7EC'}}>
                            <ListGroup.Item style={{'backgroundColor':'#E0E7EC'}} className="border-0 lob-item">
                                <img
                                    style={{'backgroundColor':'#E0E7EC', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                    src="/static/images/image/section/product/pp-1.png"
                                    className="block-card2"
                                />
                            </ListGroup.Item>
                            <img className="ppArrow" src="/static/images/image/section/product/pp-arrow.png" style={{alignSelf:'center', position: 'relative', bottom: '35px'}}/>
                            <img className="ppArrowBottom" src="/static/images/image/section/product/pp-arrowBottom.png" style={{alignSelf:'center'}}/>
                            <ListGroup.Item style={{'backgroundColor':'#E0E7EC'}} className="border-0 lob-item">
                                <img
                                    style={{'backgroundColor':'#E0E7EC', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                    src="/static/images/image/section/product/pp-2.png"
                                    className="block-card2"
                                />
                            </ListGroup.Item>
                            <img className="ppArrow" src="/static/images/image/section/product/pp-arrow.png" style={{alignSelf:'center', position: 'relative', bottom: '35px'}}/>
                            <img className="ppArrowBottom" src="/static/images/image/section/product/pp-arrowBottom.png" style={{alignSelf:'center'}}/>
                            <ListGroup.Item style={{'backgroundColor':'#E0E7EC'}} className="border-0 lob-item">
                                <img
                                    style={{'backgroundColor':'#E0E7EC', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                    src="/static/images/image/section/product/pp-3.png"
                                    className="block-card2"
                                />
                            </ListGroup.Item>
                            <img className="ppArrow" src="/static/images/image/section/product/pp-arrow.png" style={{alignSelf:'center', position: 'relative', bottom: '35px'}}/>
                            <img className="ppArrowBottom" src="/static/images/image/section/product/pp-arrowBottom.png" style={{alignSelf:'center'}}/>
                            <ListGroup.Item style={{'backgroundColor':'#E0E7EC'}} className="border-0 lob-item">
                                <img
                                    style={{'backgroundColor':'#E0E7EC', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                    src="/static/images/image/section/product/pp-4.png"
                                    className="block-card2"
                                />
                            </ListGroup.Item>
                            <img className="ppArrow" src="/static/images/image/section/product/pp-arrow.png" style={{alignSelf:'center', position: 'relative', bottom: '35px'}}/>
                            <img className="ppArrowBottom" src="/static/images/image/section/product/pp-arrowBottom.png" style={{alignSelf:'center'}}/>
                            <ListGroup.Item style={{'backgroundColor':'#E0E7EC'}} className="border-0 lob-item">
                                <img
                                    style={{'backgroundColor':'#E0E7EC', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                    src="/static/images/image/section/product/pp-5.png"
                                    className="block-card2"
                                />
                            </ListGroup.Item>
                        </ListGroup>
                    </Card.Body>
                </Card>
                : ''
            }
            
            <br/>
            {
                currentProductTab === 'FLEET' 
                ? ''
                : currentProductTab === 'MP' 
                ?
                <Card className="border-0"style={{'backgroundColor':'#fff'}}>
                    <Card.Body style={{'backgroundColor':'#fff'}}>
                        <div className="text-center">
                            <p >Toko Rekanan Modern Store & E-Commerce</p>
                            <hr style={{width: "25%"}}/>
                            <br/>
                            <img
                                className="companies"
                                src="/static/images/image/section/product/dealer-mp1.png"

                            />
                        </div>
                    </Card.Body>
                    <br/>
                    <Card.Body style={{'backgroundColor':'#fff'}}>
                        <div className="text-center">
                            <p>Toko Rekanan Local Modern Store</p>
                            <hr style={{width: "25%"}}/>
                            <br/>
                            <img
                                className="companies"
                                src="/static/images/image/section/product/dealer-mp2.png"

                            />
                        </div>
                    </Card.Body>
                </Card>
                : 
                <Card className="border-0"style={{'backgroundColor':'#fff'}}>
                    <Card.Body style={{'backgroundColor':'#fff'}}>
                        <div className="text-center">
                            <p>{t('findUsHere')}</p>
                            <br/>
                            <img
                                className="companies"
                                // style={{'backgroundColor':'#fff', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                src="/static/images/image/section/product/companies.png"

                            />
                        </div>
                    </Card.Body>
                </Card>
            }
            <br/>
            {
                <Container className="txt-black-baf">
                    <Col className="" lg={12} md={12} xs={12}>
                        <br></br>
                        <p>
                            {t('agreementText')}
                    </p>
                    </Col>
                    <br></br>
                    <div>
                        {/* <Link href="/application-form"> */}
                        {/* </Link> */}
                        {
                            currentProductTab === "MP" ? <SimulationFormMP/> :
                            currentProductTab === "NMC" ? <SimulationFormNMC/> :  
                            currentProductTab === "CAR" ? <SimulationFormCar/> :
                            currentProductTab === "SYANA" ? <SimulationFormSyana/> : ""
                        }
                        {/* <Button variant="primary" className="bg-blue-baf" onClick={goToApplicationForm}>Ajukan Sekarang</Button> */}
                        
                    </div>
                </Container>
            }
            <br/>
            {
                currentProductTab === 'FLEET' ?
                <Row>
                    <Col>
                        <Card className="border-0" style={{'backgroundColor':'#fff'}}>
                            <h4 style={{textAlign:'center',color:'#002F5F', fontSize: '32px', fontWeight: 'bold'}}>{t('contactUs')}</h4>
                            <Card.Body style={{'backgroundColor':'#fff'}}>
                                <ListGroup horizontal={'lg'} className="justify-content-center" style={{'backgroundColor':'#fff'}}>
                                    <ListGroup.Item style={{'backgroundColor':'#fff'}} className="border-0 lob-item">
                                        <img
                                            style={{'backgroundColor':'#fff', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                            src="/static/images/image/section/product/call-1.png"
                                            className="block-card mt-0 mb-0"
                                        />
                                    </ListGroup.Item>
                                    <ListGroup.Item style={{'backgroundColor':'#fff'}} className="border-0 lob-item">
                                        <img
                                            style={{'backgroundColor':'#fff', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                            src="/static/images/image/section/product/call-2.png"
                                            className="block-card mt-0 mb-0"
                                        />
                                    </ListGroup.Item>
                                    <ListGroup.Item style={{'backgroundColor':'#fff'}} className="border-0 lob-item">
                                        <img
                                            style={{'backgroundColor':'#fff', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                            src="/static/images/image/section/product/email-1.png"
                                            className="block-card mt-0 mb-0"
                                        />
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card.Body>
                        </Card>
                    </Col>
                    <br></br>
                    <Col>
                        <Card className="border-0" style={{'backgroundColor':'#fff'}}>
                            <Card.Body style={{'backgroundColor':'#fff'}}>
                                <ListGroup horizontal={'lg'} className="justify-content-center" style={{'backgroundColor':'#fff'}}>
                                    <ListGroup.Item style={{'backgroundColor':'#fff'}} className="border-0 lob-item">
                                        <img
                                            style={{'backgroundColor':'#fff', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                            src="/static/images/image/section/product/email-2.png"
                                            className="block-card mt-0 mb-0"
                                        />
                                    </ListGroup.Item>
                                    <ListGroup.Item style={{'backgroundColor':'#fff'}} className="border-0 lob-item">
                                        <img
                                            style={{'backgroundColor':'#fff', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                            src="/static/images/image/section/product/email-3.png"
                                            className="block-card mt-0 mb-0"
                                        />
                                    </ListGroup.Item>
                                    <ListGroup.Item style={{'backgroundColor':'#fff'}} className="border-0 lob-item">
                                        <img
                                            style={{'backgroundColor':'#fff', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                            src="/static/images/image/section/product/email-4.png"
                                            className="block-card mt-0 mb-0"
                                        />
                                    </ListGroup.Item>
                                    <ListGroup.Item style={{'backgroundColor':'#fff'}} className="border-0 lob-item">
                                        <img
                                            style={{'backgroundColor':'#fff', boxShadow: "0 0px 0px rgb(0 0 0 / 0%)"}}
                                            src="/static/images/image/section/product/email-5.png"
                                            className="block-card mt-0 mb-0"
                                        />
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            : ''
            }

        </Tab.Container>
    )
}