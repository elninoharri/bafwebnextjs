import { Card, Button } from "react-bootstrap";
import { useTranslation } from 'next-i18next';

export function CustomCard(props) {
    const { t } = useTranslation('common');
    return (
        <>
            <Card className={`block-card news-card h-88 ${props.isFull ? 'w-100' : ''}`}>
                <Card.Img variant="top" src={props.imgSource} className="img-fluid" onClick={props.handleClick} />
                <Card.Body className="d-flex flex-column">
                    <div className="d-flex justify-content-between align-items-center mb-2">
                        {props.tag && <span className={props.tagDetail === "Promo" ? "tag-yellow" : "tag-blue"}>{props.tagDetail}</span>}
                        {props.cardDateType == 'news' && <span className="txt-yellow">{props.cardDate}</span>}
                    </div>
                    {props.cardDateType == 'promotion' && <Card.Text><small className="text-muted">{props.cardDate}</small></Card.Text>}
                    {props.tagDetail == 'Promo' && <Card.Text><small className="text-muted">{props.cardDateEnd}</small></Card.Text>}
                    <Card.Title className="card-text-truncate txt-blue-baf">{props.cardTitle}</Card.Title>
                    <div className="card-text-truncate mb-3" dangerouslySetInnerHTML={{ __html: props.cardText }} />
                    <Button className="mt-auto ml-auto bg-blue-baf w-50" onClick={props.handleClick}>{t('read')}</Button>
                </Card.Body>
            </Card>
        </>
    )
}



