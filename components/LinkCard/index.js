import { Card, ListGroup, Button } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { setSelectedLob } from "../../store";

export function LinkCard() {
    const listPrd = [
        {
            'id': 'moto',
            'icon': '/static/images/icons/motorcycle-blue.svg',
            'title': 'Motor Baru Yamaha'
        },
        // {
        //     'icon': '/static/images/icons/motorcycle-yellow.svg',
        //     'title': 'Motor Bekas Berkualitas'
        // },
        {
            'id': 'car',
            'icon': '/static/images/icons/car-yellow.svg',
            'title': 'Mobil Baru'
        },
        {
            'id': 'mp',
            'icon': '/static/images/icons/electronics-blue.svg',
            'title': 'Elektronik & Perlengkapan Rumah Tangga'
        },
        {
            'id': 'syana',
            'icon': '/static/images/icons/money-yellow.svg',
            'title': 'Dana Syariah'
        }
        // ,
        // {
        //     'icon': '/static/images/icons/farm-yellow.svg',
        //     'title': 'Mesin Pertanian'
        // }
    ]

    const listPrd2 = [
        {
            'id': 'NMC',
            'icon': '/static/images/image/lob/nmc-white.svg',
            'title': 'Motor Baru Yamaha'
        },
        // {
                // 'id': 'moto',
        //     'icon': '/static/images/image/lob/umc.svg',
        //     'title': 'Motor Bekas Berkualitas'
        // },
        {
            'id': 'CAR',
            'icon': '/static/images/image/lob/car-white.svg',
            'title': 'Mobil Baru'
        },
        {
            'id': 'MP',
            'icon': '/static/images/image/lob/mp-white.svg',
            'title': 'Elektronik & Perlengkapan Rumah Tangga'
        },
        {
            'id': 'SYANA',
            'icon': '/static/images/image/lob/syana-white.svg',
            'title': 'Dana Syariah'
        }
        // ,
        // {
            // 'id':'agri'
        //     'icon': '/static/images/image/lob/agri.svg',
        //     'title': 'Mesin Pertanian'
        // }
    ]

    const dispatch = useDispatch();

    const setLobSelected = (idLob) => {
        dispatch(setSelectedLob(idLob))
    }



    return (
        <Card>
            <Card.Body>
                <ListGroup horizontal={'lg'} className="justify-content-center">
                    {listPrd2.map((item, index) => {
                        return (
                            <ListGroup.Item key={index} className="border-0 lob-item">
                                <Button className="bg-transparent border-0" onClick={() => {
                                    setLobSelected(item.id);
                                }}>
                                    {/* <Card style={{ width: '15rem', height: '10rem', padding: 10 }} className={index % 2 == 0 ? "bg-yellow-gradient block-card flex-row" : "bg-blue-gradient block-card flex-row"}>
                                        <Card.Img src={item.icon} style={{ height: 75, width: '55%' }} className="my-auto" />
                                        <Card.Text className="text-center txt-white">
                                            {item.title}
                                        </Card.Text>
                                    </Card> */}
                                    <img
                                        src={item.icon}
                                        className="block-card lob-img mt-0 mb-0"
                                        alt={item.id}
                                    />
                                </Button>
                            </ListGroup.Item>
                        )
                    })}
                </ListGroup>
            </Card.Body>
        </Card>
    )
}