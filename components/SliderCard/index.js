import { Button, Card, Col, Container, Row } from "react-bootstrap"
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretLeft, faCaretRight, faCircle, faCircleNotch } from '@fortawesome/free-solid-svg-icons'
import { CustomCard } from "../Card";
import { useTranslation } from 'next-i18next';
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { capitalizeFirstLetter, getTagFromType, titleToUrl } from "../../utils/utilization";
import { useRouter } from "next/router";
import { newsData } from "../../store/actions";
import ContentLoader from 'react-content-loader';
import { useMediaQuery } from 'react-responsive'

export function SliderCard() {
    const { t } = useTranslation('home');
    const dispatch = useDispatch();
    const router = useRouter();
    const [data, setData] = useState([]);
    const allData = useSelector((state) => state.getBanner.result.result ? state.getBanner.result.result.AllData : []);
    const allDataLoading = useSelector((state) => state.getBanner.loading ? state.getBanner.loading : false);

    const isDesktop = useMediaQuery({ minWidth: 1024 })
    const isBigScreen = useMediaQuery({ minWidth: 1824 })
    const isTablet = useMediaQuery({ minWidth: 464, maxWidth: 1024 })
    const isMobile = useMediaQuery({ maxWidth: 464 })


    useEffect(() => {
        if (allData.length > 0) {
            initData();
        }
    }, [allData])

    const initData = () => {
        let dataALL = allData.filter(function (item) {
            return item.BannerType != 'BANNER_TYPE_ANNOUNCEMENT';
        })

        setData(dataALL);
    }


    const responsive = {
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 3,
            slidesToSlide: 3 // optional, default to 1.
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 2,
            slidesToSlide: 2 // optional, default to 1.
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1,
            slidesToSlide: 1 // optional, default to 1.
        }
    };

    const ButtonGroup = ({ next, previous, goToSlide, ...rest }) => {
        const { carouselState: { currentSlide } } = rest;
        return (
            <div className="carousel-button-group bg-transparent arrow-container">
                <Button className={currentSlide === 0 ? 'bg-transparent disable border-0' : 'bg-transparent border-0'} onClick={() => previous()} style={{ left: 20, position: 'absolute' }}>
                    <FontAwesomeIcon icon={faCaretLeft} color="#002F5F" size={'5x'} />
                </Button>
                <Button onClick={() => next()} style={{ right: 20, position: 'absolute' }} className="bg-transparent border-0">
                    <FontAwesomeIcon icon={faCaretRight} color="#002F5F" size={'5x'} />
                </Button>
            </div>
        );
    };

    const CustomDot = ({ onClick, active, index, carouselState }) => {
        const { currentSlide } = carouselState;
        return (
            <li className="bg-transparent border-0">
                <Button
                    className="bg-transparent border-0"
                    onClick={() => onClick()}
                >
                    <FontAwesomeIcon icon={active ? faCircle : faCircleNotch} color={active ? '#002F5F' : 'grey'} size={'1x'} />
                </Button>
            </li>
        );
    };

    const handleDetail = (items, id) => {
        const type = items.BannerType;
        const category = (items.BannerCat).toLowerCase();
        const date = items.date_num || '';
        const dateto = items.date_to_num || '';
        const img = items.ContentPageList ? items.ContentPageList[0].ContentUrl : '';
        const title = items.ContentPageList ? items.ContentPageList[0].ContentName : '';
        const content = items.ContentPageList ? items.ContentPageList[0].ContentDescr : '';
        const bannerId = items.BannerID;
        const bannerCode = items.BannerCode;
        const tag = getTagFromType(type, category);

        const slug = titleToUrl(title);
        dispatch(newsData(
            {
                bannerCode: bannerCode,
                category: category,
                img: img,
                title: title,
                date: date,
                dateto: dateto,
                content: content,
                hasPIC: true
            }
        ))

        if (tag == 'Article') {
            router.push('/article/[id]/[title]', `/article/${bannerId}/${slug}`);

        } else if (tag == 'Promo') {
            router.push('/promo/[id]/[title]', `/promo/${bannerId}/${slug}`);
        } else if (tag == 'Event') {
            router.push('/event/[id]/[title]', `/event/${bannerId}/${slug}`);
        } else {
            router.push('/aboutus/[newstype]/[id]/[title]', `/aboutus/${category}/${bannerId}/${slug}`);
        }
    }

    const handleTag = (item) => {
        const type = item.BannerType;
        const category = item.BannerCat;

        return getTagFromType(type, category);
    }

    const renderLoader = props => {
        return (
            <ContentLoader height={isMobile ? '300' : '450'} width={'960'} {...props}>
                {!isMobile && <rect x="320" y="0" rx="30" ry="30" width="300" height="350" />}
                <rect x="10" y="0" rx="30" ry="30" width={isMobile ? '100%' : '300'} height={isMobile ? '100%' : '350'} />
                {!isMobile && <rect x="630" y="0" rx="30" ry="30" width="300" height="350" />}
                <circle cx="48%" cy="380" r="8" />
                <circle cx="50%" cy="380" r="8" />
                <circle cx="52%" cy="380" r="8" />
            </ContentLoader>
        )
    }

    return (
        <Container className="section-container" fluid >
            <div className="mb25">
                <h3 className="section-title" style={{ textAlign: 'center' }}>{t('promoNews')}</h3>
            </div>
            {allDataLoading ?
                <div className="d-flex justify-content-center">
                    {
                        renderLoader()
                    }
                </div>
                :
                <Carousel
                    className="carousel-styling"
                    swipeable={true}
                    draggable={false}
                    showDots={true}
                    responsive={responsive}
                    ssr={true} // means to render carousel on server-side.
                    infinite={true}
                    autoPlay={false}
                    autoPlaySpeed={3000}
                    keyBoardControl={true}
                    customTransition="all .5"
                    transitionDuration={500}
                    containerClass="container-ca"
                    removeArrowOnDeviceType={["tablet", "mobile"]}
                    dotListClass="custom-dot-list-style"
                    itemClass="carousel-item-padding-40-px container-item-ca"
                    renderButtonGroupOutside={data.length > 0 ? true : false}
                    customButtonGroup={<ButtonGroup />}
                    arrows={false}
                    customDot={<CustomDot />}
                >
                    {data.map((item, key) => {
                        return (
                            <div key={key} className="h-100" style={{ display: 'flex', margin: 'auto', justifyContent: 'center', alignItems: 'center', padding: '5px' }}>
                                <CustomCard
                                    imgSource={item.BannerImg}
                                    tag={item.BannerType}
                                    tagDetail={handleTag(item)}
                                    cardTitle={item.ContentPageList ? item.ContentPageList[0].ContentName : ''}
                                    cardDate={item.date_num}
                                    cardDateType={'promotion'}
                                    handleClick={() => { handleDetail(item, key) }}
                                    isFull={true}
                                />
                            </div>
                        )
                    })}
                </Carousel>
            }

        </Container>
    )
}