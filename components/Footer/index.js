import { Button, Col, Container, ListGroup, Row, Form, InputGroup } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { useTranslation } from 'next-i18next';

export function Footer(){
    const { t, i18n } = useTranslation('footer');

    return(
        <Container className="section-container" fluid>
        <Row>
            <Col lg={6} md={6} xs={12} className="text-center my-auto left-footer" 
            >   
                <div className="column-margin txt-blue-baf">
                    <h4 className="font-weight-bold">PT. Bussan Auto Finance</h4>
                    <h5 className="font-weight-bold">BAF Plaza</h5>
                    <p>Jalan Raya Tanjung Barat No.121 RT 14/RW 4 <br/> Kelurahan Tanjung Barat. Kecamatan Jagakarsa, <br/> Kota Jakarta 12530</p>
                    <Button variant={'primary'} className="bg-blue-baf oval-button">{t('visit')}</Button>
                    <ListGroup horizontal className="justify-content-center">
                        <ListGroup.Item style={{paddingRight: "0px"}} className="border-0 bg-transparent">
                            <a href="https://web.facebook.com/BAF.Indonesia/" target="_blank" className="my-0">
                                <svg width="30" height="30" className="svg-inline--fa f-icon fa-facebook-f fa-w-10" aria-hidden="true" focusable={false} data-prefix="fab" data-icon="facebook-f" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg=""><path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path></svg>
                            </a>
                        </ListGroup.Item>
                        <ListGroup.Item style={{paddingRight: "0px"}} className="border-0 bg-transparent">
                            <a href="https://twitter.com/baf_indo" target="_blank"> <svg width="30" height="30"  className="svg-inline--fa f-icon fa-twitter fa-w-16" aria-hidden="true" focusable={false} data-prefix="fab" data-icon="twitter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path></svg></a>
                        </ListGroup.Item>
                        <ListGroup.Item style={{paddingRight: "0px"}} className="border-0 bg-transparent">
                            <a href="https://www.instagram.com/bafindonesia" target="_blank"> <svg width="30" height="30" className="svg-inline--fa f-icon fa-instagram fa-w-14" aria-hidden="true" focusable={false} data-prefix="fab" data-icon="instagram" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path></svg></a>
                        </ListGroup.Item>
                        <ListGroup.Item style={{paddingRight: "0px"}} className="border-0 bg-transparent">
                            <a href="https://www.youtube.com/channel/UCAzIE1dze2Ia_b90b1TK7zg" target="_blank"> <svg width="30" height="30" className="svg-inline--fa f-icon fa-youtube fa-w-18" aria-hidden="true" focusable={false} data-prefix="fab" data-icon="youtube" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z"></path></svg></a>
                        </ListGroup.Item>
                        <ListGroup.Item style={{paddingRight: "0px"}} className="border-0 bg-transparent">
                            <a href="https://line.me/R/ti/p/%40nyd4148d" target="_blank"> <svg width="30" height="30" className="svg-inline--fa f-icon fa-line fa-w-14" aria-hidden="true" focusable={false} data-prefix="fab" data-icon="line" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M272.1 204.2v71.1c0 1.8-1.4 3.2-3.2 3.2h-11.4c-1.1 0-2.1-.6-2.6-1.3l-32.6-44v42.2c0 1.8-1.4 3.2-3.2 3.2h-11.4c-1.8 0-3.2-1.4-3.2-3.2v-71.1c0-1.8 1.4-3.2 3.2-3.2H219c1 0 2.1.5 2.6 1.4l32.6 44v-42.2c0-1.8 1.4-3.2 3.2-3.2h11.4c1.8-.1 3.3 1.4 3.3 3.1zm-82-3.2h-11.4c-1.8 0-3.2 1.4-3.2 3.2v71.1c0 1.8 1.4 3.2 3.2 3.2h11.4c1.8 0 3.2-1.4 3.2-3.2v-71.1c0-1.7-1.4-3.2-3.2-3.2zm-27.5 59.6h-31.1v-56.4c0-1.8-1.4-3.2-3.2-3.2h-11.4c-1.8 0-3.2 1.4-3.2 3.2v71.1c0 .9.3 1.6.9 2.2.6.5 1.3.9 2.2.9h45.7c1.8 0 3.2-1.4 3.2-3.2v-11.4c0-1.7-1.4-3.2-3.1-3.2zM332.1 201h-45.7c-1.7 0-3.2 1.4-3.2 3.2v71.1c0 1.7 1.4 3.2 3.2 3.2h45.7c1.8 0 3.2-1.4 3.2-3.2v-11.4c0-1.8-1.4-3.2-3.2-3.2H301v-12h31.1c1.8 0 3.2-1.4 3.2-3.2V234c0-1.8-1.4-3.2-3.2-3.2H301v-12h31.1c1.8 0 3.2-1.4 3.2-3.2v-11.4c-.1-1.7-1.5-3.2-3.2-3.2zM448 113.7V399c-.1 44.8-36.8 81.1-81.7 81H81c-44.8-.1-81.1-36.9-81-81.7V113c.1-44.8 36.9-81.1 81.7-81H367c44.8.1 81.1 36.8 81 81.7zm-61.6 122.6c0-73-73.2-132.4-163.1-132.4-89.9 0-163.1 59.4-163.1 132.4 0 65.4 58 120.2 136.4 130.6 19.1 4.1 16.9 11.1 12.6 36.8-.7 4.1-3.3 16.1 14.1 8.8 17.4-7.3 93.9-55.3 128.2-94.7 23.6-26 34.9-52.3 34.9-81.5z"></path></svg></a>
                        </ListGroup.Item> 
                    </ListGroup>
                    <p>&copy; 2021 PT Bussan Auto Finance</p>
                </div>
            </Col>
            <Col lg={6} md={6} xs={12} className="text-center my-auto right-footer"
            >
                <>
                    <p className="txt-blue-baf display-linebreak">{t('emailSubcription')}</p>
                    <Form.Row>
                        <Form.Group as={Col}>
                            <InputGroup style={{ maxWidth:350 }} className="mx-auto">
                                <Form.Control
                                    type="text"
                                    placeholder={t('enterEmail')}
                                    className="bg-yellow txt-email txt-white"
                                />
                                <InputGroup.Append>
                                    <InputGroup.Text className="bg-yellow txt-white">
                                        <FontAwesomeIcon icon={faEnvelope} color="white"/>
                                    </InputGroup.Text>
                                </InputGroup.Append>
                            </InputGroup>
                        </Form.Group>
                    </Form.Row> 
                    <Form.Group controlId="formBasicCheckbox">
                        <Form.Check type="checkbox" label={t('conditions')}  className="txt-blue-baf"/>
                    </Form.Group>
                    <Button variant={'primary'} className="bg-blue-baf w-25">{t('submit')}</Button>
                </>
            </Col>
        </Row>
        </Container>
    )
}