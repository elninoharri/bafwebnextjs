import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Card, Button, Row, Col, Tab, Nav, Pagination } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { getBanner, newsData } from "../../../store/actions";
import { CustomCard } from '../../Card';
import { useTranslation } from 'next-i18next';

import { capitalizeFirstLetter, titleToUrl } from '../../../utils/utilization'
export function NewsEvent() {
    const { t } = useTranslation('common');

    const router = useRouter();
    const dispatch = useDispatch();

    const [pagingData, setPagingData] = useState({
        activePageALL: 1,
        dataPagingALL: [],
        activePageALL: 1,
        activePageNEWS: 1,
        dataPagingNEWS: [],
        activePageEVENT: 1,
        dataPagingEVENT: [],
        activePagePERS: 1,
        dataPagingPERS: []
    });

    const allData = useSelector((state) => state.getBanner.result.result ? state.getBanner.result.result.AllData : []);

    useEffect(() => {
        if (allData.length < 1) {
            dispatch(getBanner({
                Code: "",
                Type: "",
                Category: ""
            }))
        }
    }, [])

    useEffect(() => {
        if (allData.length > 0) {
            initData();
        }
    }, [allData])

    const initData = () => {
        let dataALL = allData.filter(function (item) {
            return item.BannerType != 'BANNER_TYPE_ANNOUNCEMENT'
                && item.BannerType != 'BANNER_TYPE_ARTICLE';
        })

        let dataNEWS = allData.filter(function (item) {
            return item.BannerType === 'BANNER_TYPE_NEWS' && item.BannerCat === 'BERITA';
        })

        let dataEVENT = allData.filter(function (item) {
            return item.BannerType === 'BANNER_TYPE_NEWS' && item.BannerCat === 'ACARA';
        })

        let dataPERS = allData.filter(function (item) {
            return item.BannerType === 'BANNER_TYPE_NEWS' && item.BannerCat === 'PERS';
        })

        let dataPaginateAll = dataALL.length > 0 ? paginate(dataALL, 4, 1) : [];
        let dataPaginateNews = dataNEWS.length > 0 ? paginate(dataNEWS, 4, 1) : [];
        let dataPaginateEvent = dataEVENT.length > 0 ? paginate(dataEVENT, 4, 1) : [];
        let dataPaginatePers = dataPERS.length > 0 ? paginate(dataPERS, 4, 1) : [];

        setPagingData(prevState => {
            return {
                ...prevState,
                dataPagingALL: dataPaginateAll,
                dataPagingNEWS: dataPaginateNews,
                dataPagingEVENT: dataPaginateEvent,
                dataPagingPERS: dataPaginatePers
            };
        })
    }

    const handleDetail = (items, id) => {
        const category = (items.BannerCat).toLowerCase();
        const date = items.date_num || '';
        const dateto = items.date_to_num || '';
        const img = items.ContentPageList ? items.ContentPageList[0].ContentUrl : '';
        const title = items.ContentPageList ? items.ContentPageList[0].ContentName : '';
        const content = items.ContentPageList ? items.ContentPageList[0].ContentDescr : '';
        const bannerId = items.BannerID;
        const bannerCode = items.BannerCode;

        const slug = titleToUrl(title);
        dispatch(newsData(
            {
                bannerCode: bannerCode,
                category: category,
                img: img,
                title: title,
                date: date,
                dateto: dateto,
                content: content,
                hasPIC: true
            }
        ))

        router.push('/aboutus/[newstype]/[id]/[title]', `/aboutus/${category}/${bannerId}/${slug}`);
    }

    const handlePageChange = (page, flag) => {
        let dataPaginate = paginate(pagingData[`data${flag}`], 4, page);
        setPagingData(prevState => {
            return {
                ...prevState,
                [`activePage${flag}`]: page,
                [`dataPaging${flag}`]: dataPaginate,
            };
        })
    }

    const paginate = (array, page_size, page_number) => {
        return array.slice((page_number - 1) * page_size, page_number * page_size);
    }


    const renderData = (flag) => {
        let items = [];
        let active = pagingData[`activePage${flag}`];
        let data = pagingData[`dataPaging${flag}`];
        let length = data.length > 0 ? Math.ceil(data.length / 4) : 0;

        for (let number = 1; number <= length; number++) {
            items.push(
                <Pagination.Item key={number} active={number === active} onClick={() => { handlePageChange(number, flag) }}>
                    {number}
                </Pagination.Item>,
            );
        }

        return (
            <>
                {
                    data.length > 0 ?
                        <>
                            <Row>
                                {data.map((item, index) => {
                                    return (
                                        <Col key={index} lg={6} sm={12} md={6} className="align-item-stretch d-flex">
                                            <CustomCard
                                                imgSource={item.BannerImg}
                                                tag={item.BannerType}
                                                tagDetail={capitalizeFirstLetter(item.BannerCat) || 'Other'}
                                                cardTitle={item.ContentPageList ? item.ContentPageList[0].ContentName : ''}
                                                cardDate={item.date}
                                                cardDateType={'news'}
                                                handleClick={() => { handleDetail(item, index) }}
                                                isFull={true}
                                            />
                                        </Col>

                                    )
                                })}
                            </Row>
                            <Pagination className="float-right pagination-news">
                                {items}
                            </Pagination>
                        </>
                        :
                        <Row>
                            <Col>
                                <div className="d-flex justify-content-center mt-5">{t('noResults')}</div>
                            </Col>
                        </Row>
                }
            </>
        )
    }

    const tabData = [{
        id: 'ALL',
        tabHeader: `${t('all')}`,
        tabContent: renderData('ALL')
    }, {
        id: 'NEWS',
        tabHeader: `${t('news')}`,
        tabContent: renderData('NEWS')
    }, {
        id: 'EVENT',
        tabHeader: `${t('events')}`,
        tabContent: renderData('EVENT')
    }, {
        id: 'PERS',
        tabHeader: `${t('pressConference')}`,
        tabContent: renderData('PERS')
    }]

    const onChangeTab = (tab) => {
        // console.log(tab)
    }

    const defaultActive = tabData[0].id ? tabData[0].id : '';

    return (
        <>
            <Tab.Container id="news-tab" defaultActiveKey={defaultActive}>
                <Row>
                    <Col lg={8} md={12} xs={12} className="mx-auto">
                        <Nav variant="pills" className="flex-row nav-justified nav-news">
                            {tabData.map((item, index) => {
                                return (
                                    <Nav.Item key={index}>
                                        <Nav.Link
                                            onClick={() => {
                                                onChangeTab(item.id);
                                            }}
                                            eventKey={item.id}
                                            style={{ color: "#002F5F", paddingTop: "10px", paddingBottom: "10px" }}
                                            className="nav-news-link">{item.tabHeader}
                                        </Nav.Link>
                                    </Nav.Item>
                                )
                            })}
                        </Nav>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Tab.Content>
                            {tabData.map((item, index) => {
                                return (
                                    <Tab.Pane eventKey={item.id} key={index}>
                                        <div>{item.tabContent}</div>
                                    </Tab.Pane>
                                )
                            })}
                        </Tab.Content>
                    </Col>
                </Row>
            </Tab.Container>
        </>
    )
}