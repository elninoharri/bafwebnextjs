import Image from "next/image";
import { useState } from "react";
import { Card, ListGroup, Button } from "react-bootstrap";
import { useTranslation } from 'next-i18next';

export function Commissioner() {
    const { t } = useTranslation('commissioners');

    const data = [{
        'id': '1',
        'img': '/static/images/image/commissioner/Jiro-Yamada.jpg',
        'name': 'Jiro Yamada',
        'title': t('presidentCommissioner'),
        'intro': t('intro1'),
        'deskripsi': t('commissionerDesc1'),
    }, {
        'id': '2',
        'img': '/static/images/image/commissioner/Minoru-Morimoto.jpg',
        'name': 'Minoru Morimoto',
        'title': t('commissioner'),
        'intro': t('intro2'),
        'deskripsi': t('commissionerDesc2'),
    }, {
        'id': '3',
        'img': '/static/images/image/commissioner/Jun-Ikeda.jpg',
        'name': 'Jun Ikeda',
        'title': t('commissioner'),
        'intro': t('intro3'),
        'deskripsi': t('commissionerDesc3'),
    }, {
        'id': '4',
        'img': '/static/images/image/commissioner/Dani-Firmansjah.jpg',
        'name': 'Dan Firmansjah',
        'title': t('independentCommissioner'),
        'intro': t('intro4'),
        'deskripsi': t('commissionerDesc4'),
    }, {
        'id': '5',
        'img': '/static/images/image/commissioner/Prabowo.jpg',
        'name': 'Prabowo',
        'title': t('independentCommissioner'),
        'intro': t('intro5'),
        'deskripsi': t('commissionerDesc5'),
    }, {
        'id': '6',
        'img': '/static/images/image/commissioner/Nurdayadi.jpg',
        'name': 'Nurdayadi',
        'title': t('independentCommissioner'),
        'intro': t('intro6'),
        'deskripsi': t('commissionerDesc6'),
    }]

    const [readMore, setReadMore] = useState({
        'card1': "test-truncate",
        'card2': "test-truncate",
        'card3': "test-truncate",
        'card4': "test-truncate",
        'card5': "test-truncate",
        'card6': "test-truncate",
        'card7': "test-truncate"
    });


    const handleClick = (id) => {
        if(readMore['card' + id] == "test-truncate"){
            setReadMore(prevState => {
            return {...prevState, ['card' + id]: "test-readMore"}
        })}
        else{
            setReadMore(prevState => {
                return {...prevState, ['card' + id]: "test-truncate"}
            })
        }
    }

    return (
        <>
            {
                data.map((item, index) => {
                    return (
                        <Card className="block-card w-auto boards-card" key={index}>
                            <Card.Header className="justify-content-center d-flex align-items-center">
                                <img src={item.img} className="boards-image" layout={'fixed'} />
                            </Card.Header>
                            <Card.Body className="halfWidth">
                                <Card.Title className="txt-boards-name fsAlbert">{item.name}</Card.Title>
                                <Card.Text className="txt-boards-title">{item.title}</Card.Text>
                                <Card.Text className="txt-boards-desc">
                                    <p>{item.intro}</p>
                                    <p className={readMore['card' + item.id]}>{item.deskripsi}</p>
                                    {
                                        readMore['card' + item.id] === "test-truncate" ? 
                                        <Button onClick={() => handleClick(item.id)} className="border-0 bg-transparent txt-blue-baf p-0 fsAlbert">{'Read more >>>'}</Button>
                                        :
                                        <Button onClick={() => handleClick(item.id)} className="border-0 bg-transparent txt-blue-baf p-0 fsAlbert">{'Read less <<<'}</Button>
                                    }
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    )
                })
            }
        </>
    )
}