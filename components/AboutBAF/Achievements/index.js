import { Card, Button, Image } from "react-bootstrap";
import { useTranslation } from 'next-i18next';
import { useState, useEffect } from "react";

export function Achievements() {

    const { t } = useTranslation('aboutus');

    var desktop = window.matchMedia("(min-width: 990px)");
    var others = window.matchMedia("(min-width: 0px) and (max-width: 989px)");

    const [showMore, setShowMore] = useState({
        'btnText': t('readMore'),
        'btnIcon': '/static/images/icons/chevrons-down.svg',
        'itemsToShow': 0,
    });

    useEffect(() => {
        if(desktop.matches){
            setShowMore({ 
                'btnText': t('readMore'),
                'btnIcon': '/static/images/icons/chevrons-down.svg',
                'itemsToShow': 6})
        }
        else if(others.matches){
            setShowMore({
                'btnText': t('readMore'),
                'btnIcon': '/static/images/icons/chevrons-down.svg',
                'itemsToShow': 4})
        }
    }, [])

    

    const { btnText, btnIcon } = showMore;

    const data = [{
        "year": "2021",
        "image": "TOP DIGITAL INNOVATION AWARD.svg",
    },{
        "year": "2021",
        "image": "TOP DIGITAL PR AWARD.svg",
    },{
        "year": "2021",
        "image": "INDONESIA PUBLIC RELATION AWARD (IPRA).svg",
    },{
        "year": "2021",
        "image": "INDONESIA DIGITAL POPULAR BRAND AWARD (IDPBA).svg",
    },{
        "year": "2021",
        "image": "INDONESIA DIGITAL INNOVATION AWARD (IDIA).svg",
    },{
        "year": "2021",
        "image": "ICONOMICS SYARIAH AWARD.svg",
    },{
        "year": "2021",
        "image": "ICONOMICS MULTIFINANCE AWARD.svg",
    },{
        "year": "2021",
        "image": "ICONOMICS INSPIRING WOMEN AWARD.svg",
    },{
        "year": "2020",
        "image": "INFOBANK MULTIFINANCE AWARD.svg",
    },{
        "year": "2020",
        "image": "TOP DIGITAL INNOVATION AWARD.svg",
    },{
        "year": "2020",
        "image": "INDONESIA DIGITAL POPULAR BRAND AWARD - KATEGORI PEMBIAYAAN FLEET.svg",
    },{
        "year": "2020",
        "image": "INDONESIA DIGITAL POPULAR BRAND AWARD - KATEGORI PEMBIAYAAN MESIN PERTANIAN.svg",
    },{
        "year": "2020",
        "image": "INDONESIA WOW BRAND AWARD 2020.svg",
    },{
        "year": "2020",
        "image": "INDONESIA BEST MULTIFINANCE AWARD.svg",
    }]

    const handleReadMore = () => {
        if(showMore['itemsToShow'] == 6 || showMore['itemsToShow'] == 4 ){
            setShowMore({
                'itemsToShow': data.length,
                'btnText': t('showLess'),
                'btnIcon': '/static/images/icons/chevrons-up.svg'
            })
        }
        else{
            setShowMore({
                'itemsToShow': desktop.matches ? 6 : 4,
                'btnText': t('showMore'),
                'btnIcon': '/static/images/icons/chevrons-down.svg'
            })
        }
    }

    return(
        <div className="justify-content-center text-center">
            <div style={{overflow: "hidden", display: "inline-block"}}>
                <div className="achievement-container" >
                { data.length > 0 && data.slice(0, showMore['itemsToShow']).map((item, index) => { return(
                    <Card key={index} className="achievement-card">
                        <Card.Header className="achievement-header">{item.year}</Card.Header>
                        <Card.Body>
                        <img src={`/static/images/image/achievements/${item.image}`} className="achievement-image" />
                        <Card.Title className="txt-blue-baf fsAlbert achievement-title text-center">{item.image.slice(0,-4)}</Card.Title>
                        </Card.Body>            
                    </Card>
                )})}
                </div>
            </div>
        
            <Button className="bg-transparent border-0" onClick={handleReadMore}>
                <Image
                    src={btnIcon}
                    width={70}
                    height={70}
                    alt={'arrowicon'}
                />
            </Button>
            <p className="font-weight-bold">{btnText}</p>
        </div>


    )
}