import { useState } from "react";
import { Card, Button } from "react-bootstrap";
import { useTranslation } from 'next-i18next';
import { setConfig } from "isomorphic-dompurify";

export function Director() {
    const { t } = useTranslation('directors');

    const data = [{
        'id': '1',
        'img': '/static/images/image/directors/Lynn-Ramli.jpeg',
        'name': 'Lynn Ramli',
        'title': t('presidentDirector'),
        'intro': t('intro1'),
        'deskripsi': t('description1'),
    }, {
        'id': '2',
        'img': '/static/images/image/directors/Akira-Sugai.jpeg',
        'name': 'Akira Sugai',
        'title': t('vpDirector'),
        'intro': t('intro2'),
        'deskripsi': t('description2'),
    }, {
        'id': '3',
        'img': '/static/images/image/directors/Toshiyuki-Kojima.jpeg',
        'name': 'Toshiyuki Kojima',
        'title': t('vpDirector'),
        'intro': t('intro3'),
        'deskripsi': t('description3'),
    }, {
        'id': '4',
        'img': '/static/images/image/directors/Sigit-Sembodo.jpeg',
        'name': 'Sigit Sembodo',
        'title': t('director'),
        'intro': t('intro4'),
        'deskripsi': t('description4'),
    }, {
        'id': '5',
        'img': '/static/images/image/directors/A-Lung-Ng.jpeg',
        'name': 'A Lung Ng',
        'title': t('director'),
        'intro': t('intro5'),
        'deskripsi': t('description5'),
    }, {
        'id': '6',
        'img': '/static/images/image/directors/Ryohei-Nakata.png',
        'name': 'Ryohei Nakata',
        'title': t('director'),
        'intro': t('intro6'),
        'deskripsi': t('description6'),
    }, {
        'id': '7',
        'img': '/static/images/image/directors/Charles-Gultom.jpeg',
        'name': 'Charles Gultom',
        'title': t('director'),
        'intro': t('intro7'),
        'deskripsi': t('description7'),
    }]

    const [readMore, setReadMore] = useState({
        'card1': "test-truncate",
        'card2': "test-truncate",
        'card3': "test-truncate",
        'card4': "test-truncate",
        'card5': "test-truncate",
        'card6': "test-truncate",
        'card7': "test-truncate"
    });
    
    const handleClick = (id) => {
        if(readMore['card' + id] == "test-truncate"){
            setReadMore(prevState => {
            return {...prevState, ['card' + id]: "test-readMore"}
        })}
        else{
            setReadMore(prevState => {
                return {...prevState, ['card' + id]: "test-truncate"}
            })
        }
    }

    return (
        <>
            {
                data.map((item, index) => {
                    return (
                        <Card className="block-card w-auto boards-card" key={index}>
                            <Card.Header className="justify-content-center d-flex align-items-center">
                                <img src={item.img} className="boards-image" layout={'fixed'} />
                            </Card.Header>
                            <Card.Body className="halfWidth">
                                <Card.Title className="txt-boards-name fsAlbert">{item.name}</Card.Title>
                                <Card.Text className="txt-boards-title">{item.title}</Card.Text>
                                <Card.Text className="txt-boards-desc">
                                    <p>{item.intro}</p>
                                    <p className={readMore['card' + item.id]}>{item.deskripsi}</p>
                                    {
                                        readMore['card' + item.id] === "test-truncate" ? 
                                        <Button onClick={() => handleClick(item.id)} className="border-0 bg-transparent txt-blue-baf p-0 fsAlbert">{'Read more >>>'}</Button>
                                        :
                                        <Button onClick={() => handleClick(item.id)} className="border-0 bg-transparent txt-blue-baf p-0 fsAlbert">{'Read less <<<'}</Button>
                                    }
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    )
                })
            }
        </>
    )
}
