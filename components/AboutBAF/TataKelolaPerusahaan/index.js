import { Card, Button} from "react-bootstrap";
import { FaChevronDown } from "react-icons/fa";
import { useTranslation } from 'next-i18next';
import { useState } from 'react';


export function TataKelolaPerusahaan(){
    const { t } = useTranslation('aboutus');

    const pdfFiles = [{
        id: '1',
        title: t('cgcImplementation'),
        files: [
            'Penerapan Tata Kelola Perushaan.pdf',
        ],
    },{
        id: '2',
        title: t('boc'),
        files: [
            'Dewan Komisaris.pdf',
        ]
    },{
        id: '3',
        title: t('bod'),
        files: [
            ' Direksi.pdf',
        ]
    },{
        id: '4',
        title: t('internalAudit'),
        files: [
            'Satuan Kerja Audit Internal.pdf',
        ]
    },
    ,{
        id: '5',
        title: t('committeesBoc'),
        files: [
            'Komite Audit.pdf',
            'Komite Nasional.pdf',
        ]
    },
    ,{
        id: '6',
        title: t('committeesBod'),
        files: [
            'Komite Dibawah Direksi.pdf'
        ]
    },{
        id: '7',
        title: t('shariaSupervisorBoard'),
        files: [
            'Komite Dibawah Dewan Komisaris.pdf',
        ]
    },{
        id: '8',
        title: t('divPaymentPol'),
        files: [
            'Dewan Pengawas Syariah.pdf',
        ]
    },{
        id: '9',
        title: t('systemAntiCorrp'),
        files: [
            'Sistem Kebijakan Pelanggan & Anti Korupsi.pdf',
        ]
    },{
        id: '10',
        title: t('riskManagement'),
        files: [
            'Manajemen Risiko.pdf',
        ]
    },{
        id: '11',
        title: t('codeConduct'),
        files: [
            'Kode Etik.pdf',
        ]
    },{
        id: '12',
        title: t('corpSecretary'),
        files: [
            'Sekretaris Perusahaan.pdf',
        ]
    }];

    return(
        <div className="d-flex flex-wrap">
            {
                pdfFiles.map((item, index) => {
                    return (
                        <Card className="block-card mx-auto card-width" key={index}>
                            <Card.Header className="investor-card fsAlbert" >{item.title}</Card.Header>
                            <Card.Body className="mx-auto">
                                <div className="d-flex flex-wrap justify-content-center">
                                {
                                    item.files.map((fileName, index) => {
                                        return (
                                                <div className="pdf-Container">
                                                    <a href= {`/static/files/pdf/documents/${fileName}`} target='_blank' rel='noopener noreferrer'>
                                                        <img src={'/static/images/image/file.png'} width='140px' height="auto"/>
                                                        <div className="pdf-fileName">{fileName}</div>
                                                    </a>
                                                </div>
                                        )
                                    })
                                }
                                </div>
                            </Card.Body>
                        </Card>
                    )
                })
            }
        </div>
       
    )

}
