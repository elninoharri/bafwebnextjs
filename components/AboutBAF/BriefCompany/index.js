import Image from "next/image";
import { useState } from "react";
import { Button } from "react-bootstrap";
import { useTranslation } from "next-i18next";

export function BriefCompany() {
    const { t } = useTranslation('aboutus')
    const [readMore, setReadMore] = useState({
        'btnText': t('readMore'),
        'btnIcon': '/static/images/icons/chevrons-down.svg',
        'showMoreText': false
    });

    const text1 = t('briefCompany1');
    const text2 = t('briefCompany2');
    const { btnText, btnIcon, showMoreText } = readMore;

    const handleReadMore = () => {
        if (showMoreText) {
            setReadMore({
                'btnText': t('readMore'),
                'btnIcon': '/static/images/icons/chevrons-down.svg',
                'showMoreText': false
            });
        } else {
            setReadMore({
                'btnText': t('readLess'),
                'btnIcon': '/static/images/icons/chevrons-up.svg',
                'showMoreText': true
            });
        }
    }

    return (
        <>
            <p className="text-justify" style={{ fontSize: 20 }}>{text1}{showMoreText ? <>{text2}</> : ''}</p>
            <div className="justify-content-center d-flex flex-column text-center">
                <Button className="bg-transparent border-0" onClick={handleReadMore}>
                    <Image
                        src={btnIcon}
                        width={80}
                        height={80}
                        alt={'arrowicon'}
                    />
                </Button>
                <p className="font-weight-bold">{btnText}</p>
            </div>
        </>
    )
}