import { Card } from "react-bootstrap";
import { useTranslation } from 'next-i18next';

export function SocialResponsibility() {
    const { t } = useTranslation('aboutus');

    const data = [
        'Pengembangan Sosial dan Kemasyarakatan.pdf',
        'Praktik Ketenagakerjaan, Kesehatan, dan Keselamatan Kerja.pdf',
        'Lingkungan Hidup.pdf',
        'Tanggung Jawab Kepada Konsumen.pdf'
    ]
    return (
        <>
            <Card className="secretary-card">
                <Card.Body>
                    <Card.Title className="txt-boards-name fsAlbert" >{t('social1Title')}</Card.Title>
                    <Card.Text className="txt-boards-desc">
                        <p>{t('social1Text1')}</p>
                        <p>{t('social1Text2')}</p>
                    </Card.Text>
                    <Card.Title className="fsAlbert text-center txt-blue-baf mt-4">{t('imgLabel1')}</Card.Title>
                    <img src={'/static/images/image/socialImage1.png'} className="mb-4" width="100%"/>
                    <Card.Text className="txt-boards-desc">{t('imgText1')}</Card.Text>
                    <Card.Title className="fsAlbert text-center txt-blue-baf mt-4">{t('imgLabel2')}</Card.Title>
                    <img src={'/static/images/image/socialImage2.png'} className="mb-4" width="100%"/>
                </Card.Body>
            </Card>
            <Card className="secretary-card">
                <Card.Body>
                    <Card.Title className="txt-boards-name fsAlbert" >{t('social2Title')}</Card.Title>
                    <Card.Text className="txt-boards-desc">
                        <p>{t('social2Text1')}</p>
                    </Card.Text>
                </Card.Body>            
            </Card>
            <div className="d-flex mt-4 flex-wrap align-items-center justify-content-center">
                {
                    data.length > 0 && data.map(item => {return (
                            <div className="pdf-Container"> 
                                <a href={`/static/files/pdf/documents/${item}`} target='_blank' rel='noopener noreferrer'>
                                    <img src={'/static/images/image/file.svg'} width='120px' height="auto" />
                                    <div className="pdf-fileName">{item}</div>
                                </a>
                            </div>
                        
                    )})
                }
            </div>
            
        </>
    )
}