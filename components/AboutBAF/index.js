import Image from "next/image";
import { useState } from "react";
import { Tabs, Tab, Row, Col, Nav } from "react-bootstrap";
import { BriefCompany } from "./BriefCompany";
import { Commissioner } from "./Commisioner";
import { CompanyData } from "./CompanyData";
import { CompanyValues } from "./CompanyValues";
import { Director } from "./Director";
import { Secretary } from "./Secretary";
import { NewsEvent } from "./NewsEvent";
import { InvestorRelations } from "./InvestorRelations";
import { TataKelolaPerusahaan } from "./TataKelolaPerusahaan";
import { SocialResponsibility } from "./SocialResponsibility";
import { Achievements } from "./Achievements";
import { useTranslation } from 'next-i18next';

export function AboutBAF() {
    const { t, i18n } = useTranslation(['aboutus', 'common']);

    const tabData = [{
        id: 'about',
        tabHeader: t('sekilasTentangPerusahaan'),
        tabContent: <BriefCompany />,
        tabContentExt: <CompanyData />
    }, {
        id: 'commissioner',
        tabHeader: t('susunanDewanKomisaris'),
        tabContent: <Commissioner />,
        tabContentExt: null
    }, {
        id: 'director',
        tabHeader: t('susunanDewanDireksi'),
        tabContent: <Director />,
        tabContentExt: null
    }, {
        id: 'secretary',
        tabHeader: t('sekretasisPerusahaan'),
        tabContent: <Secretary />,
        tabContentExt: null
    }, {
        id: 'investor',
        tabHeader: t('hubunganInvestor'),
        tabContent: <InvestorRelations/>,
        tabContentExt: null
    }, {
        id: 'regulation',
        tabHeader: t('tataKelolaPerusahaan'),
        tabContent: <TataKelolaPerusahaan/>,
        tabContentExt: null
    }, {
        id: 'social',
        tabHeader: t('tanggungjawabSosial'),
        tabContent: <SocialResponsibility/>,
        tabContentExt: null
    }, {
        id: 'achivement',
        tabHeader: t('penghargaan'),
        tabContent: <Achievements/>,
        tabContentExt: null
    }, {
        id: 'news',
        tabHeader: t('beritaDanAcara'),
        tabContent: <NewsEvent />,
        tabContentExt: null
    }]

    const defaultActive = tabData[0].id ? tabData[0].id : '';

    return (
        <>
            <Tab.Container id="product-tab" defaultActiveKey={defaultActive}>
                <Row>
                    <Col lg={4} className="font-weight-bold pt-4 pb-5">
                        <div
                            style={{ display: "flex", justifyContent: "center" }}
                        >
                            <Image src={'/static/images/image/team.svg'} width={400} height={200} />
                        </div>
                        <div
                            style={{ display: "flex", justifyContent: "center" }}
                            className="pl-3"
                        >
                            <Nav className="flex-column nav-about">
                                {tabData.map((item, index) => {
                                    return (
                                        <Nav.Item key={index}>
                                            <Nav.Link eventKey={item.id} className="txt-blue-baf">{item.tabHeader}</Nav.Link>
                                        </Nav.Item>
                                    )
                                })}
                            </Nav>
                        </div>
                    </Col>
                    <Col lg={8} className="pt-4 pb-5">
                        <Row>
                            <Col lg={1}>
                                <div className="verticalLine"></div>
                            </Col>
                            <Col lg={11}>
                                <Tab.Content className="about-baf">
                                    {tabData.map((item, index) => {
                                        return (
                                            <Tab.Pane eventKey={item.id} key={index}>
                                                <div >{item.tabContent}</div>
                                            </Tab.Pane>
                                        )
                                    })}
                                </Tab.Content>
                            </Col>
                        </Row>

                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Tab.Content className="mx-auto">
                            {tabData.map((item, index) => {
                                return (
                                    <Tab.Pane eventKey={item.id} key={index}>
                                        <div >{item.tabContentExt}</div>
                                    </Tab.Pane>
                                )
                            })}
                        </Tab.Content>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <CompanyValues />
                    </Col>
                </Row>
            </Tab.Container>
        </>
    )
}