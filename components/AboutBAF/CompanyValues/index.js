import Image from "next/image";
import { Col, Row } from "react-bootstrap";
import { useTranslation } from "next-i18next";

export function CompanyValues() {
    const { t } = useTranslation("aboutus")
    const data = [{
        'img': '/static/images/image/company-values/visionary.svg',
        'title': 'VISIONARY',
        'subtitle': t('visionaryText')
    },
    {
        'img': '/static/images/image/company-values/reliable.svg',
        'title': 'RELIABLE',
        'subtitle': t('reliableText')
    },
    {
        'img': '/static/images/image/company-values/accessible.svg',
        'title': 'ACCESSIBLE',
        'subtitle': t('accessibleText')
    },
    ]


    return (
        <Row className="p-4 text-center">
            {data.map((item, index) => {
                return (
                    <Col key={index} xs={12} lg={4} md={4}>
                        <Row>
                            <Col className="float-right" xs={12} lg={5} md={6}>
                                <Image
                                    src={item.img}
                                    width={140}
                                    height={140}
                                    layout={'fixed'}
                                    alt={'baf'}
                                />
                            </Col>
                            <Col className="pt-4" xs={12} lg={7} md={6} style = {{paddingLeft: '2rem !important'}}>
                                <h3 className="txt-yellow company-valuesText">{item.title}</h3>
                                <h5 className="txt-blue-baf font-weight-normal company-valuesSubtext">{item.subtitle}</h5>
                            </Col>
                        </Row>
                    </Col>
                )
            })}
        </Row>
    )
}