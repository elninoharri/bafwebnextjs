import { Card } from "react-bootstrap";
import { useTranslation } from 'next-i18next';

export function Secretary() {
    const { t } = useTranslation('secretary');

    return (
        <>
            <img src={'/static/images/image/secretary/secretary.png'} width="100%"/>
            <Card className="secretary-card">
                <Card.Body>
                    <Card.Title className="txt-boards-name fsAlbert" >{t('secretaryProfile')}</Card.Title>
                    <Card.Text className="txt-boards-desc">
                        <p>{t('profileText1')}</p>
                        <p>{t('profileText2')}</p>
                    </Card.Text>
                </Card.Body>
            </Card>
            <Card className="secretary-card">
                <Card.Body>
                    <Card.Title className="txt-boards-name fsAlbert">{t('aboutSecretary')}</Card.Title>
                    <Card.Text className="txt-boards-desc">
                        <p>{t('aboutText1')}</p>
                        <p>{t('aboutText2')}</p>
                    </Card.Text>
                    <hr className="line"/>
                    <Card.Title className="secretary-subtitle fsAlbert">{t('legalBasis')}</Card.Title>
                    <Card.Text className="txt-boards-desc">
                        <p>{t('legalText1')}</p>
                        <p>{t('legalText2')}</p>
                    </Card.Text>
                    <hr className="line"/>
                    <Card.Title className="secretary-subtitle fsAlbert">{t('responsibilitiesSecretary')}</Card.Title>
                    <Card.Text className="txt-boards-desc">
                        <ul>
                            <li>
                                {t('responsibility1')}
                            </li>
                            <li>
                                {t('responsibility2')}
                            </li>
                            <li>
                                {t('responsibility3')}
                            </li>
                            <li>
                                {t('responsibility4')}
                            </li>
                            <li>
                                {t('responsibility5')}
                            </li>
                            <li>
                                {t('responsibility6')}
                            </li>
                        </ul>
                    </Card.Text>
                        <a className="d-flex align-items-center" href= {`/static/files/pdf/Duty Implementation Report.pdf`} target='_blank' rel='noopener noreferrer'>
                            <img src={'/static/images/image/pdfLogo.svg'} width="35px" height="auto" style={{marginRight: '10px'}}/>
                            <div className="txt-blue-baf">{t('report')}</div>
                        </a>
                   
                </Card.Body>            
            </Card>
        </>
    )
}