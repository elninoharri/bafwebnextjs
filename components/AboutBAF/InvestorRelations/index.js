import { Card } from "react-bootstrap";
import { FaChevronDown } from "react-icons/fa";
import { FaChevronUp } from "react-icons/fa";
import { useTranslation } from 'next-i18next';
import { useEffect, useState } from 'react';
import Masonry from 'react-masonry-css'

export function InvestorRelations() {
    const { t } = useTranslation('aboutus');

    const breakpointColumnsObj = {
        default: 2,
        1199: 2,
        990: 2,
        464: 1,
      };


    var desktop = window.matchMedia("(min-width: 990px)");
    var others = window.matchMedia("(min-width: 0px) and (max-width: 989px)");

    const [showItems, setShowItems] = useState({
        card1: 0,
        card2: 0,
        card3: 0,
        card4: 0,
        card5: 0,
        card6: 0,

    });

    useEffect(() => {
        if(desktop.matches){
            setShowItems({
                card1: 2,
                card2: 2,
                card3: 2,
                card4: 2,
                card5: 2,
                card6: 2,
            })
        }
        else if(others.matches){
            setShowItems({
                card1: 1,
                card2: 1,
                card3: 1,
                card4: 1,
                card5: 1,
                card6: 1,
            })
        }
    }, [])

    const pdfFiles = [{
        id: '1',
        title: t('financialHighlights'),
        files: [
            {filename: 'Financial Highlights 2018 1.jpeg', size: '320 KB'},
            {filename: 'Financial Highlights 2018 2.jpeg', size: '320 KB'},
            {filename: 'Financial Highlights 2019.pdf', size: '320 KB'}
        ]
    }, {
        id: '2',
        title: t('financialStatements'),
        files: [
            {filename: 'Financial Statement December 2017.pdf', size: '320 KB'},
            {filename: 'Financial Statement December 2017 Publication.pdf', size: '320 KB'},
            {filename: 'Financial Statement June 2018.pdf', size: '320 KB'},
            {filename: 'Financial Statement June 2018 Publication.pdf', size: '320 KB'},
            {filename: 'Financial Statement December 2018.pdf', size: '320 KB'},
            {filename: 'Financial Statement December 2018 Publication.pdf', size: '320 KB'},
            {filename: 'Financial Statement June 2019.pdf', size: '320 KB'},
            {filename: 'Financial Statement June 2019 Publication.pdf', size: '320 KB'},
            {filename: 'Financial Statement December 2019.pdf', size: '320 KB'},
            {filename: 'Financial Statement December 2019 Publication.pdf', size: '320 KB'},
            {filename: 'Financial Statement June 2020.pdf', size: '320 KB'},
            {filename: 'Financial Statement June 2020 Publication.pdf', size: '320 KB'},
            {filename: 'Financial Statement December 2020.pdf', size: '320 KB'}
        ]
    }, {
        id: '3',
        title: t('annualReport'),
        files: [
            {filename: 'Annual Report 2017.pdf', size: '320 KB'},
            {filename: 'Annual Report 2019.pdf', size: '320 KB'},
            {filename: 'Annual Report 2020.pdf', size: '320 KB'}
        ]
    }, {
        id: '4',
        title: t('shareholderMeeting'),
        files: [
            {filename: 'General Shareholder Meetings 2017.pdf', size: '320 KB'},
            {filename: 'General Shareholder Meetings 2018.pdf', size: '320 KB'},
            {filename: 'General Shareholder Meetings 2019.pdf', size: '320 KB'}
        ]
    },
    , {
        id: '5',
        title: t('bondInfo'),
        files: [
            {filename: 'Bond Information 2017.pdf', size: '320 KB'},
            {filename: 'Bond Information 2018.pdf', size: '320 KB'},
            {filename: 'Bond Information 2019.pdf', size: '320 KB'},
            {filename: 'Bond Information 2020.pdf', size: '320 KB'},
            {filename: 'Summary Information of Shelf Registration Phase II Year 2020 - Sukuk.pdf', size: '320 KB'},
            {filename: 'Additional Summary Information of Shelf Registration Phase II Year 2020 - Sukuk.pdf', size: '320 KB'},
            {filename: 'Additional Information PUB I Phase II Year 2020 – Sukuk.pdf', size: '320 KB'},
            {filename: 'Additional Information Summary of Shelf Registration Phase II Year 2021 - Bond.pdf', size: '320 KB'}
        ]
    },
    , {
        id: '6',
        title: t('capMarket'),
        files: [
            {filename: 'Capital Market Supporting.pdf', size: '320 KB'}
        ]
    }];

    const handleClick = (id, idx) => {
        if(showItems['card' + id] == 1 || showItems['card' + id] == 2 ){
            setShowItems(prevState => {
                return {...prevState, ['card' + id]: pdfFiles[idx].files.length}
            })
        }
        else{
            setShowItems(prevState => {
                return {...prevState, ['card' + id]: desktop.matches ? 2 : 1}
            })        }
    }
    return (
        <Masonry breakpointCols={breakpointColumnsObj} className="my-masonry-grid" columnClassName="my-masonry-grid_column">
            {
                pdfFiles.map((item, index) => {
                    return (
                        <div>
                        <Card className="investorBlock-card" key={index}>
                            <Card.Header className="investor-card fsAlbert" >{item.title}</Card.Header>
                            <Card.Body className="mx-auto">
                                <div className="d-flex flex-wrap justify-content-center">
                                    {
                                        item.files.slice(0, showItems['card' + item.id]).map((fileName) => {
                                            return (
                                                <div className="pdf-Container">
                                                    <a href={`/static/files/pdf/documents/${fileName.filename}`} target='_blank' rel='noopener noreferrer'>
                                                        <img src={'/static/images/image/file.svg'} width='140px' height="auto" />
                                                        <div className="pdf-fileName">{fileName.filename}</div>
                                                        <p className="fileSize">{fileName.size}</p>
                                                    </a>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </Card.Body>
                            <Card.Footer style={{ border: 'none', position: 'sticky', backgroundColor: 'white' }}>
                                <img src={'/static/images/icons/folder.svg'} style={{ marginRight: '20px' }} />{item.files.length} {item.files.length > 1 ? t('document') : t('document').substring(0, 8)}
                                {
                                    showItems['card' + item.id] == 2 || showItems['card' + item.id] == 1? 
                                    <FaChevronDown className="expand-icon" onClick={() => {handleClick(item.id, index)}} />
                                    :
                                    <FaChevronUp className="expand-icon" onClick={() => {handleClick(item.id, index)}} />
                                }
                            </Card.Footer>
                        </Card>
                        </div>
                    )
                })
            }
    </Masonry>
    )

}