import { Button, Card, Col, Container, Row } from "react-bootstrap";
import Image from 'next/image';
import { useTranslation } from 'next-i18next';
export function CompanyData() {
    const { t, i18n } = useTranslation('aboutus');

    const shareholders = [{
        'img': '/static/images/logos/mitsui.svg',
        'name': 'Mitsui & Co., Ltd.',
        'percentage': '65,0%',
        'hasimage': 'Y'
    }, {
        'img': '/static/images/logos/yamaha.svg',
        'name': 'Yamaha Motor\nCo., Ltd',
        'percentage': '17,7%',
        'hasimage': 'Y'
    }, {
        'img': '/static/images/logos/yamaha.svg',
        'name': 'PT Yamaha Indonesia\nMotor Manufacturing',
        'percentage': '2,3%',
        'hasimage': 'Y'
    }, {
        'img': '',
        'name': 'PT Sinergi\nAutoindoAbadi',
        'percentage': '15,0%',
        'hasimage': 'N'
    }]

    return (
        <div className="text-center txt-blue-baf">
            <span className="dot-left">
                <Image
                    src={'/static/images/icons/dot1.svg'}
                    width={180}
                    height={180}
                    layout={'fixed'}
                    alt={'dot-left'}
                />
            </span>
            <Container>
                <Row>
                    <Col className="pt-4" lg={6} xs={12}>
                        <Image
                            src={'/static/images/image/company-values/vision.svg'}
                            width={170}
                            height={170}
                            layout={'fixed'}
                            alt={'visi'}
                        />
                        <p className="super-bold mb-3 mt-3" style={{ fontFamily: 'FSalbert-Bold', fontWeight: 900, fontSize: 35 }}>{t('vision')}</p>
                        <h5 className="display-linebreak">{t('visionText')}</h5>
                    </Col>
                    <Col className="pt-4" lg={6} xs={12}>
                        <Image
                            src={'/static/images/image/company-values/mision.svg'}
                            width={170}
                            height={170}
                            layout={'fixed'}
                            alt={'misi'}
                        />
                        <p className="super-bold mb-3 mt-3" style={{ fontFamily: 'FSalbert-Bold', fontWeight: 900, fontSize: 35 }}>{t('mision')}</p>
                        <h5 className="display-linebreak">{t('missionText')}</h5>
                    </Col>
                </Row>
            </Container>
            <span className="dot-right">
                <Image
                    src={'/static/images/icons/dot2.svg'}
                    width={180}
                    height={180}
                    layout={'fixed'}
                    alt={'dot-right'}
                />
            </span>
            <Row className="p-4">
                <Col className="pt-4" lg={3} xs={12}>
                    <p className="super-bold txt-yellow" style={{ fontFamily: 'FSalbert-Bold', fontWeight: 900, fontSize: 60 }}>{'1997'}</p>
                    <h4 className="font-weight-bold">{t('year')}</h4>
                </Col>
                <Col className="pt-4" lg={3} xs={12}>
                    <p className="font-weight-bold txt-yellow" style={{ fontFamily: 'FSalbert-Bold', fontWeight: 900, fontSize: 60 }}>{'>6.400'}</p>
                    <h4 className="font-weight-bold">{t('employees')}</h4>
                </Col>
                <Col className="pt-4" lg={3} xs={12}>
                    <p className="font-weight-bold txt-yellow" style={{ fontFamily: 'FSalbert-Bold', fontWeight: 900, fontSize: 60 }}>{'>353M'}</p>
                    <h4 className="font-weight-bold">{t('capital')}</h4>
                </Col>
                <Col className="pt-4" lg={3} xs={12}>
                    <p className="font-weight-bold txt-yellow" style={{ fontFamily: 'FSalbert-Bold', fontWeight: 900, fontSize: 60 }}>{'246'}</p>
                    <h4 className="font-weight-bold">{t('businessNetwork')}</h4>
                </Col>
            </Row>
            <Row>
                <Col className="pt-4">
                    <h3 className="font-weight-bold shareholder">{t('shareHolders')}</h3>
                    <div className="tree">
                        <a href="#" className="border-0 p-0 m-0">
                            <Image
                                src={'/static/images/logos/baf.svg'}
                                width={153}
                                height={71}
                                layout={'fixed'}
                                alt={'baf'}
                            /></a>
                        <div className="d-flex justify-content-center p-0 m-0">
                            <div style={{ width: 5, height: 20, backgroundColor: '#ccc', marginTop: -10 }}></div>
                        </div>
                        <div className="d-flex justify-content-center p-0 m-0">
                            <ul className="w-90 p-0">
                                {shareholders.map((item, index) => {
                                    return (
                                        <li className="container-stackholder" key={index}>
                                            <a className="border-0">
                                                <Card className="flex-column block-card shareholder-card">
                                                    <Card.Header className="title-shareholder">
                                                        {item.percentage}
                                                    </Card.Header>
                                                    <Card.Body className="align-items-center d-flex justify-content-center">
                                                        <div>
                                                            {
                                                                item.hasimage === 'Y' ?
                                                                    <Image
                                                                        src={item.img}
                                                                        width={180}
                                                                        height={80}
                                                                        layout={'fixed'}
                                                                        alt={'baf'} />
                                                                    : null
                                                            }
                                                            <Card.Text className="display-linebreak txt-blue-baf" style={{ width: 180, fontSize: item.hasimage === 'Y' ? 18 : 22 }}>{item.name}</Card.Text>
                                                        </div>
                                                    </Card.Body>
                                                </Card>
                                            </a>
                                        </li>
                                    )
                                })}
                            </ul>
                        </div>
                    </div>
                </Col>
            </Row>
            <Container>
                <Row className="p-4">
                    <Col className="pt-4">
                        <h3 className="font-weight-bold text-brandon text-uppercase">{t('organizationStructure')}</h3>
                        <p className="text-capitalize">{t('organizationStructure')}<br />PT Bussan Auto Finance</p>
                        <a href="static/files/pdf/ccddb6f757e272794dbb6f0c2830327f.pdf" className="btn oval-button bg-blue-baf txt-white" role="button" aria-pressed="true" target="_blank">{t('clickHere')}</a>
                    </Col>
                    <Col className="pt-4">
                        <h3 className="font-weight-bold text-brandon  text-uppercase">{t('articlesOfAssociation')}</h3>
                        <p className="text-capitalize">{t('articlesOfAssociation')}<br />PT Bussan Auto Finance</p>
                        <a href="static/files/pdf/59a7772f823d694dbb206b8dc83a6367.pdf" className="btn oval-button bg-blue-baf txt-white" role="button" aria-pressed="true" target="_blank">{t('clickHere')}</a>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}