import { Card } from "react-bootstrap";
import { useTranslation } from "next-i18next";

export function Tagline() {
    const { t } = useTranslation("home");
    return (
        <>
            <Card>
                <Card.Header className="border-bottom-0 bg-white">
                    <div className="float-right">
                        <Card.Img src={'/static/images/icons/best_brand_2019.svg'} style={{ height: 75, width: 50 }} />
                        <Card.Img src={'/static/images/icons/top_brand_2020.svg'} style={{ height: 75, width: 50 }} className="ml-1 mr-1" />
                        <Card.Img src={'/static/images/icons/ojk.svg'} style={{ height: 75, width: 75 }} />
                    </div>
                </Card.Header>
                <Card.Body className="text-center">
                    <p className="txt-tagline">{t('tagline')}</p>
                </Card.Body>
            </Card>
        </>
    )
}


