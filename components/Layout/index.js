
import { useRouter } from "next/router";
import Head from "next/head";
import { Footer } from "../Footer";
import { NavBar } from "../NavBar";


export function Layout({ children }) {
	const router = useRouter();

	const page = (val) => {
		let newVal = 'Home'
		if (val.length > 1) {
			newVal = val.replace('/', '').toUpperCase();
		}
		return newVal;
	}

	const currentPage = router.pathname ? page(router.pathname) : '';
	return (
		<main className="layout">
			<Head>
				<title>
					{/* Bussan Auto Finance hadir memberikan pelayanan yang Cepat, Ringan, dan Terpercaya untuk Pembiayaan Kredit Motor Yamaha, Pembiayaan Dana Tunai Syana, Pembiayaan Kredit Barang Elektronik dan Furnitur. */}
					{'Bussan Auto Finance :: ' + currentPage}
				</title>
			</Head>
			<NavBar />
			{children}
			<Footer />
		</main>
	);
}