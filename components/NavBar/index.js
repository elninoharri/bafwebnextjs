import { Navbar, Form, FormControl, Nav, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import Link from 'next/link';
import { useTranslation } from 'next-i18next';
import Select from 'react-select';
import { useRouter } from 'next/router';
import { useCookies } from 'react-cookie';
import { useEffect, useState } from 'react';

export function NavBar() {
    const [cookie, setCookie] = useCookies(['NEXT_LOCALE']);
    const router = useRouter();
    const { t, i18n } = useTranslation('navbar');
    const [isLogin, setIsLogin] = useState(false);

    const langData = [{
        value: 'id',
        label: <img src={'/static/images/icons/ind.svg'} style={{ width: 40, height: 20 }} />
    }, {
        value: 'en',
        label: <img src={'/static/images/icons/eng.svg'} style={{ width: 40, height: 20 }} />
    }]

    const handleLang = (lang) => {
        const locale = lang.value;
        router.replace(router.pathname, router.asPath, { locale });
        if (cookie.NEXT_LOCALE !== locale) {
            setCookie("NEXT_LOCALE", locale, { path: "/" });
        }
    }

    const customStyle = {
        control: styles => ({ ...styles, borderColor: 'transparent', backgroundColor: 'transparent', width: 80, padding: 0 }),
        option: (styles, { data, isDisabled, isFocused, isSelected }) => {
            return {
                ...styles,
                backgroundColor: 'transparent',
                textAlign: 'center'
            };
        }
    };

    useEffect(() => {
        if (window) {
            const token = window.sessionStorage.getItem('userToken');
            if (token) { setIsLogin(true) } else { setIsLogin(false) };
        }
    }, [])

    const auth = () => {
        if (isLogin) {
            if (window) {
                const locale = i18n.language;
                window.sessionStorage.clear();
                router.replace(router.pathname, router.asPath, { locale });
                setIsLogin(false);
            }
        } else {
            router.push('/login');
        }
    }

    return (
        <>
            <Navbar bg="light" expand="lg" sticky="top" style={{ backgroundColor: '#edeef7' }} className={router.pathname === '/aboutus' ? "main-navigation" : ""}>
                <Link href="/" passHref locale={i18n.language}>
                    <Navbar.Brand>
                        <img
                            src={'/static/images/logos/baf.svg'}
                            className="d-inline-block align-top nav-icon"
                            alt="BAF logo"
                        />
                    </Navbar.Brand>
                </Link>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto nav-fill n-width">
                        <Link href="/aboutus" passHref locale={i18n.language}>
                            <Nav.Link className="txt-blue-baf">{t('aboutbaf')}</Nav.Link>
                        </Link>
                        <Link href="/product" passHref locale={i18n.language}>
                            <Nav.Link className="txt-blue-baf">{t('product')}</Nav.Link>
                        </Link>
                        <Link href="/event" passHref locale={i18n.language}>
                            <Nav.Link className="txt-blue-baf">{'Event'}</Nav.Link>
                        </Link>
                        <Link href="/promo" passHref locale={i18n.language}>
                            <Nav.Link className="txt-blue-baf">{'Promo'}</Nav.Link>
                        </Link>
                        <Link href="/article" passHref locale={i18n.language}>
                            <Nav.Link className="txt-blue-baf">{t('article')}</Nav.Link>
                        </Link>
                        <Link href="/contactus" passHref locale={i18n.language}>
                            <Nav.Link className="txt-blue-baf">{t('contactus')}</Nav.Link>
                        </Link>
                        <Form inline>
                            {/* <Link href="/login" locale={i18n.language}> */}
                            <Button className="bg-blue-baf mr-5 ml-5" onClick={auth}>{isLogin ? t('logout') : t('login')}</Button>
                            {/* </Link> */}
                            <FontAwesomeIcon icon={faSearch} color="#002F5F" className="mr-3" />
                            <Select
                                options={langData}
                                instanceId={'appType'}
                                styles={customStyle}
                                components={{ IndicatorSeparator: () => null }}
                                value={langData[i18n.language == 'id' ? 0 : 1]}
                                onChange={handleLang}
                            />
                        </Form>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </>
    )
}
