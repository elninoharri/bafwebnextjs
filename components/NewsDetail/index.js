import { Container, Row, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { PicNews } from "./PIC";
import Breadcrumbs from "../Breadcumb";
import DOMPurify from 'isomorphic-dompurify';
import { SimulationFormNMC, SimulationFormCar, SimulationFormSyana } from "../../components";
import { PromoInfoCard } from "../PromoTab/PromoInfoCard";
import { getVoucherCode, simulationData } from "../../store";
import { useRouter } from "next/router";
import { useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";

export function NewsDetail(props) {
    const { t } = useTranslation('alerts');
    const dispatch = useDispatch();
    const isInitialMount = useRef(true);

    const router = useRouter();
    const newsData = useSelector(state => state.newsData.data ? state.newsData.data : '');
    const voucherResult = useSelector(state => state.getVoucherCode.result);
    const voucherError = useSelector(state => state.getVoucherCode.error);
    const voucherLoading = useSelector(state => state.getVoucherCode.loading);


    const renderSimulation = () => {
        return (
            <>
                {
                    (newsData.category == 'bnmc' || newsData.category == 'bumc') ?
                        <SimulationFormNMC /> :
                        newsData.category == 'bcar' ?
                            <SimulationFormCar /> :
                            newsData.category == 'bmp' ?
                                <SimulationFormCar /> :
                                newsData.category == 'bsyana' ?
                                    <SimulationFormSyana /> :
                                    null
                }
            </>
        )
    }

    const getPromo = () => {
        if (window) {
            const token = window.sessionStorage.getItem('userToken');
            if (token) {
                dispatch(getVoucherCode({ "Code": newsData.bannerCode }, token));
            } else {
                router.push('/login');
            }
        }
    }

    useEffect(() => {
        if (isInitialMount.current) {
            isInitialMount.current = false;
        } else {
            if (voucherError) {
                swal({ title: t('warning'), text: `${voucherError.message}` || t('warningText1'), icon: 'error', timer: 3000 });
            } else if (voucherResult && voucherResult.hasOwnProperty('VoucherCode')) {
                let voucherCode = voucherResult.VoucherCode;
                let lob = newsData.category == 'bnmc' ? 'NMC' : newsData.category == 'bumc' ? 'UMC' : newsData.category == 'bmp' ? 'MP' : newsData.category == 'bsyana' ? 'SYANA' : newsData.category == 'bother' ? 'other' : '';
                let objSimulasi = {
                    jenisBarang: lob,
                    merekBarang: '',
                    nominal: '',
                    dp: '',
                    tenor: '',
                    voucherCode: voucherCode
                }
                dispatch(simulationData(objSimulasi));
                router.push({ pathname: '/application-form' });
            }
        }
    }, [voucherError, voucherResult])

    const getPricelist = () => {
        alert('coming soon');
    }

    return (
        <>
            {newsData.img && <img src={newsData.img} className="w-100 h-25" style={{ maxHeight: 480 }} />}
            <Container>
                <Breadcrumbs />
                <h2 className="txt-black font-weight-bold text-center txt-detailTitle">{newsData.title}</h2>
                <p className="text-muted text-center"><img src={'/static/images/icons/calendarIcon.svg'} style={{ paddingRight: '5px' }} /><small>{newsData.date}</small></p>
                <Row>
                    <Col>
                        {(props.bannerType == 'PROMOTION' || props.bannerType == 'EVENT') &&
                            <PromoInfoCard
                                datefrom={newsData.date}
                                dateto={newsData.dateto}
                                handlePricelist={getPricelist}
                                handlePromo={getPromo}
                                promoLoading={voucherLoading}
                            />}
                        <div className="text-justify" dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(newsData.content) }} />
                    </Col>
                </Row>
                {newsData.hasPIC && <PicNews />}
                {(props.bannerType == 'PROMOTION' || props.bannerType == 'EVENT') && renderSimulation()}
            </Container>
        </>
    )
}