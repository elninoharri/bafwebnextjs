export function PicNews() {
    return (
        <p>
            <b>Informasi lebih lanjut silakan menghubungi:<br/>
            Pangestu Wibowo</b><br/>
            Marketing Communication & Branding (Division Head)<br/>
            PT Bussan Auto Finance<br/>
            BAF Plaza 6th Floor<br/>
            Jl. Raya Tanjung Barat No.121, Jagakarsa, Jakarta Selatan 12530, Indonesia<br/>
            Telp +62-21 2939 6000<br/>
            pangestu.wibowo@baf.id<br/>
        </p>
    )
}