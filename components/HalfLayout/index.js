
import { useRouter } from "next/router";
import Head from "next/head";
import { Col, Container, Row } from "react-bootstrap";


export function HalfLayout({ children, imgSource }) {
	const router = useRouter();

	const page = (val) => {
		let newVal = 'Home'
		if (val.length > 1) {
			newVal = val.replace('/', '').toUpperCase();
		}
		return newVal;
	}

	const currentPage = router.pathname ? page(router.pathname) : '';
	return (
		<main className="layout">
			<Head>
				<title>
					{'Bussan Auto Finance :: ' + currentPage}
				</title>
			</Head>
			<Container fluid>
				<Row>
					<Col lg={6} className="h-100-vh bg-login" />
					<Col lg={6} className="h-100-vh d-flex justify-content-center half-right split">
						{children}
					</Col>
				</Row>
			</Container>
		</main>
	);
}