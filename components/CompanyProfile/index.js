
import { Col, Container, Row } from "react-bootstrap";
import ReactPlayer from "react-player";
import { useTranslation } from 'next-i18next';
export function CompanyProfile() {
    const { t } = useTranslation('common');
    return (
        <Container fluid>
            <Row className="text-center txt-blue-baf">
                <Col className="my-auto mx-auto" lg={6} md={6} xs={12}>
                    <div className="column-margin">
                        <h1 className="font-weight-bold display-linebreak">{t('companyProfile')}</h1>
                        <p className="display-linebreak">
                            {t('companyProfileText')}
                        </p>
                        <a target="_blank" href="https://www.youtube.com/watch?v=ddVUSqxdDJM" rel="noopener noreferrer">
                            <div className="btn-yuk-nonton p-3"><div className="font-weight-bold my-auto">{t('letsWatch')}</div></div>
                        </a>
                    </div>
                </Col>
                <Col>
                    <div className='player-wrapper' lg={6} md={6} xs={12}>
                        <ReactPlayer
                            className='react-player'
                            url='https://www.youtube.com/watch?v=ddVUSqxdDJM'
                            width='100%'
                            height='100%'
                        />
                    </div>
                </Col>
            </Row>
        </Container>
    );
}